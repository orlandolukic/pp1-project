package rs.ac.bg.etf.pp1.clss;

import java.util.Iterator;
import java.util.LinkedList;

import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.ast.SyntaxNode;
import rs.ac.bg.etf.pp1.context.MethodTableContext;
import rs.ac.bg.etf.pp1.util.Scopes;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class ClassDeclaration {
	
	public static final int ABSTRACT = 0,
							CONCRETE = 1;
	
	private static SemanticAnalyzer sa;
	
	private int startAddress;
	private int startAddressVirtualTable;
	private boolean isAbstract;
	private int fieldNumber;
	private Obj tblEntry;
	private ClassDeclaration superClass;
	private ClassMethods methods;
	private ClassFields fields;
	private int definedLine;
	private MethodTable methodTable;
	private LinkedList<Integer> refactorVirtualTable;
	private int sizeOfVirtTableBytes;
	
	public ClassDeclaration( String name, boolean isAbstract, ClassDeclaration superClass, int definedLine )
	{
		methods = new ClassMethods();
		fields = new ClassFields(this);
		this.startAddress = 0;
		this.startAddressVirtualTable = 0;
		this.isAbstract = isAbstract;
		this.superClass = superClass;
		this.definedLine = definedLine;
		this.refactorVirtualTable = new LinkedList<Integer>();
		this.methodTable = null;
		this.sizeOfVirtTableBytes = 0;
		if ( sa == null )
			sa = SemanticAnalyzer.__instance__();
		
		// Create table entry.
		tblEntry = Tab.insert( Obj.Type, name, new Struct( Struct.Class ) );		
		if ( superClass != null )
			tblEntry.getType().setElementType( superClass.tblEntry.getType() );
		
		insertObj( new Obj( Obj.NO_VALUE, name, new Struct( Struct.None ) ) );
		
		// Execute scope operations.
		Tab.openScope();
		Scopes.put( ClassUtil.getScopeNameForClass(this), Tab.currentScope() );
	}
	
	public MethodTable getMethodTable()
	{
		return methodTable;
	}
	
	public int getSizeOfVirtualTableInBytes()
	{
		return this.sizeOfVirtTableBytes;
	}
	
	public Obj getSymbolTableEntry()
	{
		return tblEntry;
	}	
	
	public void setStartAddress(int addr)
	{
		if ( addr >= 0 ) 
			startAddress = addr;
	}
	
	public int getStartAddress()
	{
		return startAddress;
	}
	
	public int getStartAddressVirtualTable( int addr )
	{
		if ( addr != -1 )
			refactorVirtualTable.add(addr);
		return startAddressVirtualTable;
	}
	
	public void addRefactorListElementForVT( int addr )
	{
		refactorVirtualTable.add(addr);
	}
	
	public void setStartAddressVirtualTable( int addr )
	{
		// Set start address and refactor all addresses in the code.
		if ( startAddressVirtualTable == 0 )
		{
			startAddressVirtualTable = addr;
			int k,pc;
			Iterator<Integer> it = refactorVirtualTable.iterator();
			while( it.hasNext() )
			{
				k = it.next();
				pc = Code.pc;
				Code.pc = k;
				Code.put4( addr );
				Code.pc = pc;
			};
			refactorVirtualTable.clear();
		};
	}
	
	public int getClassFieldsNumber()
	{
		return this.fieldNumber;
	}
	
	public int getDefinedLine()
	{
		return definedLine;
	}
	
	public boolean isAbstractClass()
	{
		return isAbstract;
	}
	
	public boolean isExtendedClass()
	{
		return tblEntry.getType().getElemType() != null;
	}
	
	public ClassMethods getAllMethods()
	{
		return methods;
	}
	
	public ClassFields getAllFields()
	{
		return fields;
	}
	
	public boolean canInstantiate()
	{
		return !isAbstract;
	}
	
	public ClassDeclaration getSuperClass()
	{
		return superClass;
	}
	
	public String getName()
	{
		return tblEntry.getName();
	}
	
	public MethodTable classDeclarationFinished( SyntaxNode node )
	{		
		// Set current number of fields.
		fieldNumber = _getFieldNumber();
		
		// Fill method table.
		this.methodTable = new MethodTable(this);
		this.methodTable.initMethodTable(this, node);
		
		// Check methods if class is concrete & has abstract methods.
		this.methodTable.checkMethodTable(this, node);	
		
		Tab.closeScope();
		
		// Update number of fields.
		updateFieldNumber();
		
		return this.methodTable;
	}
	
	public int getMethodStartAddress( String methodName )
	{
		ClassMethod m = methods.getMethod(methodName);
		if ( m != null )
			return m.getStartAddress();
		
		return 0;
	}
	
	public void insertObj( Obj o )
	{
		tblEntry.getType().getMembersTable().insertKey(o);
	}
	
	private int _getFieldNumber()
	{
		int num = fields.getNumberOfFields();
		ClassDeclaration d = this.superClass;
		while( d != null )
		{
			num += d.getAllFields().getNumberOfFields();
			d = d.getSuperClass();
		};		
		return num;		
	}
	
	public void updateFieldNumber()
	{
		LinkedList<ClassDeclaration> l = new LinkedList<ClassDeclaration>();
		l.add(this);
		ClassDeclaration d = this.superClass;
		while( d != null )
		{			
			l.add(d);
			d = d.getSuperClass();			
		};
		
		int pos = 1;
		Iterator<ClassDeclaration> it = l.descendingIterator();
		while( it.hasNext() )
		{
			d = it.next();
			pos = d.getAllFields().setPositionToTheFields(pos);
		};
	}
	
	/**
	 * Creates virtual table.
	 * 
	 * @return size of the virtual table
	 */
	public int createVirtualTable( int cntEntry )
	{
		int cnt = cntEntry;
		int addr = this.startAddressVirtualTable;
		Iterator<MethodTableContext> it = methodTable.iterator();
		MethodTableContext c;
		String methodName;
		while( it.hasNext() )
		{
			c = it.next();
			methodName = c.method.getMethodName();
			if ( c.method.isAbstractMethod() )
				continue;
			
			for (int i=0, n=methodName.length(); i<n; i++)
			{
				Functions.addStaticData(cnt++, methodName.charAt(i));
				addr += 8;
			};
			Functions.addStaticData( cnt++, -1 );
			addr += 8;
			Functions.addStaticData( cnt++, c.method.getStartAddress() );
			addr += 8;
		};		
		Functions.addStaticData( cnt++, -2 );
		addr += 8;		
		
		sizeOfVirtTableBytes = addr - this.startAddressVirtualTable;
		return cnt - cntEntry;
	}
}
