// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class ForeachStatement extends Matched {

    private ForeachStatementStart ForeachStatementStart;
    private ForeachParensContent ForeachParensContent;
    private StatementListOptions StatementListOptions;

    public ForeachStatement (ForeachStatementStart ForeachStatementStart, ForeachParensContent ForeachParensContent, StatementListOptions StatementListOptions) {
        this.ForeachStatementStart=ForeachStatementStart;
        if(ForeachStatementStart!=null) ForeachStatementStart.setParent(this);
        this.ForeachParensContent=ForeachParensContent;
        if(ForeachParensContent!=null) ForeachParensContent.setParent(this);
        this.StatementListOptions=StatementListOptions;
        if(StatementListOptions!=null) StatementListOptions.setParent(this);
    }

    public ForeachStatementStart getForeachStatementStart() {
        return ForeachStatementStart;
    }

    public void setForeachStatementStart(ForeachStatementStart ForeachStatementStart) {
        this.ForeachStatementStart=ForeachStatementStart;
    }

    public ForeachParensContent getForeachParensContent() {
        return ForeachParensContent;
    }

    public void setForeachParensContent(ForeachParensContent ForeachParensContent) {
        this.ForeachParensContent=ForeachParensContent;
    }

    public StatementListOptions getStatementListOptions() {
        return StatementListOptions;
    }

    public void setStatementListOptions(StatementListOptions StatementListOptions) {
        this.StatementListOptions=StatementListOptions;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ForeachStatementStart!=null) ForeachStatementStart.accept(visitor);
        if(ForeachParensContent!=null) ForeachParensContent.accept(visitor);
        if(StatementListOptions!=null) StatementListOptions.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ForeachStatementStart!=null) ForeachStatementStart.traverseTopDown(visitor);
        if(ForeachParensContent!=null) ForeachParensContent.traverseTopDown(visitor);
        if(StatementListOptions!=null) StatementListOptions.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ForeachStatementStart!=null) ForeachStatementStart.traverseBottomUp(visitor);
        if(ForeachParensContent!=null) ForeachParensContent.traverseBottomUp(visitor);
        if(StatementListOptions!=null) StatementListOptions.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ForeachStatement(\n");

        if(ForeachStatementStart!=null)
            buffer.append(ForeachStatementStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ForeachParensContent!=null)
            buffer.append(ForeachParensContent.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(StatementListOptions!=null)
            buffer.append(StatementListOptions.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ForeachStatement]");
        return buffer.toString();
    }
}
