package rs.ac.bg.etf.pp1.clss;

import java.util.Iterator;
import java.util.LinkedList;

import rs.etf.pp1.symboltable.concepts.Struct;

public class ClassDeclarationList extends LinkedList<ClassDeclaration> {

	private static final long serialVersionUID = 1L;
	
	private static ClassDeclarationList instance;
	public static ClassDeclarationList getInstance()
	{
		if ( instance == null )
			instance = new ClassDeclarationList();
		return instance;
	}
	
	public ClassDeclaration getClassDeclaration(String name)
	{
		Iterator<ClassDeclaration> it = iterator();
		ClassDeclaration c;
		while( it.hasNext() )
		{
			c = it.next();
			if ( c.getName().equals(name) )
				return c;
		};
		return null;
	}
	
	public Struct getClassType(String name)
	{
		ClassDeclaration d;
		if ( (d = getClassDeclaration(name)) != null )
			return d.getSymbolTableEntry().getType();
		
		return null;
	}
	
	public boolean isDerived( String baseClass, String subClass )
	{
		ClassDeclaration subDecl;
		if ( getClassDeclaration(baseClass) == null )
			return false;
		
		if ( ( subDecl = getClassDeclaration(subClass) ) == null )
			return false;
		
		ClassDeclaration current = subDecl;
		int i = 0;
		boolean b;
		while( current != null )
		{
			b = current.getName().equals( baseClass );
			if ( b && i > 0 )
				return true;
			else if ( b )
				return false;
			
			i++;
			current = current.getSuperClass();
		};
		
		return false;
	}
}
