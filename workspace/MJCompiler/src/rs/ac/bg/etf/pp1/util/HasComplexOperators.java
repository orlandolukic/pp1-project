package rs.ac.bg.etf.pp1.util;

import rs.ac.bg.etf.pp1.ast.*;

public class HasComplexOperators extends VisitorAdaptor {

	private boolean ignore = false;
	private int depth = 0;
	private boolean isComplex = false;
	
	public boolean cond()
	{
		return isComplex;
	}
	
	private void setDepth()
	{
		depth++;
		ignore = true;
	}
	
	private void resetDepth()
	{
		depth--;
		if ( depth == 0 )
			ignore = false;
	}
	
	public void visit( FuncCallStart s ) 
	{
		setDepth();
	}
	
	public void visit(FuncCall s )
	{
		resetDepth();
	}
	
	public void visit( FactorParensStart s ) 
	{
		setDepth();
	}
	
	public void visit( FactorParensEnd s )
	{
		resetDepth();
	}
	
	public void visit(MulopRightOperand mro)
	{
		if ( ignore )
			return;
		
		isComplex = true;
	}
	
	public void visit(AddopRightOperand aro)
	{
		if ( ignore )
			return;
		
		isComplex = true;
	}
	
	public void performCheck(SyntaxNode e)
	{
		isComplex = false;
		depth = 0;
		ignore = false;
		e.traverseBottomUp(this);
	}
}
