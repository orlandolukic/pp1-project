package rs.ac.bg.etf.pp1.context;

import java.util.LinkedList;

import rs.ac.bg.etf.pp1.ast.Relop;

public class ConditionCheckerContext {
	
	public boolean isAlwaysTrue;
	public boolean isAlwaysFalse;
	public boolean isConstantCondition;
	public boolean tempCondition;
	public int conditionTerm;
	public boolean condition;
	public int condFactNumber;
	public int condTermNumber;
	public boolean startFactCondition;
	public boolean startTermCondition;
	public boolean factCondition;
	public boolean termCondition;
	public boolean factConstantCondition;
	/**
	 * Depth of IFs inside if (...) { ... };
	 */
	public int depth;
	
	/**
	 * Variables used for code generation.
	 */
	public LinkedList<Integer> refactorElsePart;
	public LinkedList<Integer> refactorIfPart;
	public LinkedList<Integer> refactorNextCondition;
	public int addrConditionEnd;
	public int addrConditionStart;
	public boolean allowCodeGeneration;
	public ForLoopCheckerContext forLoopContext;
	public ForeachLoopCheckerContext foreachLoopContext;
	public boolean isInverse;
	public int addrStartExpr;
	public int relopOperator;
	public int relopFirstOperator;
	public boolean resetedNextCondition;
	public int ifDepth;
	public int forDepth;
	public int foreachDepth;
	
	public ConditionCheckerContext(boolean generate)
	{
		this();
		
		if ( generate )
		{
			refactorElsePart = new LinkedList<Integer>();
			refactorIfPart = new LinkedList<Integer>();
			refactorNextCondition = new LinkedList<Integer>();
			allowCodeGeneration = true;
			forLoopContext = null;
			isInverse = false;
			addrStartExpr = 0;
			resetedNextCondition = false;
			ifDepth = 0;
			forDepth = 0;
			foreachDepth = 0;
			relopFirstOperator = 0;
		};
	}
	
	public ConditionCheckerContext()
	{
		isAlwaysTrue = false;
		isAlwaysFalse = false;
		isConstantCondition = true;
		conditionTerm = 0;
		condition = false;
		condFactNumber = 0;
		condTermNumber = 0;
		factCondition = true;
		termCondition = false;
		startFactCondition = true;
		startTermCondition = false;
		factConstantCondition = true;
		depth = 0;
	}
	
	public boolean isAlwaysFalse()
	{
		return isConstantCondition && isAlwaysFalse;
	}
	
	public boolean isAlwaysTrue()
	{
		return isConstantCondition && isAlwaysTrue;
	}
	
	public void refreshNextConditionRefactorList()
	{
		refactorNextCondition = new LinkedList<Integer>();
	}
	
	public boolean isRequestCameFromFor()
	{
		return forLoopContext != null;
	}
	
}
