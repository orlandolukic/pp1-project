package rs.ac.bg.etf.pp1.des;

import java.util.Iterator;
import java.util.LinkedList;

import rs.ac.bg.etf.pp1.CodeGenerator;
import rs.ac.bg.etf.pp1.ExprChecker;
import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.clss.ClassDeclaration;
import rs.ac.bg.etf.pp1.clss.ClassField;
import rs.ac.bg.etf.pp1.clss.ClassFields;
import rs.ac.bg.etf.pp1.clss.ClassMethod;
import rs.ac.bg.etf.pp1.clss.ClassUtil;
import rs.ac.bg.etf.pp1.clss.MethodTable;
import rs.ac.bg.etf.pp1.context.DesignatorFinderContext;
import rs.ac.bg.etf.pp1.gen.ComplexOperator;
import rs.ac.bg.etf.pp1.gen.TD_Expr;
import rs.ac.bg.etf.pp1.print.DesignatorPrinter;
import rs.ac.bg.etf.pp1.util.CGExprContext;
import rs.ac.bg.etf.pp1.util.DesignatorFinderCGContext;
import rs.ac.bg.etf.pp1.util.HasComplexOperators;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class DesignatorFinder extends VisitorAdaptor {
	
	public static final int LEFT_SIDE = 1,
							RIGHT_SIDE = 2;
	
	private static int CNT = 0;
	
	protected SemanticAnalyzer sa;
	private Obj object;
	private Struct type;
	protected boolean toGenerate;
	private boolean check;
	protected Designator substitution;
	protected int factors;
	protected ExprChecker checker;
	protected boolean isArrayAccess;
	protected boolean isObjectAccess;
	protected Obj storeObj;
	protected boolean performLoadCheckAtTheEnd;
	protected Obj first;
	private int designators;
	protected boolean report;
	private LinkedList<DesignatorFinderCGContext> listOfContexts;
	private boolean loadField;
	protected int side;
	private boolean toLoadControl;
	private boolean isComplexOperator;
	
	protected DesignatorFinderContext designatorContext;
	
	public DesignatorFinder( SemanticAnalyzer sa )
	{
		this(sa, sa != null);
	}
	
	public DesignatorFinder( SemanticAnalyzer sa, boolean check )
	{
		reset(sa, check);
		toLoadControl = true;
		side = LEFT_SIDE;
	}
	
	protected void reset(SemanticAnalyzer sa, boolean check)
	{
		object = Tab.noObj;
		type = Tab.noType;
		this.sa = SemanticAnalyzer.__instance__();
		this.check = check;
		factors = 0;
		designatorContext = new DesignatorFinderContext();
		toGenerate = false;
		isArrayAccess = false;
		isObjectAccess = false;
		loadField = true;
		performLoadCheckAtTheEnd = false;
		first = null;
		designators = 0;		
		listOfContexts = new LinkedList<DesignatorFinderCGContext>();
		isComplexOperator = false;
	}
	
	public void setIsComplexOperator(boolean b)
	{
		isComplexOperator = b;
	}
	
	public void traverse( Designator node )
	{
		node.traverseBottomUp(this);
	}
	
	public void traverse( SyntaxNode node )
	{
		node.traverseBottomUp(this);
	}
	
	public void setSide(int side)
	{
		this.side = side;
	}
	
	public int getSide()
	{
		return side;
	}
	
	public void setToLoadControl(boolean val)
	{
		toLoadControl = val;
	}
	
	public boolean getToLoadControl()
	{
		return toLoadControl;
	}
	
	public Obj getObject()
	{
		return object == null ? Tab.noObj : object;
	}
	
	public Struct getType()
	{
		return type;
	}
	
	public String getVarName()
	{
		return object == null ? "" : object.getName();
	}
	
	public boolean isArrayAccess()
	{
		return isArrayAccess;
	}
	
	public boolean isObjectPropertyAccess()
	{
		return isObjectAccess;
	}
	
	public void setLoadField( boolean load )
	{
		this.loadField = load;
	}
	
	public void setLoadOperation(boolean isLoadOp)
	{
		performLoadCheckAtTheEnd = isLoadOp;
	}
	
	public String print( SyntaxNode stop )
	{
		return new DesignatorPrinter( substitution, stop ).toString();
	}
	
	public static String printDesignator( Designator d, SyntaxNode stop )
	{
		return new DesignatorPrinter( d, stop ).toString();
	}
	
	public void setCodeGeneration(boolean t)
	{
		reset(sa, false);
		toGenerate = t;
	}
	
	public boolean isMethod()
	{
		return designatorContext.isMethod;
	}
	
	public int getFactorsNum()
	{
		return factors + 1;
	}
	
	public Obj getFirstFactor()
	{
		return first;
	}
	
	public void resetDesignatorFinder(boolean toGenerate)
	{
		reset(sa, !toGenerate && check);
		this.toGenerate = toGenerate;
	}
	
	public void setCheck( boolean check )
	{
		this.check = check;
	}
	
	public void setReport(boolean report)
	{
		this.report = report;
	}
	
	protected boolean check()
	{
		return check;
	}
	
	protected SyntaxNode node()
	{
		return substitution;
	}
	
	protected void terminate( String mssg )
	{
		checker.setError( ExprChecker.ERROR , mssg );
		designatorContext.stop = true;
	}
	
	protected void warn( String mssg )
	{
		checker.setError( ExprChecker.WARNING , mssg );
		designatorContext.stop = true;
	}	
	
	protected boolean isFromFactorDesignator()
	{
		return substitution.getParent() instanceof FactorDesignator;
	}
	
	protected boolean hasFactorParensAfter()
	{
		if ( substitution.getParent() instanceof FactorDesignator )
		{
			FactorDesignator fd = (FactorDesignator) substitution.getParent();
			if ( fd.getFactorParens() instanceof FactorParensPresent )
				return true;
		}
		return false;
	}
	
	public boolean isClassReference()
	{
		return designatorContext.classDeclaration != null;
	}
	
	public boolean isReference()
	{
		return object.getType().isRefType();
	}
	
	public rs.ac.bg.etf.pp1.clss.ClassDeclaration getClassReference()
	{
		return designatorContext.classDeclaration;
	}
	
	public void storeVariable()
	{
		storeVariable(false);
	}
	
	public void storeVariable( boolean isFirst )
	{
		DesignatorFinderCGContext c;
		if ( isFirst )
			c = listOfContexts.getFirst();
		else
			 c = listOfContexts.getLast();
		
		Functions.storeVariable(c.obj, c.isArrayAccess());
	}
	
	public void visit( Designator d )
	{
		designators--;
		if ( designators == 0 )
		{			
			if ( !designatorContext.stop )
			{
				designatorBeforeFinished();
				designatorFinishedCodeGeneration();
				designatorSecurityCodeGenerationCheck();
				designatorFinished();
			};
			object = designatorContext.current;
		};
	}
	
	public void visit( DesignatorStart start )
	{
		designators++;
		if ( designators > 1 )
			return;
		
		boolean addThis = false;
		boolean toLoad = toLoadControl;
		substitution = (Designator) start.getParent();
		String var = start.getVarName();
		Obj o = Tab.find( var );
		
		designatorContext.setNode( substitution );
		designatorContext.current = o;
		designatorContext.type = o.getType();
		first = o;		
	
		if ( var.equals("this") )
		{
			if ( !sa.classIsInside )
			{
				if ( check )
					sa.report_error("Cannot use 'this' reference outside of the class", start);
				designatorContext.stop = true;
			} else
			{
				designatorContext.classDeclaration = sa.classInstance;				
			};
		} else if ( var.equals( "null" ) )
		{
			designatorContext.isNull = true;
		} else
		{
			if ( !sa.classIsInside )
			{			
				if ( o == Tab.noObj )
				{
					if ( check )
						sa.report_error( undefinedVariableMessage() , start );
					designatorContext.stop = true;
				} else
				{					
					designatorContext.current = o;
					designatorContext.type = o.getType();
					designatorContext.isMethod = o.getKind() == Obj.Meth;
					toLoad = side == RIGHT_SIDE;
					
					if ( o.getType().getKind() == Struct.Class )
						designatorContext.classDeclaration = ClassUtil.getClassDeclarationFromObj(o);
				};
			} else			// Finding designator in the class...
			{				
				boolean classFieldExists = false;
				boolean methodExists = false;					
				
				// Check if method with given name exists.
				ClassDeclaration d = null;
				d = ClassUtil.getClassDeclarationFromObj(o);
				if ( d == null )
					d = sa.classInstance;
				designatorContext.isMethod = false;		
				MethodTable tbl = new MethodTable(d);
				tbl.initMethodTable(d, start);
				ClassMethod meth;
				if ( (meth = tbl.methodExists(var)) != null )
				{						
					methodExists = true;
					addThis = true;	
					toLoad = false;
					designatorContext.classDeclaration = d;
					designatorContext.isMethod = true;						
					designatorContext.current = meth.getObj();
					designatorContext.type = meth.getObj().getType();						
				};
				
				ClassField field;
				
				// Check class fields.
				if ( !methodExists )
				{	
					// Check method params.
					boolean methodVarExists = false;
					Obj obj = sa.classCurrentMethod.getObj();
					Iterator<Obj> it = obj.getLocalSymbols().iterator();
					while( it.hasNext() && !methodVarExists )
					{
						obj = it.next();
						if ( obj.getName().equals( var ) )
						{
							designatorContext.current = obj;
							designatorContext.type = object.getType();
							methodVarExists = true;	
							toLoad = side == RIGHT_SIDE && toLoadControl;
							if ( designatorContext.current.getType().getKind() == Struct.Class )
								designatorContext.classDeclaration = ClassUtil.getClassDeclarationFromObj(obj); 
						};
					};	
					
					if ( !methodVarExists )
					{
						boolean fieldExists = checkField( sa.classInstance, var, start );
						boolean fieldVisible = (field = checkFieldVisibility(sa.classInstance, var, start)) != null;
						classFieldExists = fieldExists && fieldVisible;
						if ( sa.passed() )
						{
							if ( check && !fieldExists )
							{
								if ( o == Tab.noObj )
								{
									sa.report_error( "Variable '" + var + "' is not declared within class '" + sa.classInstance.getName() + "'", start );
									designatorContext.stop = true;
								};
							} else if ( check && !fieldVisible )
							{
								sa.report_error( "Variable '" + var + "' is not visible within the context", start );
								designatorContext.stop = true;
							} else if ( classFieldExists )
							{								
								designatorContext.current = field.getCodeGenerationObj();
								designatorContext.type = field.getCodeGenerationObj().getType();
								addThis = true;
								if ( field.getClassField().isClassReference() )
									designatorContext.classDeclaration = d;
							} else
							{
								// Check symbol table.
								if ( o == Tab.noObj )
								{
									sa.report_error( "Variable '" + var + "' is not declared within class '" + sa.classInstance.getName() + "'", start );
									designatorContext.stop = true;
								};
							};
						};
					};
				};					
				
			};
		};
		
	
		if ( !designatorContext.stop )
		{	
			/**
			 * Add 'this' variable to the list.
			 */
			if ( addThis )
				listOfContexts.add( new DesignatorFinderCGContext( Functions.thisVariable(), designatorContext.classDeclaration, true, start, true ) );
			
			listOfContexts.add( new DesignatorFinderCGContext( designatorContext.current, designatorContext.classDeclaration, toLoad, start ) );
		};
	}
	
	/**
	 * =====================================================================================================
	 * 		ARRAY ACCESS
	 * =====================================================================================================
	 */
	public void visit( DesignatorArrayAccess arrAccess )
	{
		if ( designatorContext.stop || designators > 1 )
			return;
		
		if ( designatorContext.isNull )
		{
			if ( check )
				sa.report_error( "Cannot designate an array element of null object reference", arrAccess );
			designatorContext.stop = true;
			return;
		};
		
		isArrayAccess = true;
		isObjectAccess = false;
		designatorContext.arrAccess++;
		factors++;
		designatorContext.classDeclaration = null;		
		designatorContext.isMethod = false;
	
		if ( designatorContext.current.getKind() == Obj.Con )
		{
			if ( check )
				sa.report_error("Cannot use array index brackets [] on constant '" + print(arrAccess) + "' of type '" 
						+ Functions.getVariableTypeAsString(designatorContext.current.getType()) + "'", arrAccess);
			designatorContext.stop = true;
		} else if ( designatorContext.current.getKind() == Obj.Var && designatorContext.current.getType().getKind() != Struct.Array )
		{
			if ( check )
				sa.report_error("Cannot use array index brackets [] on variable '" + print(arrAccess) + "' of type '" 
						+ Functions.getVariableTypeAsString(designatorContext.current.getType()) + "'", arrAccess);
			designatorContext.stop = true;
		} else
		{
			// Check array index.	
			ExprChecker ec = new ExprChecker(Tab.intType);
			boolean valid = ec.process((ExprNoError) arrAccess.getExpr());
			if ( !valid )
			{
				if ( ec.getRaisedErrorType() == ExprChecker.ERROR )
				{
					if ( check )
						sa.report_error(ec.getMessage(), substitution);
					designatorContext.stop = true;
					return;
				} else 
					if ( check )
						sa.report_warning(ec.getMessage(), substitution);
			};
			
			// Check value from array's index.
			try {
				String expr = ExprChecker.getExprAsString( arrAccess.getExpr() );
				double b = Functions.eval(expr);
				if ( b < 0 )
				{
					if ( check )
						sa.report_error("Cannot access negative array index (" + ((int)(b)) + ")", substitution);
					designatorContext.stop = true;
				};
			} catch( Exception e ) {
				sa.report_warning(e.getMessage(), substitution);
			};
			
			if ( report && designatorContext.arrAccess == 1 && !designatorContext.stop && check )
			{
				sa.report_info("Array access of variable " + print(arrAccess), substitution);
			};
		};			
		
		// Determine struct for array variable.
		Struct t = null, s = null, x;
		if ( designatorContext.current.getKind() == Obj.Con )
		{
			t = designatorContext.type;
			s = t;
		} else if ( designatorContext.current.getType().getKind() == Struct.Array )
		{
			s = designatorContext.current.getType();
			t = s.getElemType();
			if ( t.getKind() == Struct.Class )
			{
				designatorContext.classDeclaration = ClassUtil.getClassDeclarationFromObj( designatorContext.current );
				x = ClassUtil.getStructForClassname( ClassUtil.getClassNameFromStruct(t) , false);				
				t = x;
			};
		} else if ( designatorContext.current.getType().getKind() == Struct.Class )
		{
			designatorContext.classDeclaration = ClassUtil.getClassDeclarationFromObj( designatorContext.current );
			t = ClassUtil.getStructForClassname( ClassUtil.getClassNameFromObj(designatorContext.current) , false);
			s = t;
		};
		
		String str = getTempVarName();
		designatorContext.current = new Obj( Obj.Var, str, t );
		
		if ( !designatorContext.stop )
		{	
			listOfContexts.getLast().toLoad = true;			
			
			DesignatorFinderCGContext c = new DesignatorFinderCGContext( designatorContext.current, designatorContext.classDeclaration, true, arrAccess );			
			listOfContexts.add( c );
		};
	}
	
	/**
	 * =====================================================================================================
	 * 		OBJECT ACCESS
	 * =====================================================================================================
	 */
	public void visit( DesignatorObjectAccess objAccess )
	{
		if ( designatorContext.stop || designators > 1 )
			return;
		
		if ( designatorContext.isNull )
		{
			if ( check )
				sa.report_error( "Cannot designate object field '" + objAccess.getI1() + "' of null object reference", objAccess );
			designatorContext.stop = true;
			return;
		};
		
		String ident = objAccess.getI1();
		factors++;
		isArrayAccess = false;
		isObjectAccess = true;
		designatorContext.arrAccess = 0;
			
		if ( designatorContext.classDeclaration == null )
		{
			if ( check )
				sa.report_error("Cannot use .(dot) on variable '" + print(objAccess) + "' of type '" 
						+ Functions.getVariableTypeAsString( designatorContext.current.getType() ) + "'", objAccess);
			designatorContext.stop = true;
		} else
		{	
			ClassDeclaration d = designatorContext.classDeclaration;				
			d = designatorContext.classDeclaration;
			String clssname = designatorContext.classDeclaration.getName();
			
			boolean fieldExists = false;				
			boolean methodExists = false;
			
			// Check if method with given name exists.					
			designatorContext.isMethod = false;
			MethodTable tbl;
			if ( sa.classIsInside )
			{
				tbl = new MethodTable(d);				
				tbl.initMethodTable(d, objAccess);
			} else
				tbl = d.getMethodTable();
			
			ClassMethod meth;
			if ( (meth = tbl.methodExists(ident)) != null )
			{
				methodExists = true;
				designatorContext.classDeclaration = d;
				designatorContext.isMethod = true;						
				designatorContext.current = meth.getObj();
				designatorContext.type = meth.getObj().getType();	
				
				// Check method visibility permission.
				if ( sa.classInstance != d && meth.getAccessType() != ClassMethod.PUBLIC )
				{
					if ( check )
						sa.report_error("Method '" + meth.getMethodName() + "' is not visibile within the context", objAccess);
					designatorContext.stop = true;
				};
			};
			
			if ( !methodExists )
			{
				ClassField field = null, f;
				field = ClassFields.fieldExists( d, ident );
				// Check field access point.
				fieldExists = field != null;
				if ( fieldExists )
				{
					f = checkFieldVisibility(d, ident, objAccess);
					if ( f == null )
					{
						if ( check )
							sa.report_error("Field '" + print(objAccess) + "' is not accessable within the context", objAccess);
						designatorContext.stop = true;
					} else
					{
						designatorContext.current = field.getCodeGenerationObj();
						designatorContext.type = field.getCodeGenerationObj().getType();	
						if ( designatorContext.current.getType().getKind() == Struct.Class )
							designatorContext.classDeclaration = ClassUtil.getClassDeclarationFromObj(designatorContext.current);						
					};
				} else
				{
					if ( !sa.passed() )
						return;
					
					if ( check )
						sa.report_error("Name '" + ident + "' is not field not method inside the class '" + clssname + "'", objAccess);
					designatorContext.stop = true;
				};
			};
		};	
		
		if ( !designatorContext.stop )
		{			
			listOfContexts.getLast().toLoad = true;
			listOfContexts.add( new DesignatorFinderCGContext( designatorContext.current, designatorContext.classDeclaration, true, objAccess ) );
		};
	}
	
	private ClassField checkFieldVisibility( ClassDeclaration decl, String ident, SyntaxNode node )
	{
		if ( decl == null )
			return null;
			
		ClassField field;
		Obj x = Tab.find(ident);
		if ( x != Tab.noObj && x.getKind() == Obj.Con )
			return null;
		
		if ( (field = ClassFields.fieldExists(decl, ident)) != null 
				&& 
			ClassFields.isVisibleField( decl, ident, decl == sa.classInstance ) )
		{			
			return field;
		};
		
		return null;
	}
	
	private boolean checkField( ClassDeclaration decl, String ident, SyntaxNode node )
	{
		if ( decl == null )
			return false;
			
		Obj x = Tab.find(ident);
		if ( x != Tab.noObj && x.getKind() == Obj.Con )
			return false;
		
		if ( ClassFields.fieldExists(decl, ident) == null )
			return false;
		
		return true;
	}
	
	protected String undefinedVariableMessage()
	{
		return "Variable '" + print(substitution.getDesignatorStart()) + "' is not defined";
	}
	
	protected void designatorStartVariable( DesignatorFinderCGContext c ) 
	{		
		Code.load( c.obj );
	}
	
	protected void designatorSecurityCodeGenerationCheck() 
	{
		
		/*
		if ( !toGenerate || !performLoadCheckAtTheEnd || factors > 0 )
			return;
		
		Obj var = designatorContext.current;
		boolean isArray = var.getType().getKind() == Struct.Array;
		
		if ( !(var.getKind() == Obj.Con || var.getKind() == Obj.Fld || var.getKind() == Obj.Var) )
			return;
			
		switch( var.getKind() )
		{
		case Obj.Con:
			Code.load(var);
			break;
			
		case Obj.Fld:			
			if ( loadField )
				Code.load(var);
			break;
			
		case Obj.Var:
			switch( var.getType().getKind() )
			{
			case Struct.Int:
			case Struct.Char:
			case Struct.Bool:
			case Struct.Array:
				// Load variable.
				Code.load( var );
				if ( isArray )
					designatorSecurityIsArrayAccess();
				else
					designatorSecurityNotArrayAccess();
				break;
			};
			break;
		}
		*/		
	};
	
	protected void designatorSecurityIsArrayAccess() {}
	protected void designatorSecurityNotArrayAccess() {}
	
	protected void designatorArrayAccess(DesignatorFinderCGContext r) 
	{		
		DesignatorArrayAccess d = (DesignatorArrayAccess) r.node;		
			
		HasComplexOperators hco = new HasComplexOperators();
		ComplexOperator coGenerator = new ComplexOperator();
		TD_Expr vis = new TD_Expr();
		
		ExprNoError ex = (ExprNoError) d.getExpr();		
		hco.performCheck(ex);
		if ( !hco.cond() )
		{
			vis.createNewContext(CGExprContext.SIDE_LEFT);
			vis.visit(ex);
		} else
		{
			if ( Functions.DEBUG )
				System.out.println("Array has complex operators within index");
			coGenerator.generate(ex);
		};		
	}
	
	protected void designatorObjectPropertyAccess(DesignatorFinderCGContext r) 
	{
		if ( !r.ok )
			return;
		
		DesignatorObjectAccess d = (DesignatorObjectAccess) r.node;
		
		Obj o;
		if ( r.decl == null )
		{
			o = Tab.find( d.getI1() );
			Code.load(o);
		} else
		{
			if ( r.isLast && isComplexOperator )
				Code.put( Code.dup );
			Code.put( Code.getfield );
			Code.put2( r.obj.getAdr() );
		};
		
	}	
	
	protected void designatorFinished() {};
	
	protected void designatorFinishedCodeGeneration()
	{
		if ( !toGenerate )
			return;

		if ( !designatorContext.isMethod )
			generateVariable();
		
		if ( Functions.DEBUG )
			System.out.println("Generate basic expression: " + print(null));
	}
	
	private void designatorBeforeFinished()
	{		
	
	}
	
	private String getTempVarName()
	{
		return "temp"+(++CNT);
	}
	
	public void generateThroughTheList( boolean laodLast )
	{
		generateThroughTheList(laodLast, false);
	}
	
	public void generateThroughTheList( boolean loadLast, boolean forceLoad )
	{
		Iterator<DesignatorFinderCGContext> it = listOfContexts.iterator();
		DesignatorFinderCGContext o = null;
		int i = 0;
		int len = listOfContexts.size();
		boolean ok = true;
		while( it.hasNext() && ok )
		{
			o = it.next();
			o.isLast = i+1==len;
			o.ind = i;
			
			if ( !forceLoad && !o.toLoad )
				continue;
			
			if ( !loadLast && i > 0 && !it.hasNext() )
				o.ok = ok = false;
			
			if ( o.node == null )
			{
				if ( !o.ok )
					break;
				designatorStartVariable( o );				
			} else if ( o.node instanceof DesignatorStart )
			{
				if ( !o.ok )
					break;
				designatorStartVariable( o );
				
			} else if ( o.node instanceof DesignatorArrayAccess )
			{
				designatorArrayAccess( o );
				if ( !o.ok )
					break;
				
				// Check if array is the last factor.
				if ( isComplexOperator && o.isLast )
					Code.put( Code.dup2 );
				
				if ( side == LEFT_SIDE )
					Code.load( Functions.constantElem( o.obj.getType() ) );
			} else if ( o.node instanceof DesignatorObjectAccess )
			{
				if ( !o.ok )
					break;
				designatorObjectPropertyAccess( o );
			};
			o.ok = true;
			i++;
		};
		
		if ( o != null )
			o.ok = true;
	}
	
	/**
	 * Load everything: load last + force load.
	 */
	public void generateVariable()
	{
		generateThroughTheList(true, true);
	}
	
	public boolean isPrimitiveVariable()
	{
		return listOfContexts.size() == 1;
	}
	
	
}
