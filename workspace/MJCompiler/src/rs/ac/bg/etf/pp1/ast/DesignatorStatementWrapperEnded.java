// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class DesignatorStatementWrapperEnded extends Matched {

    private DesignatorStatementWrapper DesignatorStatementWrapper;

    public DesignatorStatementWrapperEnded (DesignatorStatementWrapper DesignatorStatementWrapper) {
        this.DesignatorStatementWrapper=DesignatorStatementWrapper;
        if(DesignatorStatementWrapper!=null) DesignatorStatementWrapper.setParent(this);
    }

    public DesignatorStatementWrapper getDesignatorStatementWrapper() {
        return DesignatorStatementWrapper;
    }

    public void setDesignatorStatementWrapper(DesignatorStatementWrapper DesignatorStatementWrapper) {
        this.DesignatorStatementWrapper=DesignatorStatementWrapper;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DesignatorStatementWrapper!=null) DesignatorStatementWrapper.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DesignatorStatementWrapper!=null) DesignatorStatementWrapper.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DesignatorStatementWrapper!=null) DesignatorStatementWrapper.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DesignatorStatementWrapperEnded(\n");

        if(DesignatorStatementWrapper!=null)
            buffer.append(DesignatorStatementWrapper.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DesignatorStatementWrapperEnded]");
        return buffer.toString();
    }
}
