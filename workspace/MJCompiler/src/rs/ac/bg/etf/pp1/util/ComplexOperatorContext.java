package rs.ac.bg.etf.pp1.util;

import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.symboltable.concepts.*;

public class ComplexOperatorContext {
	
	public int depth;
	public boolean isMulop;
	public boolean isAddop;
	public Mulop mulop;
	public Addop addop;
	public SyntaxNode snode;
	public Obj entry;
	
	public ComplexOperatorContext(ComplexOperatorContext cc)
	{
		this();
		if ( cc != null )
		{
			this.depth = cc.depth + 1;
			//depth *= depth *= depth *= depth += (depth*2) *= 2;
		};
	}
	
	public ComplexOperatorContext()
	{
		this.depth = 0;
		this.isAddop = false;
		this.isMulop = false;
		this.entry = null;
	}
}
