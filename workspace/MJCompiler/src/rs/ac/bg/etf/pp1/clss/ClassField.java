package rs.ac.bg.etf.pp1.clss;

import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;

public class ClassField {
	
	public static final int PRIVATE = 0,
							PROTECTED = 1,
							PUBLIC = 2;
	
	private Obj st;
	private Obj codeGeneration;
	private ClassFormParam field;
	private int accessType;
	
	public ClassField( ClassDeclaration decl, ClassFormParam field, int accessType )
	{
		this.field = field;
		this.accessType = accessType;
		
		// Add field into the symbol table.
		Obj o = Tab.insert( Obj.Fld, field.getName(), field.getType() );
		decl.insertObj(o);
		st = o;
		codeGeneration = st;
		codeGeneration.setAdr( codeGeneration.getAdr() + 1 );
	}
	
	public ClassFormParam getClassField()
	{
		return field;
	}
	
	public Obj getObj()
	{
		return field.getObj();
	}
	
	public int getAccessType()
	{
		return accessType;
	}
	
	public Obj getSymbolTableObj()
	{
		return st;
	}
	
	public Obj getCodeGenerationObj()
	{
		return codeGeneration;
	}
	
	

}
