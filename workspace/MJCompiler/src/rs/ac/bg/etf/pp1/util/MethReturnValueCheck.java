package rs.ac.bg.etf.pp1.util;

import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.ast.VisitorAdaptor;

public class MethReturnValueCheck extends VisitorAdaptor {
	
	private boolean hasReturnStatement = false;
	
	public boolean hasReturnStatement()
	{
		return this.hasReturnStatement;
	}
	
	public void visit(ReturnExpr retExp)
	{
		hasReturnStatement = true;
	}
}
