// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class ClassDeclMethodsMore extends ClassDeclMethods {

    private ClassDeclMethods ClassDeclMethods;
    private ClassDeclMethod ClassDeclMethod;

    public ClassDeclMethodsMore (ClassDeclMethods ClassDeclMethods, ClassDeclMethod ClassDeclMethod) {
        this.ClassDeclMethods=ClassDeclMethods;
        if(ClassDeclMethods!=null) ClassDeclMethods.setParent(this);
        this.ClassDeclMethod=ClassDeclMethod;
        if(ClassDeclMethod!=null) ClassDeclMethod.setParent(this);
    }

    public ClassDeclMethods getClassDeclMethods() {
        return ClassDeclMethods;
    }

    public void setClassDeclMethods(ClassDeclMethods ClassDeclMethods) {
        this.ClassDeclMethods=ClassDeclMethods;
    }

    public ClassDeclMethod getClassDeclMethod() {
        return ClassDeclMethod;
    }

    public void setClassDeclMethod(ClassDeclMethod ClassDeclMethod) {
        this.ClassDeclMethod=ClassDeclMethod;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ClassDeclMethods!=null) ClassDeclMethods.accept(visitor);
        if(ClassDeclMethod!=null) ClassDeclMethod.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ClassDeclMethods!=null) ClassDeclMethods.traverseTopDown(visitor);
        if(ClassDeclMethod!=null) ClassDeclMethod.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ClassDeclMethods!=null) ClassDeclMethods.traverseBottomUp(visitor);
        if(ClassDeclMethod!=null) ClassDeclMethod.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ClassDeclMethodsMore(\n");

        if(ClassDeclMethods!=null)
            buffer.append(ClassDeclMethods.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ClassDeclMethod!=null)
            buffer.append(ClassDeclMethod.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ClassDeclMethodsMore]");
        return buffer.toString();
    }
}
