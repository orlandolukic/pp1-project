package rs.ac.bg.etf.pp1.gen;

import java.util.Iterator;
import java.util.LinkedList;

import rs.ac.bg.etf.pp1.util.ComplexOperatorContext;
import rs.ac.bg.etf.pp1.util.Stack;
import rs.ac.bg.etf.pp1.ast.*;

public class T_ComplexOperator extends VisitorAdaptor {
	
	private SyntaxNode startNode;
	private Stack<Boolean> stack;
	private boolean stop;
	private boolean foundStartNode;
	private boolean ignore;
	private int funcCalls;
	private LinkedList<SyntaxNode> list;
	private LinkedList<ComplexOperatorContext> listContextLeft;
	private LinkedList<ComplexOperatorContext> listContextRight;
	
	public T_ComplexOperator( SyntaxNode startNode )
	{
		this.startNode = startNode;
		this.foundStartNode = false;
		this.stop = false;
		this.list = new LinkedList<SyntaxNode>();
		this.stack = new Stack<Boolean>();
		this.ignore = false;
		this.funcCalls = 0;
		this.listContextLeft = new LinkedList<ComplexOperatorContext>();
		this.listContextRight = new LinkedList<ComplexOperatorContext>();
	}
	
	public SyntaxNode[] getAllNodes()
	{
		SyntaxNode[] arr = new SyntaxNode[list.size()];
		Iterator<SyntaxNode> it = list.iterator();
		int i=0;
		while( it.hasNext() )
			arr[i++] = it.next();

		return arr;
	}
	
	public ComplexOperatorContext[] getAllOperationsOnLeftSide()
	{
		ComplexOperatorContext[] arr = new ComplexOperatorContext[listContextLeft.size()];
		Iterator<ComplexOperatorContext> it = listContextLeft.iterator();
		int i=0;
		while( it.hasNext() )
			arr[i++] = it.next();

		return arr;
	}
	
	public ComplexOperatorContext[] getAllOperationsOnRightSide()
	{
		ComplexOperatorContext[] arr = new ComplexOperatorContext[listContextRight.size()];
		Iterator<ComplexOperatorContext> it = listContextRight.iterator();
		int i=0;
		while( it.hasNext() )
			arr[i++] = it.next();

		return arr;
	}
	
	public void visit( AddopRightOperand a )
	{	
		if ( this.stack.size() == 0 && !foundStartNode )
		{
			ComplexOperatorContext c = new ComplexOperatorContext();
			c.isAddop = true;
			c.addop = (Addop) a;
			listContextLeft.add(c);
		};
	}
	
	public void visit(AddopLeftOperand a)
	{
		stop = true;
		if ( this.stack.size() == 0 )
		{
			ComplexOperatorContext c = new ComplexOperatorContext();
			c.isAddop = true;
			c.addop = (Addop) a;
			listContextRight.add(c);
		};
	}
	
	public void visit( MulopRightOperand m )
	{
		if ( this.stack.size() == 0 && !foundStartNode )
		{
			ComplexOperatorContext c = new ComplexOperatorContext();
			c.isMulop = true;
			c.mulop = (Mulop) m;
			listContextLeft.add(c);
		};
	}
	
	public void visit(MulopLeftOperand m)
	{
		stop = true;
		if ( this.stack.size() == 0 )
		{
			ComplexOperatorContext c = new ComplexOperatorContext();
			c.isMulop = true;
			c.mulop = (Mulop) m;
			listContextRight.add(c);
		};
	}
	
	public void visit(DesignatorOptStart dos)
	{
		this.stack.push(new Boolean(this.stop));
		this.stop = false;
	}
	
	public void visit(DesignatorOptEnd doe)
	{
		Boolean b = this.stack.pop();
		this.stop = b.booleanValue();
	}
	
	public void visit(FactorParensStart f)
	{
		funcCalls++;
		ignore = true;
	}
	
	public void visit(FactorParensEnd f)
	{
		funcCalls--;
		if ( funcCalls == 0 )
			ignore = false;
	}
	
	public void visit(FactorDesignator fd)
	{
		if ( stop || this.stack.size() > 0 || ignore )
			return;

		if ( fd != startNode )
			this.list.add(fd);
		else
			foundStartNode = true;
	}
}
