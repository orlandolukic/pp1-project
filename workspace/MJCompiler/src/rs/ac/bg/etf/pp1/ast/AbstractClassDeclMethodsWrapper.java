// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class AbstractClassDeclMethodsWrapper implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private AbstractClassDeclMethods AbstractClassDeclMethods;

    public AbstractClassDeclMethodsWrapper (AbstractClassDeclMethods AbstractClassDeclMethods) {
        this.AbstractClassDeclMethods=AbstractClassDeclMethods;
        if(AbstractClassDeclMethods!=null) AbstractClassDeclMethods.setParent(this);
    }

    public AbstractClassDeclMethods getAbstractClassDeclMethods() {
        return AbstractClassDeclMethods;
    }

    public void setAbstractClassDeclMethods(AbstractClassDeclMethods AbstractClassDeclMethods) {
        this.AbstractClassDeclMethods=AbstractClassDeclMethods;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(AbstractClassDeclMethods!=null) AbstractClassDeclMethods.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(AbstractClassDeclMethods!=null) AbstractClassDeclMethods.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(AbstractClassDeclMethods!=null) AbstractClassDeclMethods.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractClassDeclMethodsWrapper(\n");

        if(AbstractClassDeclMethods!=null)
            buffer.append(AbstractClassDeclMethods.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractClassDeclMethodsWrapper]");
        return buffer.toString();
    }
}
