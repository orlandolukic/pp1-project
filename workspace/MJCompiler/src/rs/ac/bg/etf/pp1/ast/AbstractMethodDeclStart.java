// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class AbstractMethodDeclStart implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private AccessRights AccessRights;
    private MethodReturnValue MethodReturnValue;
    private String I3;

    public AbstractMethodDeclStart (AccessRights AccessRights, MethodReturnValue MethodReturnValue, String I3) {
        this.AccessRights=AccessRights;
        if(AccessRights!=null) AccessRights.setParent(this);
        this.MethodReturnValue=MethodReturnValue;
        if(MethodReturnValue!=null) MethodReturnValue.setParent(this);
        this.I3=I3;
    }

    public AccessRights getAccessRights() {
        return AccessRights;
    }

    public void setAccessRights(AccessRights AccessRights) {
        this.AccessRights=AccessRights;
    }

    public MethodReturnValue getMethodReturnValue() {
        return MethodReturnValue;
    }

    public void setMethodReturnValue(MethodReturnValue MethodReturnValue) {
        this.MethodReturnValue=MethodReturnValue;
    }

    public String getI3() {
        return I3;
    }

    public void setI3(String I3) {
        this.I3=I3;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(AccessRights!=null) AccessRights.accept(visitor);
        if(MethodReturnValue!=null) MethodReturnValue.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(AccessRights!=null) AccessRights.traverseTopDown(visitor);
        if(MethodReturnValue!=null) MethodReturnValue.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(AccessRights!=null) AccessRights.traverseBottomUp(visitor);
        if(MethodReturnValue!=null) MethodReturnValue.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractMethodDeclStart(\n");

        if(AccessRights!=null)
            buffer.append(AccessRights.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodReturnValue!=null)
            buffer.append(MethodReturnValue.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(" "+tab+I3);
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractMethodDeclStart]");
        return buffer.toString();
    }
}
