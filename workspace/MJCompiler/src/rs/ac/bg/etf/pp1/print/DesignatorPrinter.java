package rs.ac.bg.etf.pp1.print;

import rs.ac.bg.etf.pp1.ast.*;

public class DesignatorPrinter extends Printer<Designator> {

	private SyntaxNode stopNode;
	
	public DesignatorPrinter(Designator snode) {
		super(snode);
	}
	
	public DesignatorPrinter(Designator snode, SyntaxNode stopNode) {
		this(snode);
		this.stopNode = stopNode;
	}

	@Override
	public void print() {		
		
		DesignatorVisitor v = new DesignatorVisitor(str, this.stopNode);
		snode.traverseBottomUp(v);
		
	}
	
	/**
	 * Visitor class for designator statement.
	 */
	private class DesignatorVisitor extends VisitorAdaptor
	{
		private boolean active;
		private StringBuilder str;
		private SyntaxNode stopNode;
		private int designators;
		
		public DesignatorVisitor(StringBuilder str, SyntaxNode stopNode) 
		{
			this.str = str;
			active = true;
			this.stopNode = stopNode;
			designators = 0;
		}
		
		private void checkFound( SyntaxNode given )
		{
			if ( stopNode != null && stopNode == given )
				active = false;
		}
		
		private boolean isFound( SyntaxNode node )
		{
			checkFound(node);
			return !active;
		}
		
		public void visit(DesignatorStart start)
		{
			if ( isFound(start) )
				return;
			
			str.append( start.getVarName() );
		}
		
		public void visit(DesignatorOptStart s)
		{
			isFound( s.getParent() );
		}
		
		public void visit( DesignatorArrayAccess arrAccess )
		{
			if ( isFound(arrAccess) )
				return;
			
			str.append( '[' );
			str.append( new ExprPrinter( (ExprNoError) arrAccess.getExpr() ) );
			str.append( ']' );
		}
		
		public void visit( DesignatorObjectAccess objAccess )
		{	
			if ( !active )
				return;
			
			str.append( '.' );
			str.append( objAccess.getI1() );
			
			isFound(objAccess);			
		}
	}

}
