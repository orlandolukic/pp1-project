package rs.ac.bg.etf.pp1.clss;

import java.util.Iterator;
import java.util.LinkedList;

import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class ClassFields {
	
	public static boolean isVisibleField(ClassDeclaration decl, String name)
	{
		return isVisibleField(decl, name, true);
	}
	
	public static boolean isVisibleField(ClassDeclaration decl, String name, boolean insideClass)
	{
		ClassDeclaration d = decl;
		ClassField f;
		
		if ( (f = d.getAllFields().getField(name)) != null )
			return insideClass || f.getAccessType() == ClassField.PUBLIC;
		
		d = d.getSuperClass();
		while( d != null )
		{
			f = d.getAllFields().getField(name);
			if ( f == null )
			{
				d = d.getSuperClass();
				continue;
			};
			
			if ( f.getAccessType() == ClassField.PRIVATE )
				break;
			
			if ( f.getAccessType() >= ClassField.PROTECTED )
				return insideClass;
			
			d = d.getSuperClass();
		};		
		
		return false;
	}
	
	public static ClassField fieldExists(ClassDeclaration decl, String name)
	{
		ClassDeclaration d = decl;
		ClassField f;
		
		if ( (f = d.getAllFields().getField(name)) != null )
			return f;
		
		d = d.getSuperClass();
		while( d != null )
		{
			f = d.getAllFields().getField(name);
			if ( f != null )				
				return f;
			
			
			d = d.getSuperClass();
		};		
		
		return null;
	}

	private ClassDeclaration declaration;
	private LinkedList<ClassField> list;
	
	public ClassFields( ClassDeclaration declaration )
	{
		list = new LinkedList<ClassField>();
		this.declaration = declaration;
	}
	
	/**
	 * Checks if field exists within class fields.
	 * @param name Name of the field.
	 * @return indicator whether field is found
	 */
	public boolean fieldExists( ClassFormParam field )
	{
		ClassField retval = null;
		Iterator<ClassField> it = list.iterator();		
		while( it.hasNext() )
		{
			retval = it.next();
			if ( retval.getClassField().equalsClassField(field) )
				return true;
		};		
		return false;
	}
	
	/**
	 * Checks if field exists within class fields.
	 * @param obj Symbol Table node.
	 * @return indicator whether field is found
	 */
	public boolean fieldExists( Obj obj )
	{
		ClassField retval = null;
		Iterator<ClassField> it = list.iterator();		
		while( it.hasNext() )
		{
			retval = it.next();
			if ( retval.getObj().equals(obj) )
				return true;
		};		
		return false;
	}
	
	/**
	 * Checks if field exists within class fields.
	 * @param name Name of the field.
	 * @return indicator whether field is found
	 */
	public boolean fieldExists( String name )
	{
		ClassField retval = null;
		Iterator<ClassField> it = list.iterator();		
		while( it.hasNext() )
		{
			retval = it.next();
			if ( retval.getClassField().getName().equals(name) )
				return true;
		};		
		return false;
	}
	
	/**
	 * Gets field of the class by name.
	 * @param name Name of the field.
	 * @return ClassField
	 */
	public ClassField getField( String name )
	{
		return getFieldRecursively(declaration, name);
	}
	
	/**
	 * Gets field by recursion of the class by name.
	 * @param name Name of the field.
	 * @return ClassField
	 */
	public ClassField getFieldRecursively( ClassDeclaration decl, String name )
	{
		ClassField retval = null;
		Iterator<ClassField> it = decl.getAllFields().list.iterator();		
		while( it.hasNext() )
		{
			retval = it.next();
			if ( retval.getClassField().getName().equals(name) )
				return retval;
		};
		
		// Go thorugh all super classes.
		if ( decl.getSuperClass() != null )
		{
			return getFieldRecursively(decl.getSuperClass(), name);
		}
		return null;
	}
	
	/**
	 * Number of fields within class.
	 * @return
	 */
	public int getNumberOfFields()
	{
		return list.size();
	}
	
	/**
	 * Adds field with certain access type.
	 * 
	 * @param obj Symbol Table node
	 * @param accessType Access type.
	 */
	public int addField( ClassDeclaration decl, ClassFormParam field, int accessType )
	{
		// Check if field already exists.
		if ( fieldExists( field ) )
			return 1;
		
		// Check if current field hides already declared field from super class.
		checkHiddenFields( decl, field );
		
		// Add field into the list.
		list.add( new ClassField( decl, field, accessType ) );
		
		return 0;
	}
	
	/**
	 * Checks if certain field is accessable with forTypeAccess type.
	 * 
	 * @param name Name of the field
	 * @param forTypeAccess Access type. Could be PRIVATE|PROTECTED|PUBLIC.
	 * @return true if access is granted, false otherwise or when field is not found with all fields.
	 */
	public boolean isAccessableField( String name, int forTypeAccess )
	{
		ClassField field = getField(name);
		
		if ( field != null )
			return field.getAccessType() <= forTypeAccess;
		
		return false;
	}
	
	/**
	 * Checks if certain field is accessable with forTypeAccess type.
	 * 
	 * @param name Name of the field
	 * @param forTypeAccess Access type. Could be PRIVATE|PROTECTED|PUBLIC.
	 * @return true if access is granted, false otherwise or when field is not found with all fields.
	 */
	public boolean isAccessableFieldInTheContext( String name, int forTypeAccess, boolean only )
	{
		ClassField field = getField(name);
		
		if ( field != null )
			return  only ? field.getAccessType() == forTypeAccess  : field.getAccessType() <= forTypeAccess;
		
		return false;
	}
	
	private void checkHiddenFields(ClassDeclaration decl, ClassFormParam field)
	{
		SemanticAnalyzer sa = SemanticAnalyzer.__instance__();
		ClassDeclaration current = decl.getSuperClass();
		ClassField field1;
		boolean b;
		boolean go = true;
		while( current != null && go )
		{
			b = current.getAllFields().fieldExists(field);
			if ( b && go )
			{
				field1 = current.getAllFields().getField(field.getName());
				if ( field1.getAccessType() >= ClassField.PROTECTED )
					sa.report_warning("Field '" + field.getName() + "' from class '" + decl.getName() + 
							"' is now hiding field declared in class '" + current.getName() + "'", null);
				else if ( field1.getAccessType() == ClassField.PRIVATE )
					go = false;
			};	
			current = current.getSuperClass();
		};
	}
	
	public int setPositionToTheFields(int start)
	{
		ClassField f;
		Iterator<ClassField> it = list.iterator();
		while( it.hasNext() )
		{
			f = it.next();
			f.getCodeGenerationObj().setAdr(start++);
		};
		
		return start;
	}
}
