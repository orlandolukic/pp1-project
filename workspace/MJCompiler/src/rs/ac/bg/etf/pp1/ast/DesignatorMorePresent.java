// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class DesignatorMorePresent extends DesignatorMore {

    private DesignatorMore DesignatorMore;
    private DesignatorFact DesignatorFact;

    public DesignatorMorePresent (DesignatorMore DesignatorMore, DesignatorFact DesignatorFact) {
        this.DesignatorMore=DesignatorMore;
        if(DesignatorMore!=null) DesignatorMore.setParent(this);
        this.DesignatorFact=DesignatorFact;
        if(DesignatorFact!=null) DesignatorFact.setParent(this);
    }

    public DesignatorMore getDesignatorMore() {
        return DesignatorMore;
    }

    public void setDesignatorMore(DesignatorMore DesignatorMore) {
        this.DesignatorMore=DesignatorMore;
    }

    public DesignatorFact getDesignatorFact() {
        return DesignatorFact;
    }

    public void setDesignatorFact(DesignatorFact DesignatorFact) {
        this.DesignatorFact=DesignatorFact;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DesignatorMore!=null) DesignatorMore.accept(visitor);
        if(DesignatorFact!=null) DesignatorFact.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DesignatorMore!=null) DesignatorMore.traverseTopDown(visitor);
        if(DesignatorFact!=null) DesignatorFact.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DesignatorMore!=null) DesignatorMore.traverseBottomUp(visitor);
        if(DesignatorFact!=null) DesignatorFact.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DesignatorMorePresent(\n");

        if(DesignatorMore!=null)
            buffer.append(DesignatorMore.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DesignatorFact!=null)
            buffer.append(DesignatorFact.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DesignatorMorePresent]");
        return buffer.toString();
    }
}
