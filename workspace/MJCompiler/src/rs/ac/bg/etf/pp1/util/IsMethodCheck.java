package rs.ac.bg.etf.pp1.util;

import rs.ac.bg.etf.pp1.ast.*;

public class IsMethodCheck extends VisitorAdaptor {
	
	private boolean isMethod;
	
	public IsMethodCheck()
	{
		isMethod = false;
	}
	
	public boolean isMethod()
	{
		return isMethod;
	}
	
	public void visit(FactorParensStart fs)
	{
		isMethod = true;
	}	

}
