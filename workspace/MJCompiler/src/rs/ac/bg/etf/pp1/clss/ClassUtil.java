package rs.ac.bg.etf.pp1.clss;

import java.util.Iterator;

import rs.ac.bg.etf.pp1.CodeGenerator;
import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.util.Scopes;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;
import rs.etf.pp1.symboltable.structure.HashTableDataStructure;

public class ClassUtil {
	
	public static void checkClassDeclStart( SemanticAnalyzer sa, ClassDeclStart s )
	{				
		// Check if class is already defined.
		String className = s.getI1();
		Obj o = Tab.find(className);
		if ( o != Tab.noObj )
			sa.report_error("Class " + className + " is already defined", s);
		else
		{
			ExtendsClause e = s.getExtendsClause();
			if ( e instanceof ExtendsPresent )
			{
				Type t = ((ExtendsPresent) e).getType();
				if ( !(t instanceof TIdent) )
				{
					sa.report_error("Cannot extend from basic type '" + 
							Functions.getVariableTypeAsString( Functions.getTypeBySubstitution(t) ) 
					+ "'", s);
				} else
				{
					// Check if super class exists.
					String superClassName = ((TIdent) t).getType();
					o = Tab.find( superClassName );
					if ( o == Tab.noObj )
						sa.report_error("Class '" + superClassName + "' is not defined", s);
				};
			};
		};
	}
	
	public static void checkClassDeclStart( SemanticAnalyzer sa, AbstractClassDeclStart s )
	{				
		// Check if class is already defined.
		String className = s.getI1();
		Obj o = Tab.find(className);
		if ( o != Tab.noObj )
			sa.report_error("Class " + className + " is already defined", s);
		else
		{
			ExtendsClause e = s.getExtendsClause();
			if ( e instanceof ExtendsPresent )
			{
				Type t = ((ExtendsPresent) e).getType();
				if ( !(t instanceof TIdent) )
				{
					sa.report_error("Cannot extend from basic type '" + 
							Functions.getVariableTypeAsString( Functions.getTypeBySubstitution(t) ) 
					+ "'", s);
				} else
				{
					// Check if super class exists.
					String superClassName = ((TIdent) t).getType();
					o = Tab.find( superClassName );
					if ( o == Tab.noObj )
						sa.report_error("Super class '" + superClassName + "' is not defined", s);
				};
			};
		};
	}
	
	public static String getMethodSignature( ClassDeclaration decl, ClassMethod method, boolean printVariableName )
	{
		return getMethodSignature(decl, method, printVariableName, true);
	}
	
	public static String getMethodSignature( ClassDeclaration decl, ClassMethod method, boolean printVariableName, boolean printReturnValue )
	{
		StringBuilder str = new StringBuilder();
		
		// Print return type.
		if ( printReturnValue )
		{
			if ( method.getReturnValue().isClassReference() )
				str.append( method.getReturnValue().getFormalParameterClassName() );
			else
				str.append( Functions.getVariableTypeAsString( method.getReturnValue().getType() ) );
			str.append( " " );
		};
		
		// Print method name.
		str.append( method.getObj().getName() );
		str.append( "(" );
		
		ClassFormParam[] params = method.getFormalParameters();
		for (int i=0, n=params.length; i<n; i++)
		{			
			// Print type.
			str.append( params[i].toString() );
			
			if ( printVariableName )
			{
				str.append(" ");
				
				// Append name.
				str.append( params[i].getName() );
			};
			
			if ( i+1<n )
				str.append(", ");
		};
		
		str.append( ")" );
		
		return str.toString();
	}
	
	public static void processMethod( SemanticAnalyzer sa, MethodReturnValue re, String methodName, AccessRights ar, SyntaxNode s, boolean isAbstract )
	{
		// Check if return type exists.
		if ( re instanceof MethodReturnType )
		{
			if ( ((MethodReturnType) re).getType() instanceof TIdent )
			{
				String retvalType = ((TIdent) ((MethodReturnType) re).getType()).getType();
				
				if ( Tab.find( retvalType ) == Tab.noObj )
				{
					sa.report_error("Type '" + retvalType + "' is not defined", s);
					return;
				};
			};
		};
		
		// Set method declaration mode.
		sa.methStart = true;
		sa.methName  = methodName;
		sa.methFormPars = 0;
		sa.methHasReturnStatement = false;
		
		String retvalName = null;
		Struct r;
		if ( re instanceof MethodReturnVoid )
			r = Tab.noType;
		else
		{
			MethodReturnType h = ((MethodReturnType) re);
			if ( h.getType() instanceof TIdent )
				retvalName = ( (TIdent) h.getType() ).getType();
			
			r = Functions.getTypeBySubstitution( h.getType() );
		};
		Obj o = new Obj( Obj.Meth, methodName, r );
		o.setLevel(0);
		ClassFormParam returnValue = new ClassFormParam(null, new Obj( Obj.NO_VALUE, retvalName, r ) );
		sa.classCurrentMethod = sa.classInstance.getAllMethods()
				.addMethod( sa.classInstance, o, Functions.getAccessRightBySubstitution(ar), isAbstract, returnValue, s.getLine());
	}
	
	public static String getClassNameFromStruct( Struct s )
	{
		Iterator<Obj> it;
		if ( s.getKind() == Struct.Class )
		{
			it = s.getMembers().iterator();
			return _getClassName(it);
		} else if ( s.getKind() == Struct.Array )
		{
			if ( s.getElemType().getKind() == Struct.Class )
			{
				it = s.getElemType().getMembers().iterator();
				return _getClassName(it);
			};			
		}
		return null;
	}
	
	public static String getClassNameFromObj( Obj o )
	{
		return getClassNameFromStruct( o.getType() );
	}
	
	public static ClassDeclaration getClassDeclarationFromObj( Obj o )
	{
		String name = getClassNameFromObj(o);
		if ( name != null )
		{
			return ClassDeclarationList.getInstance().getClassDeclaration(name);
		};
		
		return null;
	}
	
	private static String _getClassName(Iterator<Obj> it)
	{
		Obj x;
		while( it.hasNext() )
		{
			x = it.next();
			if ( x.getKind() == Obj.NO_VALUE && x.getType().getKind() == Struct.None )
				return x.getName();
		};
		return null;
	}
	
	public static Struct getStructForClassname( String name, boolean isArray )
	{
		Struct s = ClassDeclarationList.getInstance().getClassType(name);
		
		if ( s != null )
		{
			if ( isArray )
				s = new Struct( Struct.Array, s );
		};
		
		return s;
	}
	
	/*
	public static ClassDeclaration getBaseClass( String derivedClassName )
	{
		Iterator<ClassDeclaration> it = ClassDeclarationList.getInstance().iterator();
		ClassDeclaration d =  null;
		while( it.hasNext() )
		{
			d = it.next();
			if ( d.getName().equals(derivedClassName) )
				break;
		};
		
		return d;
	}
	*/
	
	public static ClassDeclaration getClassDeclarationByName( String name )
	{
		return ClassDeclarationList.getInstance().getClassDeclaration(name);
	}
	
	public static boolean isDerived( String baseClass, String extendedClass )
	{
		return ClassDeclarationList.getInstance().isDerived(baseClass, extendedClass);
	}
	
	public static boolean isClassReference(Obj o)
	{
		return o.getType().getKind() == Struct.Class;
	}
	
	public static boolean isArrayReference(Obj o)
	{
		return o.getType().getKind() == Struct.Array;
	}
	
	public static boolean isReference(Obj o)
	{
		return o.getType().isRefType();
	}
	
	/**
	 * Creates all virtual tables.
	 */
	public static void createVirtualTables( CodeGenerator cg, int startCnt, int main )
	{
		Code.mainPc = Code.pc;
		int cnt = startCnt;
		int addr = Code.pc;
		Iterator<ClassDeclaration> it = ClassDeclarationList.getInstance().iterator();
		ClassDeclaration decl;
		while( it.hasNext() )
		{
			decl = it.next();
			decl.setStartAddressVirtualTable( cnt );
			cnt += decl.createVirtualTable( cnt );
			addr += decl.getSizeOfVirtualTableInBytes();
		};
		
		// Jump to main function
		Code.put( Code.jmp );
		cg.refactorMainPcJump = Code.pc;
		Code.put2( main - Code.pc + 1 );
		
		Code.dataSize += addr;
	}
	
	public static String getScopeNameForMethod( ClassDeclaration decl, String methodName )
	{
		return getScopeNameForClass(decl) + "." + methodName;
	}
	
	public static String getScopeNameForClass( ClassDeclaration decl )
	{
		return "class:" + decl.getName();
	}
	
	public static void changeScopeForMethod( ClassDeclaration decl, String methodName )
	{
		Scopes.changeSymbolTableScope( getScopeNameForMethod(decl, methodName) );
	}
	
	public static void changeScopeForClass( ClassDeclaration decl )
	{
		Scopes.changeSymbolTableScope( getScopeNameForClass(decl) );
	}
}
