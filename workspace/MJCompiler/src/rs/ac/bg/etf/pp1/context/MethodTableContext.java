package rs.ac.bg.etf.pp1.context;

import rs.ac.bg.etf.pp1.clss.ClassDeclaration;
import rs.ac.bg.etf.pp1.clss.ClassMethod;

public class MethodTableContext {
	
	public ClassMethod method;
	public ClassDeclaration originClass;
	public ClassDeclaration implementationClass;
	
	public int getMethodType()
	{
		return method.isAbstractMethod() ? ClassDeclaration.ABSTRACT : ClassDeclaration.CONCRETE;
	}
	
}
