package rs.ac.bg.etf.pp1.des;

import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.etf.pp1.mj.runtime.Code;

public class DesignatorCGIncDec extends DesignatorFinder {

	private int factor;
	private int state;
	
	public DesignatorCGIncDec(SemanticAnalyzer sa, boolean check, int factor) {
		super(sa, check);
		this.factor = factor;
		this.state = 0;
	}
	
	@Override
	protected void designatorFinished() {
		
		if ( !isPrimitiveVariable() )
		{
			generateThroughTheList(false);			
		};
		
		// Get variable.
		generateVariable();
		
		Code.loadConst(1);
		if ( factor < 0 )
			Code.put( Code.sub );
		else
			Code.put( Code.add );
		
		storeVariable();
		
		/*
		Obj entry = this.getObject();
		
		if ( isArrayAccess )
		{
			Code.put( Code.dup2 );
			Code.load( Functions.constantElem(entry.getType()) );
		};
		
		switch( entry.getType().getKind() )
		{
		case Struct.Int:
			if ( !performLoadCheckAtTheEnd )
				Code.load(entry);
			
			Code.loadConst(1);
			if ( factor < 0 )		
				Code.put(Code.sub);
			else
				Code.put( Code.add );		
			
			storeVariable();
			break;
			
		default:
			if ( performLoadCheckAtTheEnd )
				Code.put( Code.pop );
			break;
		}
		*/
	}

}
