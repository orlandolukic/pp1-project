package rs.ac.bg.etf.pp1.gen;

import java.util.Iterator;
import java.util.LinkedList;

import rs.ac.bg.etf.pp1.CodeGenerator;
import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.context.ConditionCheckerContext;
import rs.ac.bg.etf.pp1.context.ForLoopCheckerContext;
import rs.ac.bg.etf.pp1.util.Stack;
import rs.etf.pp1.mj.runtime.Code;

public class ForStatementCG extends VisitorAdaptor {
	
	private CodeGenerator cg;
	private Stack<ForLoopCheckerContext> stack;
	
	public ForStatementCG(CodeGenerator cg)
	{
		this.cg = cg;
		this.stack = new Stack<ForLoopCheckerContext>();
	}
	
	private ForLoopCheckerContext top()
	{
		return stack.top();
	}
	
	public void generate(ForStatement fs)
	{
		boolean refactorIfStatement;
		
		// Prepare stack.
		stack.push( new ForLoopCheckerContext(true) );
		
		// Assign for loop to current element.
		top().fs = fs;
		
		// Generate initial statement. 
		fs.getForDesignatorStatement().traverseBottomUp(cg);
		
		top().addrCheckCondition = Code.pc;
		
		// Generate condition
		if ( fs.getForCondition() instanceof ForConditionPresent )
		{
			IfStatementCG ifsCG = new IfStatementCG(cg);
			ifsCG.generateCondition( ((ForConditionPresent)fs.getForCondition()).getCondition() );			
			ConditionCheckerContext context = ifsCG.top();			
			Functions.appendLists( top().refactorEndForLoop, context.refactorElsePart );
			top().refactorEndForLoop.add( new Integer( Code.pc-2 ) );
			
			// Set all true nodes to point here.
			backpatch( context.refactorIfPart );
		};
		
		// Generate whole body of for loop.
		fs.getForStatementEnd().getMatched().traverseBottomUp(this);
		
		// Backpatch adresses for addrIterationExpression.
		backpatch( top().refactorIterationExpression );
		
		// Generate iteration expression.
		fs.getForDesignatorStatement1().traverseBottomUp( cg );
		Code.putJump( top().addrCheckCondition );
		
		// Backpatch adresses.
		backpatch( top().refactorEndForLoop );
	}
	
	/**
	 * Backpatch all adresses.
	 */
	private void backpatch(LinkedList<Integer> reference)
	{
		int i;
		Iterator<Integer> it = reference.iterator();
		while( it.hasNext() )
		{
			i = it.next().intValue();
			Code.fixup(i);
		};
	}
	
	/**
	 * ===============================================================================================================
	 * 		VISIT METHODS
	 * ===============================================================================================================
	 */
	
	public void visit(EqualExpr ee)
	{
		if ( shouldntGenerateCode() )
			return;
		
		ee.traverseBottomUp(cg);
	}
	
	public void visit(PlusPlusExpr ppe)
	{
		if ( shouldntGenerateCode() )
			return;
		
		ppe.traverseBottomUp(cg);
	}
	
	public void visit(MinusMinusExpr mme)
	{
		if ( shouldntGenerateCode() )
			return;
		
		mme.traverseBottomUp(cg);
	}
	
	public void visit(FuncCall fc)
	{
		if ( shouldntGenerateCode() )
			return;
		
		fc.traverseBottomUp(cg);
	}
	
	public void visit(ReadStatement rs)
	{
		if ( shouldntGenerateCode() )
			return;
		
		rs.traverseBottomUp(cg);
	}
	
	public void visit(PrintStatement ps)
	{
		if ( shouldntGenerateCode() )
			return;
		
		ps.traverseBottomUp(cg);
	}
	
	/**
	 * break; (*)
	 */
	public void visit(BreakStatement bs)
	{
		if ( shouldntGenerateCode() )
			return;
		
		Code.putJump( 0 );
		top().refactorEndForLoop.add( new Integer(Code.pc-2) );
	}
	
	/**
	 * continue; (*)
	 */
	public void visit(ContinueStatement cs)
	{
		if ( shouldntGenerateCode() )
			return;
		
		Code.putJump( 0 );
		top().refactorIterationExpression.add( new Integer(Code.pc-2) );
	}
	
	/**
	 * return <Expr>; (*)
	 */
	public void visit(ReturnExpr re)
	{
		if ( shouldntGenerateCode() )
			return;
		
		re.traverseBottomUp(cg);
	}
	
	public void visit(ForStatementStart s)
	{
		top().forDepth++;
		top().allowCodeGeneration = false;
	}
	
	public void visit(ForStatement fs)
	{	
		top().forDepth--;
		if ( shouldntGenerateCode() )
			return;
		
		if ( top().forDepth == 0 )
		{
			// Generate for loop.
			generate(fs);
			
			ForLoopCheckerContext context = stack.pop();
			top().allowCodeGeneration = true;
		};
	}
	
	public void visit(ForeachStatementStart start)
	{
		top().foreachDepth++;
	}
	
	public void visit(ForeachStatement fs)
	{
		top().foreachDepth--;
		if ( shouldntGenerateCode() )
			return;
		
		if ( top().foreachDepth == 0 && top().ifDepth == 0 && top().forDepth == 0 )
		{
			ForeachStatementCG fsc = new ForeachStatementCG(cg);
			fsc.generate(fs);
		};
	}

	private boolean shouldntGenerateCode()
	{
		return top().ifDepth > 0 || top().forDepth > 0 || top().foreachDepth > 0;
	}
	
	/**
	 * ===============================================================================================================
	 * 			*** VISIT METHODS ***
	 * 		--------------------------
	 * 			  IF STATEMENTS
	 * ===============================================================================================================
	 */
	
	public void visit(IfStart i)
	{
		top().ifDepth++;
		top().allowCodeGeneration = false;
	}
	
	public void visit(MatchedIfElse i)
	{
		top().ifDepth--;
		
		if ( shouldntGenerateCode() )
			return;
		
		if ( top().ifDepth == 0 )
		{
			IfStatementCG j = new IfStatementCG(cg);
			j.top().forLoopContext = top();
			j.generateAll(i);
			top().allowCodeGeneration = true;
		};
	}
	
	public void visit(UnmatchedIf i)
	{
		top().ifDepth--;
		
		if ( shouldntGenerateCode() )
			return;
		
		if ( top().ifDepth == 0 && top().forDepth == 0 )
		{
			IfStatementCG j = new IfStatementCG(cg);
			j.top().forLoopContext = top();
			j.generateAll(i);
			top().allowCodeGeneration = true;
		};
	}
	
	public void visit(UnmatchedIfElse i)
	{
		top().ifDepth--;
		
		if ( shouldntGenerateCode() )
			return;
		
		if ( top().ifDepth == 0 )
		{
			IfStatementCG j = new IfStatementCG(cg);
			j.top().forLoopContext = top();
			j.generateAll(i);
			top().allowCodeGeneration = true;
		};
	}

}
