// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class ReturnExpr extends Matched {

    private ReturnExprStart ReturnExprStart;
    private RetVal RetVal;

    public ReturnExpr (ReturnExprStart ReturnExprStart, RetVal RetVal) {
        this.ReturnExprStart=ReturnExprStart;
        if(ReturnExprStart!=null) ReturnExprStart.setParent(this);
        this.RetVal=RetVal;
        if(RetVal!=null) RetVal.setParent(this);
    }

    public ReturnExprStart getReturnExprStart() {
        return ReturnExprStart;
    }

    public void setReturnExprStart(ReturnExprStart ReturnExprStart) {
        this.ReturnExprStart=ReturnExprStart;
    }

    public RetVal getRetVal() {
        return RetVal;
    }

    public void setRetVal(RetVal RetVal) {
        this.RetVal=RetVal;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ReturnExprStart!=null) ReturnExprStart.accept(visitor);
        if(RetVal!=null) RetVal.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ReturnExprStart!=null) ReturnExprStart.traverseTopDown(visitor);
        if(RetVal!=null) RetVal.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ReturnExprStart!=null) ReturnExprStart.traverseBottomUp(visitor);
        if(RetVal!=null) RetVal.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ReturnExpr(\n");

        if(ReturnExprStart!=null)
            buffer.append(ReturnExprStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(RetVal!=null)
            buffer.append(RetVal.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ReturnExpr]");
        return buffer.toString();
    }
}
