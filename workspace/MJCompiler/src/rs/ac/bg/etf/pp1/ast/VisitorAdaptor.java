// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public abstract class VisitorAdaptor implements Visitor { 

    public void visit(ActParsMore ActParsMore) { }
    public void visit(Factor Factor) { }
    public void visit(StatementListOptions StatementListOptions) { }
    public void visit(FactorExpr FactorExpr) { }
    public void visit(AddopLeft AddopLeft) { }
    public void visit(AssignopRight AssignopRight) { }
    public void visit(AddopRight AddopRight) { }
    public void visit(Declarations Declarations) { }
    public void visit(ConstDeclMore ConstDeclMore) { }
    public void visit(Relop Relop) { }
    public void visit(ConstDeclVarOnly ConstDeclVarOnly) { }
    public void visit(MulopRight MulopRight) { }
    public void visit(DesignatorFact DesignatorFact) { }
    public void visit(IsConstDecl IsConstDecl) { }
    public void visit(MulopLeft MulopLeft) { }
    public void visit(Expr Expr) { }
    public void visit(AssignopLeft AssignopLeft) { }
    public void visit(AbstractClassDecl AbstractClassDecl) { }
    public void visit(CondFactMore CondFactMore) { }
    public void visit(FormParsArray FormParsArray) { }
    public void visit(Unmatched Unmatched) { }
    public void visit(ForCondition ForCondition) { }
    public void visit(ConstDeclType ConstDeclType) { }
    public void visit(AbstractMethodDeclList AbstractMethodDeclList) { }
    public void visit(DesignatorFuncCall DesignatorFuncCall) { }
    public void visit(AddopTerm AddopTerm) { }
    public void visit(NegativeExpr NegativeExpr) { }
    public void visit(Invertor Invertor) { }
    public void visit(PrintNumAddon PrintNumAddon) { }
    public void visit(ClassDeclMethodsWrapper ClassDeclMethodsWrapper) { }
    public void visit(ActParsWrapper ActParsWrapper) { }
    public void visit(Condition Condition) { }
    public void visit(AccessRights AccessRights) { }
    public void visit(ClassDeclMethods ClassDeclMethods) { }
    public void visit(Mulop Mulop) { }
    public void visit(DesignatorStatement DesignatorStatement) { }
    public void visit(FormParsMore FormParsMore) { }
    public void visit(DesignatorMore DesignatorMore) { }
    public void visit(ExprConstDeclType ExprConstDeclType) { }
    public void visit(Addop Addop) { }
    public void visit(StatementList StatementList) { }
    public void visit(Assignop Assignop) { }
    public void visit(ConstDecl ConstDecl) { }
    public void visit(FormParsWrapper FormParsWrapper) { }
    public void visit(ArrDecl ArrDecl) { }
    public void visit(AbstractClassDeclMethods AbstractClassDeclMethods) { }
    public void visit(MulopFactor MulopFactor) { }
    public void visit(Type Type) { }
    public void visit(ForDesignatorStatement ForDesignatorStatement) { }
    public void visit(RetVal RetVal) { }
    public void visit(NumerusSign NumerusSign) { }
    public void visit(SemiMaybe SemiMaybe) { }
    public void visit(ClassDecl ClassDecl) { }
    public void visit(MethodVarDecl MethodVarDecl) { }
    public void visit(CondTermMore CondTermMore) { }
    public void visit(MethodDeclList MethodDeclList) { }
    public void visit(FactorParens FactorParens) { }
    public void visit(MethodReturnValue MethodReturnValue) { }
    public void visit(CondFactVolatile CondFactVolatile) { }
    public void visit(FormParam FormParam) { }
    public void visit(DesignatorStatementWrapper DesignatorStatementWrapper) { }
    public void visit(Matched Matched) { }
    public void visit(VarDeclList VarDeclList) { }
    public void visit(ExtendsClause ExtendsClause) { }
    public void visit(VarDeclMore VarDeclMore) { }
    public void visit(AssignopMulop AssignopMulop) { visit(); }
    public void visit(AssignopAddop AssignopAddop) { visit(); }
    public void visit(AssignopEqual AssignopEqual) { visit(); }
    public void visit(AddopMinusEqual AddopMinusEqual) { visit(); }
    public void visit(AddopPlusEqual AddopPlusEqual) { visit(); }
    public void visit(AddopMinus AddopMinus) { visit(); }
    public void visit(AddopPlus AddopPlus) { visit(); }
    public void visit(AddopRightOperand AddopRightOperand) { visit(); }
    public void visit(AddopLeftOperand AddopLeftOperand) { visit(); }
    public void visit(MulopModEqual MulopModEqual) { visit(); }
    public void visit(MulopDivEqual MulopDivEqual) { visit(); }
    public void visit(MulopTimesEqual MulopTimesEqual) { visit(); }
    public void visit(MulopMod MulopMod) { visit(); }
    public void visit(MulopDiv MulopDiv) { visit(); }
    public void visit(MulopTimes MulopTimes) { visit(); }
    public void visit(MulopRightOperand MulopRightOperand) { visit(); }
    public void visit(MulopLeftOperand MulopLeftOperand) { visit(); }
    public void visit(FactorWithExprEnd FactorWithExprEnd) { visit(); }
    public void visit(FactorWithExprStart FactorWithExprStart) { visit(); }
    public void visit(NoFactorExpr NoFactorExpr) { visit(); }
    public void visit(FactorExprAll FactorExprAll) { visit(); }
    public void visit(FactorParensEnd FactorParensEnd) { visit(); }
    public void visit(FactorParensStart FactorParensStart) { visit(); }
    public void visit(FactorParensNotPresent FactorParensNotPresent) { visit(); }
    public void visit(FactorParensPresent FactorParensPresent) { visit(); }
    public void visit(ENumberConst ENumberConst) { visit(); }
    public void visit(ECharConst ECharConst) { visit(); }
    public void visit(EBoolConst EBoolConst) { visit(); }
    public void visit(FactorDesignator FactorDesignator) { visit(); }
    public void visit(FactorNew FactorNew) { visit(); }
    public void visit(FactorWithExpr FactorWithExpr) { visit(); }
    public void visit(FactorConstDeclType FactorConstDeclType) { visit(); }
    public void visit(NoMulopFactor NoMulopFactor) { visit(); }
    public void visit(MulopFactorAll MulopFactorAll) { visit(); }
    public void visit(Term Term) { visit(); }
    public void visit(NoAddopTerm NoAddopTerm) { visit(); }
    public void visit(AddopTermAll AddopTermAll) { visit(); }
    public void visit(NegativeExprNoMinus NegativeExprNoMinus) { visit(); }
    public void visit(NegativeExprMinus NegativeExprMinus) { visit(); }
    public void visit(ExprNoError ExprNoError) { visit(); }
    public void visit(DesignatorStart DesignatorStart) { visit(); }
    public void visit(DesignatorOptEnd DesignatorOptEnd) { visit(); }
    public void visit(DesignatorOptStart DesignatorOptStart) { visit(); }
    public void visit(DesignatorObjectAccess DesignatorObjectAccess) { visit(); }
    public void visit(DesignatorArrayAccess DesignatorArrayAccess) { visit(); }
    public void visit(DesignatorMoreNotPresent DesignatorMoreNotPresent) { visit(); }
    public void visit(DesignatorMorePresent DesignatorMorePresent) { visit(); }
    public void visit(Designator Designator) { visit(); }
    public void visit(InvertorNotPresent InvertorNotPresent) { visit(); }
    public void visit(InvertorPresent InvertorPresent) { visit(); }
    public void visit(RLTE RLTE) { visit(); }
    public void visit(RLT RLT) { visit(); }
    public void visit(RGTE RGTE) { visit(); }
    public void visit(RGT RGT) { visit(); }
    public void visit(RNEQ RNEQ) { visit(); }
    public void visit(REQ REQ) { visit(); }
    public void visit(CondFactMoreNotPresent CondFactMoreNotPresent) { visit(); }
    public void visit(CondFactMorePresent CondFactMorePresent) { visit(); }
    public void visit(CondTermMoreNotPresent CondTermMoreNotPresent) { visit(); }
    public void visit(CondTermMorePresent CondTermMorePresent) { visit(); }
    public void visit(CondFactIsVolatile CondFactIsVolatile) { visit(); }
    public void visit(CondFactNotVolatile CondFactNotVolatile) { visit(); }
    public void visit(CondFact CondFact) { visit(); }
    public void visit(CondTerm CondTerm) { visit(); }
    public void visit(ConditionErrorSemi ConditionErrorSemi) { visit(); }
    public void visit(ConditionNoError ConditionNoError) { visit(); }
    public void visit(SemiDoesNotExist SemiDoesNotExist) { visit(); }
    public void visit(SemiExists SemiExists) { visit(); }
    public void visit(EqualExprStart EqualExprStart) { visit(); }
    public void visit(ForDesignatorStatementNotExists ForDesignatorStatementNotExists) { visit(); }
    public void visit(ForDesignatorStatementExists ForDesignatorStatementExists) { visit(); }
    public void visit(ForConditionNotPresent ForConditionNotPresent) { visit(); }
    public void visit(ForConditionPresent ForConditionPresent) { visit(); }
    public void visit(ForeachParensContent ForeachParensContent) { visit(); }
    public void visit(ForeachStatementStart ForeachStatementStart) { visit(); }
    public void visit(ElseBegin ElseBegin) { visit(); }
    public void visit(IfCondition IfCondition) { visit(); }
    public void visit(IfStart IfStart) { visit(); }
    public void visit(ForStatementEnd ForStatementEnd) { visit(); }
    public void visit(ForStatementStart ForStatementStart) { visit(); }
    public void visit(MultipleStatementsEnd MultipleStatementsEnd) { visit(); }
    public void visit(MultipleStatementsStart MultipleStatementsStart) { visit(); }
    public void visit(ReturnExprStart ReturnExprStart) { visit(); }
    public void visit(FuncCallStart FuncCallStart) { visit(); }
    public void visit(FuncCall FuncCall) { visit(); }
    public void visit(MinusMinusExpr MinusMinusExpr) { visit(); }
    public void visit(PlusPlusExpr PlusPlusExpr) { visit(); }
    public void visit(EqualExpr EqualExpr) { visit(); }
    public void visit(DesignatorStatementWrapperError DesignatorStatementWrapperError) { visit(); }
    public void visit(DesignatorStatementWrapperNoError DesignatorStatementWrapperNoError) { visit(); }
    public void visit(PrintStart PrintStart) { visit(); }
    public void visit(PrintWrapperAll PrintWrapperAll) { visit(); }
    public void visit(PrintWrapper PrintWrapper) { visit(); }
    public void visit(RetValVoid RetValVoid) { visit(); }
    public void visit(RetValType RetValType) { visit(); }
    public void visit(UnmatchedIfElse UnmatchedIfElse) { visit(); }
    public void visit(UnmatchedIf UnmatchedIf) { visit(); }
    public void visit(MatchedIfElse MatchedIfElse) { visit(); }
    public void visit(ReturnExpr ReturnExpr) { visit(); }
    public void visit(ForeachStatement ForeachStatement) { visit(); }
    public void visit(ForStatement ForStatement) { visit(); }
    public void visit(ContinueStatement ContinueStatement) { visit(); }
    public void visit(BreakStatement BreakStatement) { visit(); }
    public void visit(MultipleStatements MultipleStatements) { visit(); }
    public void visit(PrintStatement PrintStatement) { visit(); }
    public void visit(ReadStatement ReadStatement) { visit(); }
    public void visit(DesignatorStatementWrapperEnded DesignatorStatementWrapperEnded) { visit(); }
    public void visit(StatementUnmatched StatementUnmatched) { visit(); }
    public void visit(StatementMatched StatementMatched) { visit(); }
    public void visit(StatementListEnd StatementListEnd) { visit(); }
    public void visit(StatementListMany StatementListMany) { visit(); }
    public void visit(ActParam ActParam) { visit(); }
    public void visit(ActParsMoreNotPresent ActParsMoreNotPresent) { visit(); }
    public void visit(ActParsMorePresent ActParsMorePresent) { visit(); }
    public void visit(ActPars ActPars) { visit(); }
    public void visit(ActParsNotPresent ActParsNotPresent) { visit(); }
    public void visit(ActParsPresent ActParsPresent) { visit(); }
    public void visit(FormParsNotArray FormParsNotArray) { visit(); }
    public void visit(FormParsIsArray FormParsIsArray) { visit(); }
    public void visit(FormParsMoreNotPresent FormParsMoreNotPresent) { visit(); }
    public void visit(FormParsMorePresent FormParsMorePresent) { visit(); }
    public void visit(FormPars FormPars) { visit(); }
    public void visit(FormParamErrorEnd FormParamErrorEnd) { visit(); }
    public void visit(FormParamErrorComma FormParamErrorComma) { visit(); }
    public void visit(FormParamNoError FormParamNoError) { visit(); }
    public void visit(FormParsNotPresent FormParsNotPresent) { visit(); }
    public void visit(FormParsPresent FormParsPresent) { visit(); }
    public void visit(NoMethodVarDecl NoMethodVarDecl) { visit(); }
    public void visit(MethodVarDeclAll MethodVarDeclAll) { visit(); }
    public void visit(MethodReturnType MethodReturnType) { visit(); }
    public void visit(MethodReturnVoid MethodReturnVoid) { visit(); }
    public void visit(MethodDeclStart MethodDeclStart) { visit(); }
    public void visit(MethodDeclFormParsEnd MethodDeclFormParsEnd) { visit(); }
    public void visit(MethodDecl MethodDecl) { visit(); }
    public void visit(MethodDeclListEnd MethodDeclListEnd) { visit(); }
    public void visit(MethodDeclListMany MethodDeclListMany) { visit(); }
    public void visit(AbstractClassDeclMethodsRegularStart AbstractClassDeclMethodsRegularStart) { visit(); }
    public void visit(AbstractMethodDeclStart AbstractMethodDeclStart) { visit(); }
    public void visit(AbstractClassDeclStart AbstractClassDeclStart) { visit(); }
    public void visit(AbstractMethodDecl AbstractMethodDecl) { visit(); }
    public void visit(AbstractClassDeclMethodsNotPresent AbstractClassDeclMethodsNotPresent) { visit(); }
    public void visit(AbstractClassDeclMethodsAbstract AbstractClassDeclMethodsAbstract) { visit(); }
    public void visit(AbstractClassDeclMethodsRegular AbstractClassDeclMethodsRegular) { visit(); }
    public void visit(AbstractClassDeclMethodsWrapper AbstractClassDeclMethodsWrapper) { visit(); }
    public void visit(AbstractClassDeclNoError AbstractClassDeclNoError) { visit(); }
    public void visit(ClassDeclMethodStart ClassDeclMethodStart) { visit(); }
    public void visit(ClassDeclStart ClassDeclStart) { visit(); }
    public void visit(ClassDeclMethod ClassDeclMethod) { visit(); }
    public void visit(ClassDeclMethodsNoMore ClassDeclMethodsNoMore) { visit(); }
    public void visit(ClassDeclMethodsMore ClassDeclMethodsMore) { visit(); }
    public void visit(ClassDeclMethodsNotPresent ClassDeclMethodsNotPresent) { visit(); }
    public void visit(ClassDeclMethodsWrapperPresent ClassDeclMethodsWrapperPresent) { visit(); }
    public void visit(ExtendsNotPresent ExtendsNotPresent) { visit(); }
    public void visit(ExtendsPresent ExtendsPresent) { visit(); }
    public void visit(ClassDeclNoError ClassDeclNoError) { visit(); }
    public void visit(VarDeclAccessRights VarDeclAccessRights) { visit(); }
    public void visit(NoPrintNumAddon NoPrintNumAddon) { visit(); }
    public void visit(PrintNumAddonNoEps PrintNumAddonNoEps) { visit(); }
    public void visit(VarDeclListNoMore VarDeclListNoMore) { visit(); }
    public void visit(VarDeclListMore VarDeclListMore) { visit(); }
    public void visit(TIdent TIdent) { visit(); }
    public void visit(TChar TChar) { visit(); }
    public void visit(TBoolean TBoolean) { visit(); }
    public void visit(TInteger TInteger) { visit(); }
    public void visit(VarDeclOne VarDeclOne) { visit(); }
    public void visit(NoVarDeclMore NoVarDeclMore) { visit(); }
    public void visit(VarDeclMoreAll VarDeclMoreAll) { visit(); }
    public void visit(NoArrDecl NoArrDecl) { visit(); }
    public void visit(ArrDeclPresent ArrDeclPresent) { visit(); }
    public void visit(VarDeclType VarDeclType) { visit(); }
    public void visit(VarDecl VarDecl) { visit(); }
    public void visit(AccessNotPresent AccessNotPresent) { visit(); }
    public void visit(AccessPublic AccessPublic) { visit(); }
    public void visit(AccessProtected AccessProtected) { visit(); }
    public void visit(AccessPrivate AccessPrivate) { visit(); }
    public void visit(NumerusSignPositive NumerusSignPositive) { visit(); }
    public void visit(NumerusSignNegative NumerusSignNegative) { visit(); }
    public void visit(NumberConst NumberConst) { visit(); }
    public void visit(CharConst CharConst) { visit(); }
    public void visit(BoolConst BoolConst) { visit(); }
    public void visit(ConstDeclVarOnlyError ConstDeclVarOnlyError) { visit(); }
    public void visit(ConstDeclVarOnlyNoError ConstDeclVarOnlyNoError) { visit(); }
    public void visit(ConstDeclMoreAll ConstDeclMoreAll) { visit(); }
    public void visit(ConstDeclMoreVarOnly ConstDeclMoreVarOnly) { visit(); }
    public void visit(IsConstDeclStart IsConstDeclStart) { visit(); }
    public void visit(ConstDeclError ConstDeclError) { visit(); }
    public void visit(ConstDeclNoError ConstDeclNoError) { visit(); }
    public void visit(DeclarationsEnd DeclarationsEnd) { visit(); }
    public void visit(EmptyDeclaration EmptyDeclaration) { visit(); }
    public void visit(AbstractClassDeclaration AbstractClassDeclaration) { visit(); }
    public void visit(ClassDeclaration ClassDeclaration) { visit(); }
    public void visit(VarDeclEnding VarDeclEnding) { visit(); }
    public void visit(ConstDeclEnding ConstDeclEnding) { visit(); }
    public void visit(ProgName ProgName) { visit(); }
    public void visit(Program Program) { visit(); }


    public void visit() { }
}
