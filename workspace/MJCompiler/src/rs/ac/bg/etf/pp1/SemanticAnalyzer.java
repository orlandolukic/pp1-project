package rs.ac.bg.etf.pp1;

import java.util.Iterator;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.checkers.ConditionChecker;
import rs.ac.bg.etf.pp1.checkers.ForLoopChecker;
import rs.ac.bg.etf.pp1.checkers.ForeachLoopChecker;
import rs.ac.bg.etf.pp1.clss.ClassDeclarationList;
import rs.ac.bg.etf.pp1.clss.ClassField;
import rs.ac.bg.etf.pp1.clss.ClassFormParam;
import rs.ac.bg.etf.pp1.clss.ClassMethod;
import rs.ac.bg.etf.pp1.clss.ClassUtil;
import rs.ac.bg.etf.pp1.clss.MethodTable;
import rs.ac.bg.etf.pp1.context.MethodInvocationContext;
import rs.ac.bg.etf.pp1.des.DesignatorEqualExprStart;
import rs.ac.bg.etf.pp1.des.DesignatorFinder;
import rs.ac.bg.etf.pp1.des.DesignatorReadCheck;
import rs.ac.bg.etf.pp1.print.DesignatorPrinter;
import rs.ac.bg.etf.pp1.print.ExprPrinter;
import rs.ac.bg.etf.pp1.print.StatementPrinter;
import rs.ac.bg.etf.pp1.util.ExprCheckerContext;
import rs.ac.bg.etf.pp1.util.HasComplexOperators;
import rs.ac.bg.etf.pp1.util.MethInvocationCheck;
import rs.ac.bg.etf.pp1.util.Scopes;
import rs.ac.bg.etf.pp1.util.Stack;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class SemanticAnalyzer extends VisitorAdaptor {
	
	private static final boolean PRINT_METHOD_TABLE = false;
	private static String progname;
	public static String getProgName()
	{
		return progname;
	}
	
	private static SemanticAnalyzer instance;
	public static SemanticAnalyzer __instance__()
	{
		return instance;
	}
	
	private MJParser parser;
	
	public SemanticAnalyzer(MJParser parser)
	{
		this.parser = parser;
		this.stmtFuncStack = new Stack<MethodInvocationContext>();
		if ( instance == null )
			instance = this;
	}
	
	/**
	 * STATIC PUBLIC DATA
	 */
	public static final int NO_TYPE = -1;
	public static final int CHAR = 0;
	public static final int INT = 1;
	public static final int BOOL = 2;
	
	Logger log = Logger.getLogger(getClass());
	
	int printCallCount = 0;
	int varDeclCount = 0;
	TIdent identType = null;
	Obj currentMethod = null;
	boolean returnFound = false;
	boolean errorDetected = false;
	
	/* CONST DECLARATION/DEFINITION */
	boolean constDeclError = false;
	int constCurrentType = -1;
	
	/* VAR DECLARATION */
	int varType;
	int varNumber = 0;
	
	/* METHOD DECLARATION */
	public boolean methStart = false;
	
	public void report_error(String message, SyntaxNode info)
	{
		errorDetected = true;
		StringBuilder str = new StringBuilder(message);
		int line = (info == null) ? 0 : info.getLine();
		if ( line != 0 )
			str.append( " on line " ).append(line).append(".");
		log.error(str.toString());
	}
	
	public void report_warning(String message, SyntaxNode info)
	{
		StringBuilder str = new StringBuilder(message);
		int line = (info == null) ? 0 : info.getLine();
		if ( line != 0 )
			str.append( " on line " ).append(line).append(".");
		log.warn(str.toString());
	}
	
	public void report_info(String message, SyntaxNode info)
	{
		StringBuilder str = new StringBuilder(message);
		int line = (info == null) ? 0 : info.getLine();
		if ( line != 0 )
			str.append( " on line " ).append(line).append('.');
		log.info(str.toString());
	}
	
	public void report_checker(ExprChecker checker, SyntaxNode node)
	{
		if ( checker.getRaisedErrorType() == ExprChecker.ERROR )
			report_error(checker.getMessage(), node);
		else if ( checker.getRaisedErrorType() == ExprChecker.WARNING )
			report_warning(checker.getMessage(), node);
	}
	
	public void report_checker(String prepend, ExprChecker checker, SyntaxNode node, String append)
	{
		String mssg = ( prepend != null ? prepend : "" ) + checker.getMessage() + ( append != null ? append : "" );
		if ( checker.getRaisedErrorType() == ExprChecker.ERROR )
			report_error( mssg, node );
		else if ( checker.getRaisedErrorType() == ExprChecker.WARNING )
			report_warning( mssg, node );
	}
	
	public boolean passed()
	{
		return !errorDetected && !parser.errorDetected;
	}
	
	private void unreachableBlock(SyntaxNode snode, SyntaxNode line)
	{	
		snode = snode.getParent();
		StatementPrinter printer = new StatementPrinter((StatementListOptions) snode);
		report_error("Unreachable statement " + printer , line);
	}
	
	/**
	 * ==============================================================================
	 * 		START SEMANTIC ANALYZER
	 * ==============================================================================
	 */	
	
	/**
	 * program __name__
	 * 		constDecl
	 * 		varDecl
	 * {
	 * 		...
	 * } (*)
	 */
	public void visit(Program program)
	{
		if ( Tab.find("main") == Tab.noObj )
		{
			if ( passed() )
				report_error("Could not find reference to method 'main'.", null);
		};
		Tab.chainLocalSymbols(program.obj);
		Tab.closeScope();
	}
	
	/**
	 * program __name__ (*)
	 * ...
	 */
	public void visit(ProgName progName)
	{
		// Add boolean type in symbol table.
		Functions.boolObj = new Obj(Obj.Type, "bool", Tab.intType);
		Tab.currentScope().addToLocals( Functions.boolObj );
		Scopes.put("root", Tab.currentScope());
		
		// Add program symbol inside symbol table.
		((Program)progName.getParent()).obj = Tab.insert(Obj.Prog, progName.getProgname(), Tab.noType);		
		Tab.openScope();
		report_info("PROGRAM > " + progName.getProgname() + " < started", progName);
		Scopes.put(progName.getProgname(), Tab.currentScope());
	}
	
	boolean inConstDecl = false;
	public void visit(IsConstDeclStart startOfConstDecl)
	{
		inConstDecl = true;
		Type t = startOfConstDecl.getType();
		if ( t instanceof TInteger )
			constCurrentType = INT;
		else if ( t instanceof TChar )
			constCurrentType = CHAR;
		else if ( t instanceof TBoolean )
			constCurrentType = BOOL;
	}
	
	public void visit(ConstDeclNoError v)
	{
		
	}
	
	public void visit( DeclarationsEnd end )
	{
		Code.dataSize = varNumber;
	}
	
	/**
	 * const [type]
	 * __varName__ = __val__ (*)
	 */
	public void visit(ConstDeclVarOnlyNoError constDecl)
	{
		String constName = constDecl.getVarName();
		String str = "Definition of const '" + constName + "'";
		String type = "";
		String value = "";
		Struct ct = null;
		int tmpVal = -1;
		ConstDeclType v = constDecl.getConstDeclType();
		if ( v instanceof BoolConst )
		{
			type = "boolean";
			value = ((BoolConst)v).getVal();
			tmpVal = value.equals("false") ? 0 : 1;
		} else if ( v instanceof NumberConst )
		{
			boolean neg = ((NumberConst) v).getNumerusSign() instanceof NumerusSignNegative;
			type = "int";
			value = new String( (neg ? "-" : "") + ((NumberConst)v).getVal() );
			tmpVal = new Integer(value);
		} else if ( v instanceof CharConst )
		{
			type = "char";
			value = ((CharConst)v).getVal();
			tmpVal = value.toCharArray()[1];
		} else
		{
			constDeclError = true;
		};
		if ( !constDeclError )
		{
			if ( Tab.find(constName) == Tab.noObj )
			{
				ct = Functions.getStructForSymTable(constCurrentType, false);
				Obj cnst = Tab.insert(Obj.Con, constName, ct);
				cnst.setAdr(tmpVal);
				str += " of type '" + type + "' with value " + value;
				report_info(str, constDecl);
			} else
			{
				report_error("Const variable '" + constName + "' already defined.", null);
				// Stop parser ?
			};
			
		};
	}
	
	/** 
	 * Catch type of the variable.
	 * int (*) ...
	 *  */
	public void visit(VarDeclType varDeclType)
	{
		identType = null;
		varType = -1;
		Type t = varDeclType.getType();
		if ( t instanceof TInteger )
			varType = INT;
		else if ( t instanceof TChar )
			varType = CHAR;
		else if ( t instanceof TBoolean )
			varType = BOOL;
		else
			identType = ((TIdent) t);
		
	}
	
	/**
	 * public|protected|private (*) Type __varname__ , ...
	 */
	public void visit(VarDeclAccessRights ar)
	{		
		AccessRights a = ar.getAccessRights();
		classIsAssumedAccess = false;
		if ( !(a instanceof AccessNotPresent) && !classIsInside )
		{
			report_error("Cannot use access right on variable outside of the class", ar);
		} else if ( a instanceof AccessNotPresent && classIsInside && !methStart )
		{			
			classAccessRight = ClassField.PROTECTED;
			classIsAssumedAccess = true;
		} else if ( classIsInside )
		{
			if ( a instanceof AccessPrivate )
				classAccessRight = ClassField.PRIVATE;
			else if ( a instanceof AccessProtected )
				classAccessRight = ClassField.PROTECTED;
			else if ( a instanceof AccessPublic )
				classAccessRight = ClassField.PUBLIC;
		};		
	}
	
	/**
	 * Declaration of one variable
	 * ... a   (*)  ...
	 * ... a[] (*) ...
	 */
	public void visit(VarDeclOne varDeclOne)
	{	
		String varName = varDeclOne.getVarName();
		boolean isArray = varDeclOne.getArrDecl() instanceof ArrDeclPresent;
		String varname = varDeclOne.getVarName();
		Struct ct = null;
		
		if ( classIsInside )
		{			
			ClassFormParam p;
			
			// Is assumed access for field(s).
			if ( classIsAssumedAccess )
			{
				report_warning("Assumed protected access for field '" + varname + "'", varDeclOne);
			}
			
			// Add field within the class.
			if ( identType == null )
			{
				ct = Functions.getStructForSymTable(varType, isArray);
				p = ClassFormParam.makeClassFormParam( varname, ct, false );
			} else
			{				
				p = ClassFormParam.makeClassFormParamAsField(identType.getType(), varname, isArray);
			};
			
			if ( !methStart )
			{
				if ( !passed() )
					return;
				
				classInstance.getAllFields().addField(classInstance, p, classAccessRight);
				
				/*
				report_info("Declaration of variable '" + Functions.getAccessRight(classAccessRight) + " " + varname + "' of type '" + 
						Functions.getVariableTypeAsString(e.getType()) + "' inside class '"
						+ classInstance.getName() +"'", varDeclOne);
						*/
			} else	// Add variable to the symbol table.
			{
				Struct st;
				if ( identType != null )
					st = ClassDeclarationList.getInstance().getClassType( identType.getType() );
				else
					st = Functions.getStructForSymTable(varType, isArray);	
				
				if ( classCurrentMethod.parameterExists(varname) )
				{
					report_error( "Variable '" + varname + "' is already declared within the method " + 
								ClassUtil.getMethodSignature(classInstance, classCurrentMethod, true) , varDeclOne );
					return;
				};
				
				Tab.insert( Obj.Var, varname, st );
				Tab.chainLocalSymbols( classCurrentMethod.getObj() );
			};
		} else
		{		
			Obj entry = Tab.find(varName);
			
			if ( entry == Tab.noObj )
			{
				if ( identType != null )
				{
					ct = ClassUtil.getStructForClassname(identType.getType(), isArray);
				} else
					ct = Functions.getStructForSymTable(varType, isArray);
				
				Obj e = Tab.insert( Obj.Var, varName, ct );				
				
				if ( !methStart )
				{			
					report_info("Declaration of global variable '" + varname + "' of type '" + Functions.getVariableTypeAsString(e.getType()) + "'", varDeclOne);
					varNumber++;									
				};
			} else
			{
				if ( entry.getLevel() > 0  )
				{
					if ( methStart )
					{
						if ( entry.getFpPos() == 0 )
							report_error("Local variable '" + varName + "' is already defined.", null);
						else
							report_error("Formal parameter '" + varName + "' already exists. Error declaring variable", varDeclOne);
					} else
						report_error("Global variable '" + varName + "' is already defined.", null);
				} else
				{
					ct = Functions.getStructForSymTable(varType, isArray);				
					Tab.insert(Obj.Var, varName, ct);
				};
			};
		};

	}
	
	/**
	 * ==============================================================================
	 * 		STATEMENT LIST OPTIONS
	 * ==============================================================================
	 */
	
	/**
	 * print( Expr [, num] ) (*) ;
	 */
	public void visit(PrintStatement printStatement)
	{
		if ( this.methHasReturnStatement || this.stmtForwardUnreachable )
		{
			unreachableBlock(printStatement, printStatement.getPrintWrapper());
			return;
		};		
	
		ExprNoError e = (ExprNoError) printStatement.getPrintWrapper().getPrintWrapperAll().getExpr();
		
		HasComplexOperators hco = new HasComplexOperators();
		hco.performCheck(e);
	
		ExprChecker checkerBoolean = new ExprChecker(Functions.boolType);
		ExprChecker checkerNumber = new ExprChecker(Tab.intType);
		ExprChecker checkerChar = new ExprChecker(Tab.charType);
		boolean c1, c2, c3;
		checkerBoolean.doPerformComplexExpressionCheck( hco.cond() );
		checkerNumber.doPerformComplexExpressionCheck( hco.cond() );
		checkerChar.doPerformComplexExpressionCheck( hco.cond() );
		
		checkerBoolean.getContext().reportInDesignatorSearch = false;
		checkerNumber.getContext().reportInDesignatorSearch = false;
		checkerChar.getContext().reportInDesignatorSearch = false;
		
		c1 = checkerBoolean.process(e);
		c2 = checkerNumber.process(e);
		c3 = checkerChar.process(e);
		
		//System.out.println("boolean=" + c1 + ", number=" + c2 + ", char=" + c3);
		
		// None of checkers passed.
		if ( !c1 && !c2 && !c3 )
		{
			report_error("Error occured in expression during call of method print()", printStatement.getParent());
		};
	}
	
	/**
	 * read( Designator ); (*)
	 */
	public void visit(ReadStatement readStatement)
	{
		if ( this.methHasReturnStatement || this.stmtForwardUnreachable )
		{
			unreachableBlock(readStatement, readStatement);
			return;
		};
		
		DesignatorReadCheck drc = new DesignatorReadCheck(this);
		readStatement.getDesignator().traverseBottomUp(drc);
	}
	
	/**
	 * ==============================================================================
	 * 		CLASS DECLARATION
	 * ==============================================================================
	 */
	
	public boolean classIsInside = false;
	public Obj classObj = null;
	public rs.ac.bg.etf.pp1.clss.ClassDeclaration classInstance;
	public int classAccessRight;
	public boolean classIsAssumedAccess;
	public ClassMethod classCurrentMethod;
	public AccessRights classMethodAccessRight = null;
	
	/**
	 * ==============================================================================
	 * 		METHOD DECLARATION
	 * ==============================================================================
	 */
	
	int methActPars;
	public int methFormPars;
	public String methName;
	public boolean methHasReturnStatement;
	public boolean methReturnStarted;
	
	/**
	 * void __name__ (*)
	 *  */
	public void visit(MethodDeclStart methodDeclStart)
	{
		if ( classIsInside )
		{			
			ClassUtil.processMethod(this, methodDeclStart.getMethodReturnValue(), methodDeclStart.getMethodName(), classMethodAccessRight, methodDeclStart, false);
			classMethodAccessRight = null;			
		} else
		{			
			String methName = methodDeclStart.getMethodName();
			Obj method = Tab.find(methName);
			
			if ( method == Tab.noObj )
			{
				// Set method declaration mode.
				this.methStart = true;
				this.methName  = methodDeclStart.getMethodName();
				this.methFormPars = 0;
				this.methHasReturnStatement = false;
				
				MethodReturnValue mrv = methodDeclStart.getMethodReturnValue();
				
				Struct retVal = mrv instanceof MethodReturnVoid ? Tab.noType : Functions.getTypeBySubstitution( ((MethodReturnType)mrv).getType() );
				
				Obj o = Tab.insert(Obj.Meth, methodDeclStart.getMethodName(), retVal);
				((MethodDecl)methodDeclStart.getParent()).obj = o;
				Tab.openScope();
				
				Scopes.put(methodDeclStart.getMethodName(), Tab.currentScope());
			} else
			{	
				if ( classIsInside )
					return;
				
				if ( method.getKind() == Obj.Con )
					report_error("Constant name '" + methName + "' cannot be used as method name.", null);
				else if ( method.getKind() == Obj.Var )
					report_error("Variable name '" + methName + "' cannot be used as method name.", null);
				else if ( method.getKind() == Obj.Meth )
					report_error("Method with name '" + methName + "' is already defined.", null);
				else
					report_error("Error naming method with name '" + methName + "'.", null);
			};
		};
	}
	
	/**
	 * public|protected|private abstract Type __methodName__ (*) ( ... );
	 */
	public void visit(AbstractMethodDeclStart s)
	{			
		ClassUtil.processMethod(this, s.getMethodReturnValue(), s.getI3(), s.getAccessRights(), s, true);
	}
	
	/**
	 * public|protected|private abstract Type __methodName__( ... ); (*)
	 */
	public void visit( AbstractMethodDecl amd )
	{				
		classCurrentMethod.methodDeclarationFinished( classInstance );
	}
	
	
	/**
	 * __retVal__ methodName(
	 * 		... 
	 * 		Type __paramname__ []? (*)
	 * 		...
	 * ) ...
	 */
	public void visit(FormParamNoError fp)
	{
		if ( classIsInside )
		{
			if ( !passed() )
				return;
			
			this.methFormPars++;
			boolean isArray = fp.getFormParsArray() instanceof FormParsIsArray;
			Struct s = Functions.getTypeBySubstitution(fp.getType());
			
			if ( fp.getType() instanceof TIdent )
			{				
				TIdent t = (TIdent) fp.getType();
				if ( ClassDeclarationList.getInstance().getClassDeclaration(t.getType()) == null )
				{
					report_error("Type '" + t.getType() + "' does not exist", fp);
					return;
				};
			};			
			
			String xcName = null;
			if ( fp.getType() instanceof TIdent )
				xcName = ((TIdent)fp.getType()).getType();
			
			ClassFormParam x;
			if ( xcName != null )
				x = ClassFormParam.makeClassFormParamReference(xcName, fp.getParamName(), isArray);
			else
				x = ClassFormParam.makeClassFormParam( fp.getParamName(), s, isArray );
			
			if ( classCurrentMethod.parameterExists(x) )
			{
				report_error("Cannot redeclare formal parameter '" + fp.getParamName() + "'", fp);
			} else
			{
				classCurrentMethod.addFormalParam(x);
			};
			
		} else	// Basic param's declaration.
		{
			String paramName = fp.getParamName();
			// Check if param already exists.
			Obj entry = Tab.find(paramName);
			if ( entry != Tab.noObj )
			{
				if ( entry.getLevel() > 0 )
					report_error("Cannot redeclare formal parameter '" + paramName + "'", fp);
				else
				{
					addMethodFormalParam(fp);
				};
			} else	// Param does not exists in the symbol table.
			{
				addMethodFormalParam(fp);
			};
		};
	}
	
	private void addMethodFormalParam(FormParamNoError fp)
	{
		this.methFormPars++;
		boolean isArray = fp.getFormParsArray() instanceof FormParsIsArray;
		Struct s = Functions.getTypeBySubstitution(fp.getType());
		if ( s == Tab.noType )
		{
			TIdent t = (TIdent) fp.getType();
			report_error("Type '" + t.getType() + "' does not exist", fp);
		} else
		{			
			Obj e = Tab.insert( Obj.Var, fp.getParamName(), Functions.getStructRegular( s, isArray) );
			e.setFpPos( this.methFormPars );
		};
	}
	
	/**
	 * End of method declaration.
	 */
	public void visit(MethodDecl methodDecl)
	{
		if ( methStart )
		{
			if ( classIsInside )
			{
				if ( !passed() )
					return;
				
				if ( !classCurrentMethod.isAbstractMethod() && classCurrentMethod.getReturnValue().getType() != Tab.noType 
						&& !methHasReturnStatement )
				{
					if ( passed() )
						report_error("Method '" + ClassUtil.getMethodSignature( classInstance , classCurrentMethod, false) + 
								"' should return type '" + Functions.getVariableTypeAsString( classCurrentMethod.getReturnValue().getType() ) + "'.", null);
				};
			} else
			{			
				Tab.chainLocalSymbols(methodDecl.obj);	
				Tab.closeScope();
				
				Obj method = Tab.find(methName);
				
				if ( method.getType() != Tab.noType && !methHasReturnStatement )
				{
					report_error("Method '" + Functions.getMethodSignature(method) + 
							"' should return type '" + Functions.getVariableTypeAsString(method.getType()) + "'.", null);
				};
			};
			
			// Reset method declaration.
			methStart = false;
		};
	}
	
	/**
	 * End of method's formal parameters.
	 * 
	 * Type __methodname__( ... ) (*) { ... }
	 */
	public void visit(MethodDeclFormParsEnd e)
	{		
		if ( !classIsInside )
		{
			// Set number of formal params.
			MethodDecl md = ((MethodDecl)e.getParent());
			md.obj.setLevel(methFormPars);
			Tab.chainLocalSymbols(md.obj);		
		} else
		{
			/*
			if ( !passed() )
				return;
				*/
			
			classCurrentMethod.chainParametersIntoSymbolTable();
		};
	}
	
	/**
	 * __methodname__ ( ... ) (*) ;
	 */
	public void visit(FuncCall fc)
	{
		MethodInvocationContext context = stmtFuncStack.pop();
		if ( passed() )
		{			
			DesignatorFinder finder = new DesignatorFinder(this);
			fc.getFuncCallStart().getDesignator().traverseBottomUp(finder);
			Obj o = finder.getObject();
			Functions.functionCallCheck( this, o, fc );
		};
	}
	
	/**
	 * return (*) Expr;
	 */
	public void visit( ReturnExprStart s )
	{
		methReturnStarted = true;
	}
	
	/**
	 * return Expr; (*) 
	 */
	public void visit(ReturnExpr re)
	{	
		if ( !passed() )
		{
			methReturnStarted = false;
			return;
		};
		
		if ( !(stmtIfDepth > 0 || stmtForLoopNested > 0 || stmtForeachLoopNested > 0) )
			this.methHasReturnStatement = true;
		
		Obj method;
		if ( !classIsInside )
			method = Tab.find( methName );
		else
			method = classCurrentMethod.getReturnValue().getObj();
		
		RetVal rv = re.getRetVal();
		Struct retValExpected = method.getType();
		
		if ( method.getType() == Tab.noType )
		{
			if ( rv instanceof RetValType )
			{
				report_error(
						"Method '" + Functions.getMethodSignature(method) + "' " +
						"should not return any type.", null);
			};
		} else if ( method.getType().getKind() == Struct.Class )
		{
			if ( rv instanceof RetValVoid )
			{
				report_error("Method need to return type of '" + method.getName() + "'", re);
			} else
			{
				rs.ac.bg.etf.pp1.clss.ClassDeclaration decl = ClassDeclarationList.getInstance().getClassDeclaration(method.getName());
				ExprChecker checker = new ExprChecker( retValExpected );
				ExprCheckerContext c = checker.getContext();
				c.isExpectedReference = true;
				c.classDeclaration = decl;
				//c.isReference = true;
				boolean valid = checker.process( (ExprNoError) ((RetValType) rv).getExpr() );
				if ( !valid )
				{
					report_checker(checker, re);
				}
			};
			
		} else
		{			
			if ( rv instanceof RetValType )
			{
				ExprChecker checker = new ExprChecker( retValExpected );
				boolean valid = checker.process( (ExprNoError) ((RetValType) rv).getExpr() );
				if ( !valid )
					report_checker( null, checker, null, ". Modify return statement for method '" + methName + "'." );
			} else	// If method's return type is void.
			{
				report_error(
						"Method '" + Functions.getMethodSignature(method) + "' " +
						"should return type '" + Functions.getVariableTypeAsString(retValExpected) + "'.", null);
			};
		};
		
		methReturnStarted = false;
	}
	
	/**
	 * ==============================================================================
	 * 		EXPRESSIONS
	 * ==============================================================================
	 */
	
	String exprVarName = null;
	boolean exprIsRef = false;
	Struct exprEqualStruct = null;
	DesignatorEqualExprStart exprDesFinder = null;
	DesignatorFinder exprDesignator = null;
	
	public void visit(Designator designator)
	{		
		if ( !passed() )
			return;
		
		if ( designator.getParent() instanceof FactorDesignator )
		{
			DesignatorFinder f = new DesignatorFinder(this);
			f.resetDesignatorFinder(false);
			f.setCheck(false);
			designator.traverseBottomUp(f);
			
			FactorDesignator g = (FactorDesignator) designator.getParent();
			FactorParens fp = g.getFactorParens();
			if ( fp instanceof FactorParensPresent )
			{
				
				exprDesignator = f;
				stmtFuncActPars = 0; 
				stmtFuncObj = f.getObject();
				
			};
		};
	}
	
	/**
	 * ( Expr ) (*) 
	 */
	public void visit(FactorWithExpr f)
	{
		f.struct = f.getExpr().struct;
	}
	
	/**
	 * Factor MulopFactor (*)
	 */
	public void visit(Term term)
	{

	}
	
	/**
	 * MulopFactor ::= (MulopFactorAll) MulopFactor Mulop Factor (*)
	 */
	public void visit(MulopFactorAll m)
	{
		m.struct = m.getFactor().struct;
	}
	
	/**
	 * Expr
	 * ["-"] Term { Addop Term } (*)
	 */
	public void visit(ExprNoError x)
	{
		
	}
	
	/**
	 * In program, assign variable with value.
	 * 
	 * __varname__ (*) = <Expr>;
	 */
	public void visit(EqualExprStart equalExprStart)
	{
		if ( !passed() )
			return;
		
		DesignatorEqualExprStart dd = new DesignatorEqualExprStart(this);
		equalExprStart.getDesignator().traverseBottomUp(dd);
		
		exprDesFinder = dd;
		Obj entry = dd.getObject();
		// TODO: Change this part...
		exprVarName = null;
		exprIsRef = dd.getFactorsNum() == 1 ? dd.getFirstFactor().getType().isRefType() : entry.getType().isRefType();
		exprEqualStruct = entry.getType();		
	}
	
	/**
	 * Equal Expression
	 * In program, assign variable with value.
	 * 
	 * __varname__ = <Expr> (*) ;
	 */
	public void visit(EqualExpr equalExpr)
	{
		if ( !passed() )
			return;
		
		ExprNoError e = (ExprNoError) equalExpr.getExpr();
		
		if ( passed() )
		{		
			ExprChecker checker = new ExprChecker(exprEqualStruct);
			ExprCheckerContext c = checker.getContext();
			c.isEqualStatement = true;
			c.isReference = exprIsRef;
			c.allowNullReference = exprIsRef;
			c.varname = exprVarName;
			c.isExpectedReference = exprDesFinder.isReference();
			c.classDeclaration = ClassUtil.getClassDeclarationFromObj( exprDesFinder.getObject() );
			
			boolean valid = checker.process(e);
			if ( !valid )
				this.report_checker(checker, equalExpr);			
		};
		
		exprIsRef = false;
		exprEqualStruct = null;
		exprVarName = null;
		exprDesFinder = null;
	}
	
	/**
	 * <Designator> <FactorParens> (*)
	 */
	public void visit(FactorDesignator factorDesignator)
	{	
		if ( !passed() )
			return;
		
		FactorParens p = factorDesignator.getFactorParens();
		if ( p instanceof FactorParensPresent )			// Function call.
		{			
			DesignatorFinder f = new DesignatorFinder(this);
			factorDesignator.getDesignator().traverseBottomUp(f);
			if ( passed() )
				Functions.functionCallCheck( this, f.getObject(), factorDesignator );
		};		
	}
	
	/**
	 * designator.__varname__ = new __type__[NUMBER] (*) ;
	 */
	public void visit(FactorNew fn)
	{
		if ( !passed() )
			return;
		
		int type = Functions.getStructIntBasedOnTName(fn.getType());
		/*	
		boolean basicType = type == Struct.Int || type == Struct.Char || type == Struct.Bool;
		if ( basicType )
		{
			String obj = fn.getFactorExpr() instanceof FactorExprAll ? "s" : "";
			report_error("Cannot create new object" + obj + " of basic type " + Functions.getNameForStruct(type), fn);
			return;
		};
		*/
		
		// It's array on the right side of the '=' sign.
		if ( fn.getFactorExpr() instanceof FactorExprAll )
		{
			FactorExprAll f = (FactorExprAll) fn.getFactorExpr();						

			if ( exprEqualStruct != null && 
					( 
							exprDesFinder.isArrayAccess()							
							||
							exprDesFinder.isClassReference()								
							||
							exprEqualStruct.getKind() == Struct.Array && exprEqualStruct.getElemType().getKind() != type 
							|| 
							exprEqualStruct.getKind() != Struct.Array && exprEqualStruct.getKind() != type 
					)
				) 
			{				
				
				Obj o = exprDesFinder.getFirstFactor();
				String typeName = null;
				
				if ( exprDesFinder.isArrayAccess() )
				{					
					int t = o.getType().getElemType().getKind();
					
					if ( t == Struct.Int || t == Struct.Char || t == Struct.Bool )
						typeName = Functions.getNameForStruct(t);
					else if ( t == Struct.Class )
						typeName = ClassUtil.getClassNameFromObj(o);
						
					report_error("Variable '" + o.getName() + "[]' is an array of type '" + 
							typeName + "'. Remove []", fn);
					
				} else if ( exprDesFinder.isClassReference() )
				{					
					if ( exprDesFinder.getObject().getType().getKind() != Struct.Array )
						report_error("Variable '" + o.getName() + "' is not an array of type '" + 
								exprDesFinder.getClassReference().getName() + "'. Remove []", fn);
				} else
				{
					Struct t = exprEqualStruct;
					if ( t.getKind() == Struct.Array )
						t = t.getElemType();
					
					
					report_error("Cannot allocate new elements of type '" + 
						Functions.getNameForStruct(type) + "' for the variable with type '" + 
							Functions.getNameForStruct(t.getKind()) + "'", fn);
				};
			} else if ( exprEqualStruct == null && stmtIfDepth == 0 && stmtForLoopNested == 0 )
			{
				report_error("Operator 'new' has to be used only on reference types", fn);
			};
			fn.struct = Tab.nullType;
		} else
		{
			if ( methReturnStarted )
				return;		
			
			// Check if user is trying to instantiate object of abstract class.
			if ( fn.getType() instanceof TIdent )
			{
				String className = ((TIdent)fn.getType()).getType();
				rs.ac.bg.etf.pp1.clss.ClassDeclaration c = ClassUtil.getClassDeclarationByName(className);
				if ( c != null && !c.canInstantiate() ) 
				{
					report_error( "Cannot instantiate new object of type '" + className + "' because type is abstract", fn );
					return;
				};					
			};
				
			Obj h = exprDesFinder.getObject();
			String typeName = null;
			if ( h.getType().getKind() == Struct.Array )
			{
				Struct s = h.getType().getElemType();
				typeName = s.getKind() == Struct.Class ? ClassUtil.getClassNameFromObj(h) : Functions.getNameForStruct(s.getKind());
				report_error( "Variable '" + h.getName() + "[]' is an array of type '" + typeName + "'. Add '[elemNo]' clause", fn );
			};
		};
		
	}
	
	/**
	 * __varname__-- (*);
	 */
	public void visit(MinusMinusExpr mme)
	{
		ExprChecker.checkIncrementOperators(this, mme, mme.getDesignator());
	}
	
	/**
	 * __varname__++ (*) ;
	 */
	public void visit(PlusPlusExpr ppe)
	{
		ExprChecker.checkIncrementOperators(this, ppe, ppe.getDesignator());
	}
	
	/**
	 * ==============================================================================
	 * 		STATEMENTS
	 * ==============================================================================
	 */	
	
	int stmtFuncActPars;
	Obj stmtFuncObj;
	Stack<MethodInvocationContext> stmtFuncStack;
	public boolean stmtAllowMultipleStatements = false;
	public boolean stmtIsForLoop = false;
	public boolean stmtIsForeachLoop = false;
	public int stmtForLoopNested = 0; 
	public int stmtForeachLoopNested = 0;
	int stmtIfDepth = 0;
	public boolean stmtForwardUnreachable = false;
	public int stmtMultipleStatements = 0;
	public int stmtAllowedMultipleStatements = 0;
	
	public void visit( MultipleStatementsStart mss )
	{
		stmtMultipleStatements++;
		if ( !stmtAllowMultipleStatements || stmtMultipleStatements > stmtAllowedMultipleStatements )
			report_error( "{ Statement [, Statement] } cannot be used here", mss.getParent() );
	}
	
	public void visit( MultipleStatements ms )
	{
		stmtMultipleStatements--;
	}
	
	public void visit( ForStatementStart fss )
	{
		stmtAllowMultipleStatements = true;
		stmtIsForLoop = true;
		stmtForLoopNested++;
		stmtAllowedMultipleStatements++;
	}
	
	public void visit( ForeachStatementStart start )
	{
		stmtAllowMultipleStatements = true;
		stmtIsForeachLoop = true;
		stmtForeachLoopNested++;
		stmtAllowedMultipleStatements++;
	}
	
	public void visit( ForeachStatement s )
	{
		stmtForeachLoopNested--;
		stmtAllowedMultipleStatements--;
		if ( stmtForLoopNested == 0 && stmtForeachLoopNested == 0 )
		{
			stmtIsForeachLoop = false;
			
			// Check foreach loop.
			ForeachLoopChecker flc = new ForeachLoopChecker(this);
			s.traverseBottomUp(flc);
		};
	}
	
	/**
	 * for ( ... ; ... ; ... ) { ... } (*)
	 */
	public void visit( ForStatementEnd fse )
	{
		stmtForLoopNested--;
		stmtAllowedMultipleStatements--;
		if ( stmtForLoopNested == 0 && stmtForeachLoopNested == 0 )
		{
			stmtAllowMultipleStatements = false;
			stmtIsForLoop = false;
			// Check for loop.
			ForLoopChecker flc = new ForLoopChecker(this);
			fse.getParent().traverseBottomUp(flc);
		};
	}
	
	/**
	 * break; (*);
	 */
	public void visit(BreakStatement bs)
	{
		if ( passed() && !stmtIsForLoop && !stmtIsForeachLoop )
			report_error("Cannot use 'break' statement outside for/foreach loop", bs);
	}
	
	/**
	 * continue; (*);
	 */
	public void visit(ContinueStatement cs)
	{
		if ( passed() && !stmtIsForLoop && !stmtIsForeachLoop )
			report_error("Cannot use 'continue' statement outside for/foreach loop", cs);
	}
	
	/**
	 * if ( <Condition> (*) ) { ... };
	 */
	public void visit(IfCondition ifc)
	{
		if ( !passed() )
			return;
		
		ConditionChecker cc = new ConditionChecker(this);
		ifc.getCondition().traverseBottomUp(cc);
	}
	
	/**
	 * if ( (*) ... ) { ... }
	 */
	public void visit(IfStart is)
	{
		stmtAllowMultipleStatements = true;
		stmtIfDepth++;
		stmtAllowedMultipleStatements++;
	}
	
	/**
	 * if ( <Condition> ) (<Matched>|<Unmatched>) (*)
	 */
	public void visit(UnmatchedIf uif)
	{
		stmtIfDepth--;
		if ( stmtIfDepth == 0 )
			stmtAllowMultipleStatements = false;
		stmtAllowedMultipleStatements--;
	}
	
	
	/**
	 * if ( <Condition> ) <Matched> else <Unmatched>; (*)
	 */
	public void visit( UnmatchedIfElse f )
	{
		stmtIfDepth--;
		if ( stmtIfDepth == 0 )
			stmtAllowMultipleStatements = false;
		stmtAllowedMultipleStatements--;
		
		ConditionChecker cc = new ConditionChecker(this);
		f.traverseBottomUp(cc);
	}
	
	/**
	 * if ( <Condition> ) <Matched> else <Matched>; (*)
	 */
	public void visit(MatchedIfElse f)
	{
		stmtIfDepth--;
		if ( stmtIfDepth == 0 )
			stmtAllowMultipleStatements = false;
		stmtAllowedMultipleStatements--;
		
		ConditionChecker cc = new ConditionChecker(this);
		f.traverseBottomUp(cc);
	}
	
	/**
	 * designator.__methodname__ (*) ( ... ) ;
	 */
	public void visit(FuncCallStart f)
	{
		DesignatorFinder finder = new DesignatorFinder(this);
		finder.traverse( f.getDesignator() );
		
		MethodInvocationContext invocationContext = new MethodInvocationContext( finder.getObject() );
		stmtFuncStack.push( invocationContext );
		invocationContext.isClassMethod = finder.isClassReference() || classIsInside;
		invocationContext.exprDesignator = finder;
		
	}

	/**
	 * Method invocation.
	 * 
	 * __methodname__( ... (*) ) ;
	 */
	public void visit(ActPars ap)
	{
		int params = 0;
		MethodInvocationContext context = stmtFuncStack.top();
		if ( context.methodObj != Tab.noObj && context.methodObj.getKind() == Obj.Meth )
		{
			params = context.methodObj.getLevel();
			int paramsGiven = stmtFuncStack.top().parametersProvided;
			if ( classIsInside && ( context.methodObj != Tab.lenObj && context.methodObj != Tab.chrObj && context.methodObj != Tab.ordObj )
					|| context.exprDesignator.isClassReference() )
				paramsGiven++;
			if ( params != paramsGiven )
			{
				report_error(
						"Method '" + context.methodObj.getName() + "' should be provided with " + params + 
						" " + Functions.getPluralForWord("params", params) + ", but " + paramsGiven + " given"
						, ap.getParent().getParent() );
				report_error( "============================================================================", null );
				report_error( "\t\tMethod signature :: " + Functions.getMethodSignature(context.methodObj), null );
				report_error( "============================================================================", null );
			} else
			{
				boolean f = context.isClassMethod;
				int invocationParametersOld = context.parametersProvided;
				
				SyntaxNode node = context.designator == null ? ap.getParent().getParent() : context.designator;
				MethInvocationCheck mic = new MethInvocationCheck(this, context, node);
				context.isClassMethod = classIsInside && ( context.methodObj != Tab.lenObj && context.methodObj != Tab.chrObj && context.methodObj != Tab.ordObj ) 
						|| context.exprDesignator != null && context.exprDesignator.isClassReference();
				ap.traverseBottomUp(mic);
				
				context.parametersProvided = invocationParametersOld;
				context.isClassMethod = f;
			};
		};
	}
	
	/**
	 * Method invocation without parameters.
	 * 
	 * designator.__methodname__( (*) ) ;
	 */
	public void visit(ActParsNotPresent ap)
	{
		if ( !passed() )
			return;
		
		int params = 0;
		MethodInvocationContext context = stmtFuncStack.top();
		if ( context.methodObj != Tab.noObj && context.methodObj.getKind() == Obj.Meth )
		{
			params = context.methodObj.getLevel();
			int paramsGiven = stmtFuncStack.top().parametersProvided;
			if ( classIsInside || context.exprDesignator.isClassReference() )
				paramsGiven++;
			if ( params != paramsGiven )
			{
				report_error(
						"Method '" + context.methodObj.getName() + "' should be provided with " + params + 
						" " + Functions.getPluralForWord("params", params) + ", but " + paramsGiven + " given"
						, ap.getParent().getParent() );
				report_error( "============================================================================", null );
				report_error( "\t\tMethod signature :: " + Functions.getMethodSignature(context.methodObj), null );
				report_error( "============================================================================", null );
			};
		};
	}
	
	/**
	 * Called after every actual param.
	 * 
	 * __methodname__( Expr (*), ... ) ;
	 */
	public void visit(ActParam ap)
	{
		//this.stmtFuncActPars++;
		stmtFuncStack.top().parametersProvided++;
	}
	
	/**
	 * designator.__methodname__( (*) ... )
	 */
	public void visit(FactorParensStart fps)
	{		
		FactorDesignator fd = (FactorDesignator) fps.getParent().getParent();
		DesignatorFinder df = new DesignatorFinder(this);
		df.traverse( fd.getDesignator() );
		
		MethodInvocationContext invocationContext = new MethodInvocationContext( df.getObject() );
		stmtFuncStack.push( invocationContext );
		invocationContext.exprDesignator = df;
	}
	
	/**
	 * designator.__methodname__( ... ) (*)
	 */
	public void visit(FactorParensEnd fpe)
	{
		MethodInvocationContext context = stmtFuncStack.pop();
	}
	
	/**
	 * Statement; (*);
	 */
	public void visit(DesignatorStatementWrapperEnded e)
	{
		if ( this.methHasReturnStatement || this.stmtForwardUnreachable )
		{
			DesignatorStatementWrapperNoError w = null;
			try {
				w = (DesignatorStatementWrapperNoError) e.getDesignatorStatementWrapper();
			} catch( Exception exe ) {
				return;
			};
			
			DesignatorStatement ds = w.getDesignatorStatement();
			
			if ( ds instanceof EqualExpr )
			{
				Designator designator = ((EqualExpr) ds).getEqualExprStart().getDesignator();
				DesignatorPrinter dprinter = new DesignatorPrinter(designator);
				ExprNoError ex = (ExprNoError) ((EqualExpr) ds).getExpr();
				ExprPrinter printer = new ExprPrinter(ex);
				report_error("Unreachable statement \"" + dprinter + " = " + printer + "\"", designator);
			} else if ( ds instanceof PlusPlusExpr )
			{
				Designator designator = ((PlusPlusExpr) ds).getDesignator();
				DesignatorPrinter dprinter = new DesignatorPrinter(designator);
				report_error("Unreachable statement \"" + dprinter + "++\"", designator);
			} else if ( ds instanceof MinusMinusExpr )
			{
				Designator designator = ((PlusPlusExpr) ds).getDesignator();
				DesignatorPrinter dprinter = new DesignatorPrinter(designator);
				report_error("Unreachable statement \"" + dprinter + "--\"", designator);
			} else
				report_error("Unreachable statement", e);
		};	
	}
	
	/**
	 * ==============================================================================
	 * 		CLASS DECLARATION
	 * ==============================================================================
	 */
	
	public void visit(ClassDeclStart s)
	{				
		this.methStart = false;
		ClassUtil.checkClassDeclStart(this, s);
		this.classIsInside = true;
		rs.ac.bg.etf.pp1.clss.ClassDeclarationList classes = rs.ac.bg.etf.pp1.clss.ClassDeclarationList.getInstance();
		rs.ac.bg.etf.pp1.clss.ClassDeclaration superClass = null;
		ExtendsClause c = s.getExtendsClause();
		if ( c instanceof ExtendsPresent )
		{
			String superClassName = ((TIdent)(((ExtendsPresent) c).getType())).getType();
			superClass = ClassUtil.getClassDeclarationByName( superClassName );
		};
		
		// Add class into the list.
		rs.ac.bg.etf.pp1.clss.ClassDeclaration clss = new rs.ac.bg.etf.pp1.clss.ClassDeclaration( s.getI1(), false, superClass, s.getLine());
		classes.add(clss);
		classObj = clss.getSymbolTableEntry();
		classInstance = clss;
	}
	
	public void visit(ClassDeclNoError e)
	{		
		MethodTable m = classInstance.classDeclarationFinished(e);
		if ( PRINT_METHOD_TABLE )
			System.out.println(m);
		
		// Reset all variables.
		classAccessRight = -1;
		classIsInside = false;
		classObj = null;
		classInstance = null;
	}
	
	public void visit(ClassDeclMethodStart s)
	{		
		classMethodAccessRight = s.getAccessRights();
	}
	
	public void visit(ClassDeclMethod end)
	{		
		classCurrentMethod.methodDeclarationFinished(classInstance);
	}
	
	/**
	 * ==============================================================================
	 * 		ABSTRACT CLASS DECLARATION
	 * ==============================================================================
	 */
	
	/**
	 * abstract class __classname__ [extends __classname__] (*)
	 */
	public void visit( AbstractClassDeclStart s )
	{
		this.methStart = false;
		this.classIsInside = true;
		ClassUtil.checkClassDeclStart(this, s);
		
		rs.ac.bg.etf.pp1.clss.ClassDeclarationList classes = rs.ac.bg.etf.pp1.clss.ClassDeclarationList.getInstance();
		rs.ac.bg.etf.pp1.clss.ClassDeclaration superClass = null;
		ExtendsClause c = s.getExtendsClause();
		if ( c instanceof ExtendsPresent )
		{
			String superClassName = ((TIdent)(((ExtendsPresent) c).getType())).getType();
			superClass = classes.getClassDeclaration( superClassName );
		};
		
		// Add class into the list.
		rs.ac.bg.etf.pp1.clss.ClassDeclaration clss = new rs.ac.bg.etf.pp1.clss.ClassDeclaration( s.getI1(), true, superClass, s.getLine() );
		classes.add(clss);
		classObj = clss.getSymbolTableEntry();
		classInstance = clss;
	}
	
	public void visit( AbstractClassDeclNoError end )
	{
		MethodTable m = classInstance.classDeclarationFinished(end);
		if ( PRINT_METHOD_TABLE )
			System.out.println(m);
		
		
		// Reset all variables.
		classAccessRight = -1;
		classIsInside = false;
		classObj = null;
		classInstance = null;
	}
	
	public void visit(AbstractClassDeclMethodsRegularStart s)
	{
		classMethodAccessRight = s.getAccessRights();
	}
	
	public void visit(AbstractClassDeclMethodsRegular s)
	{
		classCurrentMethod.methodDeclarationFinished( classInstance );
	}
	
	/**
	 * ==============================================================================
	 * 		MISCELLANEOUS
	 * ==============================================================================
	 */
	
	public void visit(TInteger i)
	{
		i.struct = Tab.intType;
	}
	
	public void visit(TChar i)
	{
		i.struct = Tab.charType;
	}
	
	public void visit(TBoolean i)
	{
		i.struct = Functions.boolType;
	}
	
}
