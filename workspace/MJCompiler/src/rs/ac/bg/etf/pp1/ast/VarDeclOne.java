// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class VarDeclOne implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private String varName;
    private ArrDecl ArrDecl;

    public VarDeclOne (String varName, ArrDecl ArrDecl) {
        this.varName=varName;
        this.ArrDecl=ArrDecl;
        if(ArrDecl!=null) ArrDecl.setParent(this);
    }

    public String getVarName() {
        return varName;
    }

    public void setVarName(String varName) {
        this.varName=varName;
    }

    public ArrDecl getArrDecl() {
        return ArrDecl;
    }

    public void setArrDecl(ArrDecl ArrDecl) {
        this.ArrDecl=ArrDecl;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ArrDecl!=null) ArrDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ArrDecl!=null) ArrDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ArrDecl!=null) ArrDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("VarDeclOne(\n");

        buffer.append(" "+tab+varName);
        buffer.append("\n");

        if(ArrDecl!=null)
            buffer.append(ArrDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [VarDeclOne]");
        return buffer.toString();
    }
}
