// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class AbstractClassDeclMethodsRegular extends AbstractClassDeclMethods {

    private AbstractClassDeclMethods AbstractClassDeclMethods;
    private AbstractClassDeclMethodsRegularStart AbstractClassDeclMethodsRegularStart;
    private MethodDecl MethodDecl;

    public AbstractClassDeclMethodsRegular (AbstractClassDeclMethods AbstractClassDeclMethods, AbstractClassDeclMethodsRegularStart AbstractClassDeclMethodsRegularStart, MethodDecl MethodDecl) {
        this.AbstractClassDeclMethods=AbstractClassDeclMethods;
        if(AbstractClassDeclMethods!=null) AbstractClassDeclMethods.setParent(this);
        this.AbstractClassDeclMethodsRegularStart=AbstractClassDeclMethodsRegularStart;
        if(AbstractClassDeclMethodsRegularStart!=null) AbstractClassDeclMethodsRegularStart.setParent(this);
        this.MethodDecl=MethodDecl;
        if(MethodDecl!=null) MethodDecl.setParent(this);
    }

    public AbstractClassDeclMethods getAbstractClassDeclMethods() {
        return AbstractClassDeclMethods;
    }

    public void setAbstractClassDeclMethods(AbstractClassDeclMethods AbstractClassDeclMethods) {
        this.AbstractClassDeclMethods=AbstractClassDeclMethods;
    }

    public AbstractClassDeclMethodsRegularStart getAbstractClassDeclMethodsRegularStart() {
        return AbstractClassDeclMethodsRegularStart;
    }

    public void setAbstractClassDeclMethodsRegularStart(AbstractClassDeclMethodsRegularStart AbstractClassDeclMethodsRegularStart) {
        this.AbstractClassDeclMethodsRegularStart=AbstractClassDeclMethodsRegularStart;
    }

    public MethodDecl getMethodDecl() {
        return MethodDecl;
    }

    public void setMethodDecl(MethodDecl MethodDecl) {
        this.MethodDecl=MethodDecl;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(AbstractClassDeclMethods!=null) AbstractClassDeclMethods.accept(visitor);
        if(AbstractClassDeclMethodsRegularStart!=null) AbstractClassDeclMethodsRegularStart.accept(visitor);
        if(MethodDecl!=null) MethodDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(AbstractClassDeclMethods!=null) AbstractClassDeclMethods.traverseTopDown(visitor);
        if(AbstractClassDeclMethodsRegularStart!=null) AbstractClassDeclMethodsRegularStart.traverseTopDown(visitor);
        if(MethodDecl!=null) MethodDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(AbstractClassDeclMethods!=null) AbstractClassDeclMethods.traverseBottomUp(visitor);
        if(AbstractClassDeclMethodsRegularStart!=null) AbstractClassDeclMethodsRegularStart.traverseBottomUp(visitor);
        if(MethodDecl!=null) MethodDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractClassDeclMethodsRegular(\n");

        if(AbstractClassDeclMethods!=null)
            buffer.append(AbstractClassDeclMethods.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(AbstractClassDeclMethodsRegularStart!=null)
            buffer.append(AbstractClassDeclMethodsRegularStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodDecl!=null)
            buffer.append(MethodDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractClassDeclMethodsRegular]");
        return buffer.toString();
    }
}
