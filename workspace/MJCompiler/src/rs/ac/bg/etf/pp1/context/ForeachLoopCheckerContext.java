package rs.ac.bg.etf.pp1.context;

import java.util.LinkedList;

import rs.ac.bg.etf.pp1.ast.ForStatement;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class ForeachLoopCheckerContext {
	
	/**
	 * Variables used for code generation.
	 */
	public Obj ident;
	public LinkedList<Integer> refactorIterationExpression;
	public LinkedList<Integer> refactorEndForLoop;
	public int addrIterationExpression;
	public int addrCheckCondition;
	public int addrEnd;
	public int addrBodyOfLoop;
	public ForStatement fs;
	public int forDepth;
	public boolean allowCodeGeneration;
	public int ifDepth;
	public int foreachDepth;
	public int methodDepth;
	
	public ForeachLoopCheckerContext( boolean generate )
	{
		this();
		if ( generate )
		{
			refactorIterationExpression = new LinkedList<Integer>();
			refactorEndForLoop = new LinkedList<Integer>();
			methodDepth = 0;
		};
	}
	
	public ForeachLoopCheckerContext()
	{
		ident = Tab.noObj;
		ifDepth = 0;
		foreachDepth = 0;
	}
	
}
