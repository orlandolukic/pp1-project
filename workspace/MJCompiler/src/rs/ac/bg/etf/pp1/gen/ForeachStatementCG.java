package rs.ac.bg.etf.pp1.gen;

import java.util.Iterator;
import java.util.LinkedList;

import rs.ac.bg.etf.pp1.CodeGenerator;
import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.context.ForeachLoopCheckerContext;
import rs.ac.bg.etf.pp1.des.DesignatorFinder;
import rs.ac.bg.etf.pp1.util.Stack;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;

public class ForeachStatementCG extends VisitorAdaptor {
	
	private CodeGenerator cg;
	private Stack<ForeachLoopCheckerContext> stack;
	
	public ForeachStatementCG(CodeGenerator cg)
	{
		stack = new Stack<ForeachLoopCheckerContext>();
		this.cg = cg;
	}
	
	public ForeachLoopCheckerContext top()
	{
		return stack.top();
	}
	
	/**
	 * Backpatch all adresses.
	 */
	private void backpatch(LinkedList<Integer> reference)
	{
		int i;
		Iterator<Integer> it = reference.iterator();
		while( it.hasNext() )
		{
			i = it.next().intValue();
			Code.fixup(i);
		};
	}
	
	public void generate(ForeachStatement fs)
	{
		// Create new value on stack.
		stack.push( new ForeachLoopCheckerContext(true) );
		
		DesignatorFinder f = new DesignatorFinder(null);
		Designator d = fs.getForeachParensContent().getDesignator();
		d.traverseBottomUp(f);
		
		Obj var = Tab.find( fs.getForeachParensContent().getI1() );
		Obj arr = f.getObject();
		f.setCodeGeneration(true);
		f.setLoadOperation(true);
		
		// i = 0;
		Code.load( Functions.constantInt(0) );				// const_0			0
		int iterationCheck = Code.pc;						// # return here on new iteration
		Code.put( Code.dup );								// dup				0 0
		d.traverseBottomUp(f);								// push arr
		int lenAddr = Tab.find("len").getAdr() - Code.pc;
		Code.put( Code.call );
		Code.put2( lenAddr );								// arraylength		0 0 N
		
		Code.put( Code.jcc + Code.lt );
		Code.put2(0);										// jlt				0
		int fixToArrElementLoad = Code.pc - 2; 
		Code.putJump(0);									// jmp				0
		int fixToEnd = Code.pc - 2;
		
		// Load array element here.
		Code.fixup(fixToArrElementLoad);
		
		Code.put( Code.dup );								// dup				0 0				
		f.resetDesignatorFinder(true);
		f.setLoadOperation(true);
		d.traverseBottomUp(f);								// load arr			0 0 arr
		Code.put( Code.dup_x1 );							// dup_x1			0 arr 0 arr
		Code.put( Code.pop );								// pop				0 arr 0
		Code.load( Functions.constantElem(
				arr.getType().getElemType()) 
		);													// (b|a)load		0 arr[0]
		Functions.storeVariable(var, false);				// store_x			0
		
		// Generate all statements within foreach loop.
		fs.getStatementListOptions().traverseBottomUp(this);
		
		// i++;												//					0
		Code.put( Code.dup );								// dup				0 0
		Code.load( Functions.constantInt(1) );				// const_1			0 0 1
		Code.put( Code.add );								// add				0 1
		Code.put( Code.dup_x1 );							// dup_x1			1 0 1
		Code.put( Code.pop );								// pop				1 0
		Code.put( Code.pop );								// pop				1
		Code.putJump(iterationCheck);						// jmp				1		
		
		// Jump here when finished.
		Code.fixup(fixToEnd);								//					N
		Code.put( Code.pop );								// pop				-empty-
	}
	
	public void visit(EqualExpr ee)
	{
		if ( shouldntGenerateCode() )
			return;
		
		ee.traverseBottomUp(cg);
	}
	
	public void visit(PlusPlusExpr ppe)
	{
		if ( shouldntGenerateCode() )
			return;
		
		ppe.traverseBottomUp(cg);
	}
	
	public void visit(MinusMinusExpr mme)
	{
		if ( shouldntGenerateCode() )
			return;
		
		mme.traverseBottomUp(cg);
	}
	
	public void visit(FuncCall fc)
	{
		top().methodDepth--;
		if ( shouldntGenerateCode() )
			return;
		
		fc.traverseBottomUp(cg);
	}
	
	public void visit(ReadStatement rs)
	{
		if ( shouldntGenerateCode() )
			return;
		
		rs.traverseBottomUp(cg);
	}
	
	public void visit(PrintStatement ps)
	{
		if ( shouldntGenerateCode() )
			return;
		
		ps.traverseBottomUp(cg);
	}
	
	public void visit(FuncCallStart s)
	{
		top().methodDepth++;
	}
	
	public void visit(FactorParensStart st)
	{
		top().methodDepth++;
	}
	
	public void visit(FactorParensEnd st)
	{
		top().methodDepth--;
	}
	
	/**
	 * break; (*)
	 */
	public void visit(BreakStatement bs)
	{
		if ( shouldntGenerateCode() )
			return;
		
		Code.putJump( 0 );
		top().refactorEndForLoop.add( new Integer(Code.pc-2) );
	}
	
	/**
	 * continue; (*)
	 */
	public void visit(ContinueStatement cs)
	{
		if ( shouldntGenerateCode() )
			return;
		
		Code.putJump( 0 );
		top().refactorIterationExpression.add( new Integer(Code.pc-2) );
	}
	
	/**
	 * return <Expr>; (*)
	 */
	public void visit(ReturnExpr re)
	{
		if ( shouldntGenerateCode() )
			return;
		
		re.traverseBottomUp(cg);
	}
	
	public void visit(ForStatementStart s)
	{
		top().forDepth++;
		top().allowCodeGeneration = false;
	}
	
	public void visit(ForStatement fs)
	{	
		top().forDepth--;
		if ( shouldntGenerateCode() )
			return;
		
		if ( top().forDepth == 0 )
		{
			ForStatementCG f = new ForStatementCG(cg);
			fs.traverseBottomUp(f);
		};
	}
	
	public void visit(ForeachStatementStart start)
	{
		top().foreachDepth++;
	}
	
	public void visit(ForeachStatement fs)
	{
		top().foreachDepth--;
		if ( shouldntGenerateCode() )
			return;
		
		generate(fs);
		stack.pop();
	}
	
	private boolean shouldntGenerateCode()
	{
		return top().ifDepth > 0 || top().forDepth > 0 || top().foreachDepth > 0 || top().methodDepth > 0;
	}
	
	/**
	 * ===============================================================================================================
	 * 			*** VISIT METHODS ***
	 * 		--------------------------
	 * 			  IF STATEMENTS
	 * ===============================================================================================================
	 */
	
	public void visit(IfStart i)
	{
		top().ifDepth++;
		top().allowCodeGeneration = false;
	}
	
	public void visit(MatchedIfElse i)
	{
		top().ifDepth--;
		
		if ( shouldntGenerateCode() )
			return;
		
		if ( top().ifDepth == 0 && top().forDepth == 0 && top().foreachDepth == 0 )
		{
			IfStatementCG j = new IfStatementCG(cg);
			j.top().foreachLoopContext = top();
			j.generateAll(i);
			top().allowCodeGeneration = true;
		};
	}
	
	public void visit(UnmatchedIf i)
	{
		top().ifDepth--;
		
		if ( shouldntGenerateCode() )
			return;
		
		if ( top().ifDepth == 0 && top().forDepth == 0 && top().foreachDepth == 0 )
		{
			IfStatementCG j = new IfStatementCG(cg);
			j.top().foreachLoopContext = top();
			j.generateAll(i);
			top().allowCodeGeneration = true;
		};
	}
	
	public void visit(UnmatchedIfElse i)
	{
		top().ifDepth--;
		
		if ( shouldntGenerateCode() )
			return;
		
		if ( top().ifDepth == 0 && top().forDepth == 0 && top().foreachDepth == 0 )
		{
			IfStatementCG j = new IfStatementCG(cg);
			j.top().foreachLoopContext = top();
			j.generateAll(i);
			top().allowCodeGeneration = true;
		};
	}
}
