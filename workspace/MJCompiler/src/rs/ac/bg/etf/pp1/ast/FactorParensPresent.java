// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class FactorParensPresent extends FactorParens {

    private FactorParensStart FactorParensStart;
    private ActParsWrapper ActParsWrapper;
    private FactorParensEnd FactorParensEnd;

    public FactorParensPresent (FactorParensStart FactorParensStart, ActParsWrapper ActParsWrapper, FactorParensEnd FactorParensEnd) {
        this.FactorParensStart=FactorParensStart;
        if(FactorParensStart!=null) FactorParensStart.setParent(this);
        this.ActParsWrapper=ActParsWrapper;
        if(ActParsWrapper!=null) ActParsWrapper.setParent(this);
        this.FactorParensEnd=FactorParensEnd;
        if(FactorParensEnd!=null) FactorParensEnd.setParent(this);
    }

    public FactorParensStart getFactorParensStart() {
        return FactorParensStart;
    }

    public void setFactorParensStart(FactorParensStart FactorParensStart) {
        this.FactorParensStart=FactorParensStart;
    }

    public ActParsWrapper getActParsWrapper() {
        return ActParsWrapper;
    }

    public void setActParsWrapper(ActParsWrapper ActParsWrapper) {
        this.ActParsWrapper=ActParsWrapper;
    }

    public FactorParensEnd getFactorParensEnd() {
        return FactorParensEnd;
    }

    public void setFactorParensEnd(FactorParensEnd FactorParensEnd) {
        this.FactorParensEnd=FactorParensEnd;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(FactorParensStart!=null) FactorParensStart.accept(visitor);
        if(ActParsWrapper!=null) ActParsWrapper.accept(visitor);
        if(FactorParensEnd!=null) FactorParensEnd.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(FactorParensStart!=null) FactorParensStart.traverseTopDown(visitor);
        if(ActParsWrapper!=null) ActParsWrapper.traverseTopDown(visitor);
        if(FactorParensEnd!=null) FactorParensEnd.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(FactorParensStart!=null) FactorParensStart.traverseBottomUp(visitor);
        if(ActParsWrapper!=null) ActParsWrapper.traverseBottomUp(visitor);
        if(FactorParensEnd!=null) FactorParensEnd.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("FactorParensPresent(\n");

        if(FactorParensStart!=null)
            buffer.append(FactorParensStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ActParsWrapper!=null)
            buffer.append(ActParsWrapper.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(FactorParensEnd!=null)
            buffer.append(FactorParensEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [FactorParensPresent]");
        return buffer.toString();
    }
}
