// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class ConstDeclMoreAll extends ConstDeclMore {

    private ConstDeclMore ConstDeclMore;
    private ConstDeclVarOnly ConstDeclVarOnly;

    public ConstDeclMoreAll (ConstDeclMore ConstDeclMore, ConstDeclVarOnly ConstDeclVarOnly) {
        this.ConstDeclMore=ConstDeclMore;
        if(ConstDeclMore!=null) ConstDeclMore.setParent(this);
        this.ConstDeclVarOnly=ConstDeclVarOnly;
        if(ConstDeclVarOnly!=null) ConstDeclVarOnly.setParent(this);
    }

    public ConstDeclMore getConstDeclMore() {
        return ConstDeclMore;
    }

    public void setConstDeclMore(ConstDeclMore ConstDeclMore) {
        this.ConstDeclMore=ConstDeclMore;
    }

    public ConstDeclVarOnly getConstDeclVarOnly() {
        return ConstDeclVarOnly;
    }

    public void setConstDeclVarOnly(ConstDeclVarOnly ConstDeclVarOnly) {
        this.ConstDeclVarOnly=ConstDeclVarOnly;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ConstDeclMore!=null) ConstDeclMore.accept(visitor);
        if(ConstDeclVarOnly!=null) ConstDeclVarOnly.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ConstDeclMore!=null) ConstDeclMore.traverseTopDown(visitor);
        if(ConstDeclVarOnly!=null) ConstDeclVarOnly.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ConstDeclMore!=null) ConstDeclMore.traverseBottomUp(visitor);
        if(ConstDeclVarOnly!=null) ConstDeclVarOnly.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ConstDeclMoreAll(\n");

        if(ConstDeclMore!=null)
            buffer.append(ConstDeclMore.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConstDeclVarOnly!=null)
            buffer.append(ConstDeclVarOnly.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ConstDeclMoreAll]");
        return buffer.toString();
    }
}
