// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class ClassDeclMethod implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private ClassDeclMethodStart ClassDeclMethodStart;
    private MethodDecl MethodDecl;

    public ClassDeclMethod (ClassDeclMethodStart ClassDeclMethodStart, MethodDecl MethodDecl) {
        this.ClassDeclMethodStart=ClassDeclMethodStart;
        if(ClassDeclMethodStart!=null) ClassDeclMethodStart.setParent(this);
        this.MethodDecl=MethodDecl;
        if(MethodDecl!=null) MethodDecl.setParent(this);
    }

    public ClassDeclMethodStart getClassDeclMethodStart() {
        return ClassDeclMethodStart;
    }

    public void setClassDeclMethodStart(ClassDeclMethodStart ClassDeclMethodStart) {
        this.ClassDeclMethodStart=ClassDeclMethodStart;
    }

    public MethodDecl getMethodDecl() {
        return MethodDecl;
    }

    public void setMethodDecl(MethodDecl MethodDecl) {
        this.MethodDecl=MethodDecl;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ClassDeclMethodStart!=null) ClassDeclMethodStart.accept(visitor);
        if(MethodDecl!=null) MethodDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ClassDeclMethodStart!=null) ClassDeclMethodStart.traverseTopDown(visitor);
        if(MethodDecl!=null) MethodDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ClassDeclMethodStart!=null) ClassDeclMethodStart.traverseBottomUp(visitor);
        if(MethodDecl!=null) MethodDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ClassDeclMethod(\n");

        if(ClassDeclMethodStart!=null)
            buffer.append(ClassDeclMethodStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodDecl!=null)
            buffer.append(MethodDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ClassDeclMethod]");
        return buffer.toString();
    }
}
