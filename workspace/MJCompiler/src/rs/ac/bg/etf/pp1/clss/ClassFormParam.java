package rs.ac.bg.etf.pp1.clss;

import rs.ac.bg.etf.pp1.Functions;
import rs.etf.pp1.symboltable.concepts.*;

public class ClassFormParam {
	
	public static ClassFormParam makeClassFormParamReference( String referenceName, String paramName, boolean isArray )
	{
		Struct type = ClassDeclarationList.getInstance().getClassType( referenceName );
		if ( isArray )
			type = new Struct( Struct.Array, type );
		return new ClassFormParam(paramName, new Obj( Obj.Var, referenceName, type ) );
	}
	
	public static ClassFormParam makeClassFormParamAsField( String referenceName, String paramName, boolean isArray )
	{
		Struct type = ClassDeclarationList.getInstance().getClassType( referenceName );		
		if ( isArray )
			type = new Struct( Struct.Array, type );
		return new ClassFormParam(paramName, new Obj( Obj.Var, referenceName, type ) );
	}
	
	public static ClassFormParam makeClassFormParam( String paramName, Struct type, boolean isArray )
	{
		if ( isArray )
			type = new Struct( Struct.Array, type );
		return new ClassFormParam(paramName, new Obj( Obj.Var, null, type ) );
	}
	
	String name;
	Obj o;
	
	public ClassFormParam( String name, Obj o )
	{
		this.name = name;
		this.o = o;
	}
	
	public String getName()
	{
		return name;
	}
	
	public Struct getType()
	{
		return o.getType();
	}
	
	public Obj getObj()
	{
		return o;
	}
	
	public boolean isArray()
	{
		return o.getType().getKind() == Struct.Array;
	}
	
	public boolean isArrayOfNaturalType()
	{
		if ( !isArray() )
			return false;
		
		int t = o.getType().getElemType().getKind();
		return t == Struct.Int || t == Struct.Char || t == Struct.Bool;
	}
	
	public boolean isArrayOfReferenceType()
	{
		if ( !isArray() )
			return false;
		
		int t = o.getType().getElemType().getKind();
		return t == Struct.Class;
	}
	
	public boolean isClassReference()
	{
		return o.getName() != null;
	}
	
	public String getFormalParameterClassName()
	{
		return o.getName();
	}
	
	@Override
	public boolean equals(Object obj) {
		if ( obj instanceof ClassFormParam )
		{
			ClassFormParam p = (ClassFormParam) obj;
			return equalsType(p);
		};
		return false;
	}
	
	public boolean equalsNameType( ClassFormParam p )
	{
		if ( p.isClassReference() && this.isClassReference() )
		{
			boolean b = p.getFormalParameterClassName().equals( this.getFormalParameterClassName() );
			return b && ( p.isArray() && this.isArray() || !p.isArray() && !this.isArray() );
		};
		
		return p.getType().equals( this.getType() ) && this.name.equals( p.name );
	}
	
	public boolean equalsType( ClassFormParam p )
	{
		if ( p.isClassReference() && this.isClassReference() )
		{
			boolean b = p.getFormalParameterClassName().equals( this.getFormalParameterClassName() );
			return b && ( p.isArray() && this.isArray() || !p.isArray() && !this.isArray() );
		};
		
		return p.getType().equals( this.getType() );
	}
	
	public boolean equalsReturnValue( ClassFormParam p )
	{
		if ( p.isClassReference() && isClassReference() )
		{
			return equalsType(p) && ClassDeclarationList.getInstance().isDerived(this.getFormalParameterClassName(), p.getFormalParameterClassName());
		} else
			return equalsType(p);
	}
	
	public boolean equalsClassField( ClassFormParam field )
	{
		return field.getName().equals( this.getName() );
	}
	
	@Override
	public String toString() {
		String str = null;
		
		if ( isClassReference() )
		{
			str = getFormalParameterClassName();
			if ( isArray() )
				str += "[]";
		} else
			str = Functions.getVariableTypeAsString( getType() );
		
		return str;
	}
}
