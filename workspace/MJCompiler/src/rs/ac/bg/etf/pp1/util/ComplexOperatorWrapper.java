package rs.ac.bg.etf.pp1.util;

import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.des.DesignatorFinder;
import rs.etf.pp1.symboltable.concepts.*;

public class ComplexOperatorWrapper extends VisitorAdaptor {
	
	private class StackDepth
	{
		int depth;
		boolean checkCondition;
		boolean errorOccured;
		
		private StackDepth()
		{
			depth = 1;
		}
	}
	
	private boolean generateCode;
	private boolean checkCondition;
	private boolean errorOccured;
	private boolean justStarted;
	private String message;
	private int methodInvocation;
	private Stack<StackDepth> stackDepth;
	private Stack<ComplexOperatorWrapperContext> stack;
	
	public ComplexOperatorWrapper(boolean generateCode)
	{
		this.stack = new Stack<ComplexOperatorWrapperContext>();
		this.stackDepth = new Stack<StackDepth>();
		this.checkCondition = true;
		this.generateCode = generateCode;
		this.justStarted = true;
		this.methodInvocation = 0;
		
		justStarted();
	}	
	
	private ComplexOperatorWrapperContext current()
	{
		return this.stack.top();
	}
	
	private void reportError(String error)
	{
		this.errorOccured = true;
		this.message = error;
	}
	
	public String getMessage()
	{
		return message;
	}
	
	public boolean errorOccured()
	{
		return errorOccured;
	}
	
	public Stack<ComplexOperatorWrapperContext> getStack()
	{
		return stack;
	}
	
	private void push()
	{
		stack.push( new ComplexOperatorWrapperContext() );
	}
	
	private void justStarted()
	{
		if ( justStarted )
		{
			push();
			justStarted = false;
		};
	}
	
	/**
	 * Example:
	 * 
	 * bodovi *= bodovi *= (bodovi *= 4);
	 */
	
	public void visit(NegativeExprMinus ne)
	{
		current().negative = true;
	}
	
	public void visit(FactorDesignator f)
	{				
		Designator d = ((FactorDesignator) f).getDesignator();
		DesignatorFinder df = new DesignatorFinder(null);
		df.traverse(d);
		
		Obj var = df.getObject();
		
		ComplexOperatorWrapperContext context = current();
		context.isVariable &= var.getKind() == Obj.Var || var.getKind() == Obj.Fld;
		context.variable = var;
		//context.arrayOffset = doe instanceof DesignatorOpt ? (ExprNoError) ((DesignatorOpt)doe).getExpr() : null;
	}
	
	public void visit(FactorParensStart fps)
	{
		this.methodInvocation++;
	}
	
	public void visit(FactorParensEnd fpe)
	{
		this.methodInvocation--;
	}
	
	public void visit(FactorConstDeclType f)
	{		
		ComplexOperatorWrapperContext context = current();
		int val = Functions.getValueFromConstant(f);
		context.variable = Functions.constantInt( context.negative ? -val : val );
		
		if ( this.checkCondition )
			context.isVariable &= false;
	}
	
	/**
	 * Operators: *=,/=,%=
	 */
	public void visit(MulopRightOperand mr)
	{
		if ( methodInvocation > 0 )
			return;
		
		ComplexOperatorWrapperContext context = stack.top();
		if ( !context.isVariable )
			reportError("The left-hand side of assignment must be a variable");
		
		this.checkCondition = true;
		push();
		current().isComplex = true;
		current().isMulop = true;
		current().mulop = (Mulop) mr;
		if ( stackDepth.size() > 0 )
			this.stackDepth.top().depth++;
	}
	
	/**
	 * Operators: *,/,%
	 */
	public void visit(MulopLeftOperand ml)
	{
		if ( methodInvocation > 0 )
			return;
		
		push();
		current().mulop = (Mulop) ml;
		current().isMulop = true;
		if ( stackDepth.size() > 0 )
			this.stackDepth.top().depth++;
	}
	
	/**
	 * Operators: +=,-=
	 */
	public void visit(AddopRightOperand mr)
	{
		if ( methodInvocation > 0 )
			return;
		
		ComplexOperatorWrapperContext context = stack.top();
		if ( !context.isVariable )
			reportError("The left-hand side of assignment must be a variable");
		
		this.checkCondition = true;
		push();
		current().isComplex = true;
		current().isAddop = true;
		current().addop = (Addop) mr;
		if ( stackDepth.size() > 0 )
			this.stackDepth.top().depth++;
	}
	
	/**
	 * Operators: +,-
	 */
	public void visit(AddopLeftOperand ml)
	{
		if ( methodInvocation > 0 )
			return;
		
		push();
		current().addop = (Addop) ml;
		current().isAddop = true;
		if ( stackDepth.size() > 0 )
			this.stackDepth.top().depth++;
	}
	
	public void visit(DesignatorOptStart dos)
	{
		this.checkCondition = false;
		this.justStarted = true;
		push();
		this.stackDepth.push(new StackDepth());
	}
	
	public void visit(DesignatorOptEnd dos)
	{
		int i = this.stackDepth.top().depth;
		while( i > 0 )
		{
			stack.pop(); i--;
		};	
		
		this.stackDepth.pop();
		this.checkCondition = this.stackDepth.size() > 0 ? this.stackDepth.top().checkCondition : true;
		this.errorOccured = this.stackDepth.size() > 0 ? this.stackDepth.top().errorOccured : false;
	}
	
	/**
	 * Whole expression.
	 */
	public void finalCheck()
	{
		if ( !generateCode )
		{			
			ComplexOperatorWrapperContext context;
			boolean isStronglyConnected;
			isStronglyConnected = false;
	
			while( stack.size() > 0 )
			{
				context = stack.pop();
				
				if ( isStronglyConnected && context.negative )
				{
					this.reportError("Cannot use '-' when expression is connected with complex operator");
					return;
				};
				
				isStronglyConnected |= context.isComplex;
				
				if ( (isStronglyConnected && !context.isComplex && stack.size() >= 1) && this.methodInvocation==0 )
				{					
					this.reportError("Expected non modifiable expression on the end of expression");
					return;					
				};
			};
			
			return;
		};
	}
}
