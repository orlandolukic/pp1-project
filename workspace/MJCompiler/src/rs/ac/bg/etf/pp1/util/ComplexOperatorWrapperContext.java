package rs.ac.bg.etf.pp1.util;

import rs.ac.bg.etf.pp1.ast.ExprNoError;
import rs.ac.bg.etf.pp1.ast.*;
import rs.etf.pp1.symboltable.concepts.Obj;

public class ComplexOperatorWrapperContext {
	
	// Operator ?
	public boolean isMulop;
	public Mulop mulop;
	public boolean isAddop;
	public Addop addop;
	
	public boolean isVariable;
	public Obj variable;
	public ExprNoError arrayOffset;
	public boolean negative;
	public Stack<ComplexOperatorWrapperContext> arrayContext;
		
	public boolean isComplex;
	
	public ComplexOperatorWrapperContext()
	{
		this.isVariable = true;
		this.isComplex = false;
		this.variable = null;
		this.arrayOffset = null;
		this.negative = false;
		
		this.isMulop = false;
		this.mulop = null;
		this.isAddop = false;
		this.addop = null;
	}
}
