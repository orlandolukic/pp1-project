package rs.ac.bg.etf.pp1.des;

import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.ast.Assignop;
import rs.ac.bg.etf.pp1.ast.AssignopEqual;
import rs.ac.bg.etf.pp1.ast.DesignatorArrayAccess;
import rs.ac.bg.etf.pp1.ast.DesignatorObjectAccess;
import rs.ac.bg.etf.pp1.ast.ExprNoError;
import rs.ac.bg.etf.pp1.gen.ComplexOperator;
import rs.ac.bg.etf.pp1.gen.TD_Expr;
import rs.ac.bg.etf.pp1.util.CGExprContext;
import rs.ac.bg.etf.pp1.util.DesignatorFinderCGContext;
import rs.ac.bg.etf.pp1.util.HasComplexOperators;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class DesignatorEqualExpr extends DesignatorFinder {

	private TD_Expr vis;
	private ComplexOperator coGenerator;
	private HasComplexOperators hco;
	private Assignop aop;
	private ExprNoError expRight;
	private int genType;
	
	public DesignatorEqualExpr(Assignop aop, ExprNoError expRight) {
		super(null);
		this.expRight = expRight;
		this.aop = aop;
		this.genType = 0;
		
		coGenerator = new ComplexOperator();
		vis = new TD_Expr();	
		hco = new HasComplexOperators();
		hco.performCheck(expRight);
	}

	
	@Override
	protected void designatorFinished() {
		
		hco = new HasComplexOperators();
		hco.performCheck(expRight);
		
		if ( !hco.cond() && aop instanceof AssignopEqual )
		{
			// Get variable.
			generateThroughTheList(false);
			
			// Handle right side of the expression.
			vis.createNewContext(CGExprContext.SIDE_RIGHT);
			vis.visit(expRight);
			
			// Store variable.
			storeVariable();
			
		} else	// Complex operators on right side || complex operator between sides.
		{
			boolean complexOperator = !(aop instanceof AssignopEqual);
			/**
			 * Operators +=, -=, *=, /=, %=
			 */
			
			/*
			if ( isArrayAccess )
			{
				Code.put( Code.dup_x2 );
				storeVariable();
				Code.put( Code.pop );
				return;
			};
			*/
			
			// Get variable.
			//genType = 1;			
			setIsComplexOperator( true );
			generateVariable();
			//genType = 0;
						
			// Generate right side.
			hco.performCheck(expRight);
			if ( !hco.cond() )
			{					
				vis.createNewContext(CGExprContext.SIDE_RIGHT);
				vis.visit(expRight);
			} else
			{
				// Generate code for right expression when complex expression.
				coGenerator.generate(expRight);
			};
			
			/**
			 * Operators +=, -=, *=, /=, %=
			 */
			if ( complexOperator )
			{					
				// Generate +,-,*,/,%
				Functions.generateOperator( aop );
				
				// For primitive variable.
				if ( isPrimitiveVariable() )
				{				
					Code.put( Code.dup );
					
					storeVariable();
					
					Code.put( Code.pop );
				} else
				{
					if ( isArrayAccess )
					{
						Code.put( Code.dup_x2 );
						storeVariable();
						Code.put(Code.pop);
					} else
					{
						// Generate address.
						generateThroughTheList(false);
						
						// Switch sides for address and operand.
						Code.put( Code.dup_x1 );
						
						// Delete previously generated address.
						Code.put( Code.pop );
						
						// Store variable.
						storeVariable();
					};
				};
			} else
			{
				// Store variable.
				storeVariable(true);
			};
		};
	}

}
