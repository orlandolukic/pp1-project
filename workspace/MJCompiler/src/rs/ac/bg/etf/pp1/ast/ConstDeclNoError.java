// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class ConstDeclNoError extends ConstDecl {

    private IsConstDecl IsConstDecl;
    private ConstDeclMore ConstDeclMore;

    public ConstDeclNoError (IsConstDecl IsConstDecl, ConstDeclMore ConstDeclMore) {
        this.IsConstDecl=IsConstDecl;
        if(IsConstDecl!=null) IsConstDecl.setParent(this);
        this.ConstDeclMore=ConstDeclMore;
        if(ConstDeclMore!=null) ConstDeclMore.setParent(this);
    }

    public IsConstDecl getIsConstDecl() {
        return IsConstDecl;
    }

    public void setIsConstDecl(IsConstDecl IsConstDecl) {
        this.IsConstDecl=IsConstDecl;
    }

    public ConstDeclMore getConstDeclMore() {
        return ConstDeclMore;
    }

    public void setConstDeclMore(ConstDeclMore ConstDeclMore) {
        this.ConstDeclMore=ConstDeclMore;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(IsConstDecl!=null) IsConstDecl.accept(visitor);
        if(ConstDeclMore!=null) ConstDeclMore.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(IsConstDecl!=null) IsConstDecl.traverseTopDown(visitor);
        if(ConstDeclMore!=null) ConstDeclMore.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(IsConstDecl!=null) IsConstDecl.traverseBottomUp(visitor);
        if(ConstDeclMore!=null) ConstDeclMore.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ConstDeclNoError(\n");

        if(IsConstDecl!=null)
            buffer.append(IsConstDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ConstDeclMore!=null)
            buffer.append(ConstDeclMore.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ConstDeclNoError]");
        return buffer.toString();
    }
}
