package rs.ac.bg.etf.pp1.util;

import java.util.LinkedList;

import rs.ac.bg.etf.pp1.ast.*;

public class GetStartNode extends VisitorAdaptor {
	
	private class GSNContext
	{
		private int complexOp;
		private int normalOp;
		private boolean found;
		private SyntaxNode factor;
	}
	
	private Factor temp;
	private boolean found;
	private GSNContext foundContext;
	private boolean ignore;
	private int nestedMethods;
	private Stack<GSNContext> stack;
	
	public GetStartNode()
	{
		found = false;
		this.ignore = false;
		this.nestedMethods = 0;
		stack = new Stack<GSNContext>();
		stack.push(new GSNContext());
		this.foundContext = null;
	}
	
	public SyntaxNode getFactor()
	{
		GSNContext context = found ? foundContext : stack.top();
		if ( !found )
		{			
			if ( context.normalOp == 0 && context.complexOp >= 0 )
			{
				context.factor = temp;
				if ( context.factor.getParent().getParent() instanceof ExprNoError )
					context.factor = temp.getParent().getParent();
			};
		} else 
		{
			if ( context.normalOp == 0 && context.complexOp == 0 )
				context.factor = context.factor.getParent().getParent();
		};
		return context.factor;
	}
	
	public void visit(DesignatorOptStart s)
	{
		ignore = true;
	}
	
	public void visit(DesignatorOptEnd s)
	{
		ignore = false;
	}
	
	public void visit(FactorParensStart f)
	{
		nestedMethods++;
		if ( nestedMethods == 1 )
			ignore = true;
	}
	
	public void visit(FactorParensEnd f)
	{
		nestedMethods--;
		if ( nestedMethods == 0 )
			ignore = false;
	}
	
	public void visit(FactorWithExprStart f)
	{
		stack.push(new GSNContext());
	}
	
	public void visit(FactorWithExprEnd e)
	{
		GSNContext context = stack.pop();
		if ( context.found )
		{
			found = true;
			foundContext = context;
		} else if ( context.complexOp == 0 && context.normalOp == 0 )
		{
			context.factor = temp;
			context.found = true;
			foundContext = context;
			found = true;
		} else
		{
			found = context.found;
			if ( found )
				foundContext = context;
		};
	}
	
	public void visit(FactorConstDeclType f)
	{
		if ( ignore )
			return;
		
		temp = f;
	}
	
	public void visit(FactorDesignator fd)
	{
		if ( ignore )
			return;
		
		temp = fd;
	}
	
	public void visit(MulopLeftOperand m)
	{
		GSNContext c = stack.top();
		c.normalOp++;
		if ( !found && c.normalOp == 1 )
		{
			c.factor = temp;
			c.found = true;
			foundContext = c;
			found = true;
		};
	}
	
	public void visit(AddopLeftOperand a)
	{
		GSNContext c = stack.top();
		c.normalOp++;
		if ( !found && c.normalOp == 1 )
		{
			c.factor = temp;
			c.found = true;
			foundContext = c;
			found = true;
		};
	}
	
	public void visit(MulopRightOperand m)
	{
		stack.top().complexOp++;
	}
	
	public void visit(AddopRightOperand a)
	{
		stack.top().complexOp++;
	}
}
