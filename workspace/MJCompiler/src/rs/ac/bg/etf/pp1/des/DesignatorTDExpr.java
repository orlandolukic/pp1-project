package rs.ac.bg.etf.pp1.des;

import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.ast.DesignatorArrayAccess;
import rs.ac.bg.etf.pp1.ast.DesignatorObjectAccess;
import rs.ac.bg.etf.pp1.ast.ExprNoError;
import rs.ac.bg.etf.pp1.gen.ComplexOperator;
import rs.ac.bg.etf.pp1.gen.TD_Expr;
import rs.ac.bg.etf.pp1.util.CGExprContext;
import rs.ac.bg.etf.pp1.util.DesignatorFinderCGContext;
import rs.ac.bg.etf.pp1.util.HasComplexOperators;
import rs.etf.pp1.mj.runtime.Code;

public class DesignatorTDExpr extends DesignatorFinder {

	private TD_Expr td;
	private boolean finished;
	
	public DesignatorTDExpr( TD_Expr expr ) {
		super(null);
		this.td = expr;
		this.finished = false;
	}
	
	public boolean isFinished()
	{
		return finished;
	}
	
	@Override
	protected void designatorStartVariable(DesignatorFinderCGContext c) {
		
		if ( c.isLast && !isPrimitiveVariable() && td.getContext().generateArrayDuplication )
			Code.put( Code.dup );		
		super.designatorStartVariable(c);
	}
	
	@Override
	protected void designatorArrayAccess(DesignatorFinderCGContext r) 
	{
		DesignatorArrayAccess d = (DesignatorArrayAccess) r.node;
		HasComplexOperators hco = new HasComplexOperators();
		ExprNoError ex = (ExprNoError) d.getExpr();
		hco.performCheck( ex );
		boolean isComplex = hco.cond();
		
		if ( !isComplex )
		{					
			// Create new context.
			CGExprContext prev = td.getContext();
			td.createNewContext( CGExprContext.SIDE_EXPR );
			td.visit( (ExprNoError) (d.getExpr()) );		
			td.setContext(prev);
		} else
		{
			ComplexOperator co = new ComplexOperator();
			co.generate( ex );
		};		
		
		if ( r.isLast && !isPrimitiveVariable() && td.getContext().generateArrayDuplication )
			Code.put( Code.dup2 );
		
		Code.load( Functions.constantElem( r.obj.getType() ) );
	}
	
	@Override
	protected void designatorObjectPropertyAccess(DesignatorFinderCGContext d) {
		if ( d.isLast && !isPrimitiveVariable() && td.getContext().generateArrayDuplication )
			Code.put( Code.dup );	
		Code.load( d.obj );
	}
	
	@Override
	protected void designatorFinished() {
		if ( td.getContext().potentiallyVarConst )
		{
			td.getContext().type = designatorContext.current.getType();
			finished = true;
		};
	}

}
