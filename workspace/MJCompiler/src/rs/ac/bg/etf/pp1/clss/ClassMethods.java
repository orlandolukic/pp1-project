package rs.ac.bg.etf.pp1.clss;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class ClassMethods {
	
	private LinkedList<ClassMethod> list;
	
	public ClassMethods()
	{
		list = new LinkedList<ClassMethod>();
	}
	
	/**
	 * Gets iterator for methods.
	 * 
	 * @return iterator
	 */
	public Iterator<ClassMethod> getIterator()
	{
		return list.iterator();
	}
	
	/**
	 * Checks if method exists within the all class methods.
	 * @param obj Symbol Table node.
	 * @return indicator whether method is found
	 */
	public boolean methodExists( String name )
	{
		ClassMethod retval = null;
		Iterator<ClassMethod> it = list.iterator();		
		while( it.hasNext() )
		{
			retval = it.next();
			if ( retval.getObj().getName().equals(name) )
				return true;
		};		
		return false;
	}
	
	/**
	 * Checks if method exists within the all class methods.
	 * @param obj Symbol Table node.
	 * @return indicator whether method is found
	 */
	public boolean methodExists( Obj obj )
	{
		ClassMethod retval = null;
		Iterator<ClassMethod> it = list.iterator();		
		while( it.hasNext() )
		{
			retval = it.next();
			if ( retval.getObj().equals(obj) )
				return true;
		};		
		return false;
	}
	
	/**
	 * Checks if method is defined within the all class methods.
	 * @param obj Symbol Table node.
	 * @return indicator whether method is found
	 */
	public Obj isDefinedMethod( Obj obj )
	{
		ClassMethod retval = null;
		Iterator<ClassMethod> it = list.iterator();		
		while( it.hasNext() )
		{
			retval = it.next();
			if ( retval.getObj().equals(obj) )
				return retval.getObj();
		};		
		
		return null;
	}
	
	/**
	 * Checks if method is defined within the all class methods.
	 * @param obj Symbol Table node.
	 * @return indicator whether method is found
	 */
	public Obj isDefinedMethod( String name )
	{
		ClassMethod retval = null;
		Iterator<ClassMethod> it = list.iterator();		
		while( it.hasNext() )
		{
			retval = it.next();
			if ( retval.getObj().getName().equals(name) )
				return retval.getObj();
		};		
		
		return null;
	}
	
	/**
	 * Gets method of the class by name.
	 * @param name Name of the method.
	 * @return ClassMethod
	 */
	public ClassMethod getMethod( String name )
	{
		ClassMethod retval = null;
		Iterator<ClassMethod> it = list.iterator();		
		while( it.hasNext() )
		{
			retval = it.next();
			if ( retval.getObj().getName().equals(name) )
				return retval;
		};		
		return null;
	}
	
	/**
	 * Number of fields within class.
	 * @return
	 */
	public int getNumberOfMethods()
	{
		return list.size();
	}
	
	/**
	 * Adds method with certain access type.
	 * 
	 * @param obj Symbol Table node
	 * @param accessType Access type.
	 * @param isVirtual Virtual/concrete method
	 * @param int Line on which method is declared
	 */
	public ClassMethod addMethod( ClassDeclaration classDecl, Obj obj, int accessType, boolean isVirtual, ClassFormParam returnValue, int line )
	{		
		// Add field into the list.
		ClassMethod method = new ClassMethod( classDecl, obj, accessType, isVirtual, returnValue, line );
		list.add( method );
		
		return method;
	}
	
	public void addMethod( ClassMethod method )
	{
		list.add( method );
	}
	
	/**
	 * Checks if certain method is accessable with forTypeAccess type.
	 * 
	 * @param name Name of the method
	 * @param forTypeAccess Access type. Could be PRIVATE|PROTECTED|PUBLIC.
	 * @return true if access is granted, false otherwise or when method is not found with all fields.
	 */
	public boolean isAccessableMethod( String name, int forTypeAccess )
	{
		ClassMethod meth = getMethod(name);
		
		if ( meth != null )
			return meth.getAccessType() <= forTypeAccess;
		
		return false;
	}
	
	public int a(int a)
	{
		return 0;
	}
	
	public void a(boolean a)
	{
		return;
	}
	
	/**
	 * Checks if method is already defined.
	 * 
	 * @param type
	 * @param name
	 * @return
	 */
	public boolean isAlreadyDefined( ClassMethod escapeMethod, String name, ClassFormParam[] parameters )
	{	
		Iterator<ClassMethod> itm = list.iterator();
		ClassMethod m = null;
		LinkedList<ClassMethod> okMethods = new LinkedList<ClassMethod>();
		int i = 0;
		boolean found = false;
		while( itm.hasNext() )
		{
			m = itm.next();
			if ( m.getFormalParametersNumber() == parameters.length && name.equals( m.getObj().getName() ) 
					&& ( escapeMethod != null && escapeMethod != m || escapeMethod == null ) 
				)
				okMethods.add(m);
		};
		
		if ( okMethods.size() == 0 )
			return false;
		
		ClassMethod meth;
		int methodArgs;
		Iterator<ClassMethod> it1 = okMethods.iterator();
		while( it1.hasNext() )
		{
			meth = it1.next();
			if ( meth.equals(escapeMethod) && escapeMethod.equalReturnValue(meth) )
				return true;			
		};
		
		return false;
	}

}
