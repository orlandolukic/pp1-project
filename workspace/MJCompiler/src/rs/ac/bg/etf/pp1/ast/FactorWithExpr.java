// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class FactorWithExpr extends Factor {

    private FactorWithExprStart FactorWithExprStart;
    private Expr Expr;
    private FactorWithExprEnd FactorWithExprEnd;

    public FactorWithExpr (FactorWithExprStart FactorWithExprStart, Expr Expr, FactorWithExprEnd FactorWithExprEnd) {
        this.FactorWithExprStart=FactorWithExprStart;
        if(FactorWithExprStart!=null) FactorWithExprStart.setParent(this);
        this.Expr=Expr;
        if(Expr!=null) Expr.setParent(this);
        this.FactorWithExprEnd=FactorWithExprEnd;
        if(FactorWithExprEnd!=null) FactorWithExprEnd.setParent(this);
    }

    public FactorWithExprStart getFactorWithExprStart() {
        return FactorWithExprStart;
    }

    public void setFactorWithExprStart(FactorWithExprStart FactorWithExprStart) {
        this.FactorWithExprStart=FactorWithExprStart;
    }

    public Expr getExpr() {
        return Expr;
    }

    public void setExpr(Expr Expr) {
        this.Expr=Expr;
    }

    public FactorWithExprEnd getFactorWithExprEnd() {
        return FactorWithExprEnd;
    }

    public void setFactorWithExprEnd(FactorWithExprEnd FactorWithExprEnd) {
        this.FactorWithExprEnd=FactorWithExprEnd;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(FactorWithExprStart!=null) FactorWithExprStart.accept(visitor);
        if(Expr!=null) Expr.accept(visitor);
        if(FactorWithExprEnd!=null) FactorWithExprEnd.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(FactorWithExprStart!=null) FactorWithExprStart.traverseTopDown(visitor);
        if(Expr!=null) Expr.traverseTopDown(visitor);
        if(FactorWithExprEnd!=null) FactorWithExprEnd.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(FactorWithExprStart!=null) FactorWithExprStart.traverseBottomUp(visitor);
        if(Expr!=null) Expr.traverseBottomUp(visitor);
        if(FactorWithExprEnd!=null) FactorWithExprEnd.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("FactorWithExpr(\n");

        if(FactorWithExprStart!=null)
            buffer.append(FactorWithExprStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expr!=null)
            buffer.append(Expr.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(FactorWithExprEnd!=null)
            buffer.append(FactorWithExprEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [FactorWithExpr]");
        return buffer.toString();
    }
}
