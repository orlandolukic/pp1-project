package rs.ac.bg.etf.pp1.des;

import rs.ac.bg.etf.pp1.ExprChecker;
import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.clss.ClassDeclarationList;
import rs.ac.bg.etf.pp1.clss.ClassUtil;
import rs.ac.bg.etf.pp1.util.DesignatorFinderCGContext;
import rs.ac.bg.etf.pp1.util.ExprCheckerContext;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class DesignatorTraverse extends DesignatorFinder
{
	private boolean isNull;
	private ExprCheckerContext context;
	public DesignatorTraverse( SemanticAnalyzer sa, ExprChecker checker ) {
		super(sa);
		this.checker = checker;
		this.context = checker.getContext();
		this.isNull = false;
	}

	@Override
	protected void designatorStartVariable( DesignatorFinderCGContext c ) {			
		if ( !sa.methReturnStarted 
				&& c.obj.getName() != null 
				&& c.obj.getName().equals("null") 
				&& ( !context.allowNullReference || context.multiple ) )
		{
			this.isNull = true;
			this.terminate("Cannot use null object reference in expression");	
		};
	}

	@Override
	protected void designatorFinished() {
		
		Obj node = designatorContext.current;
		String ident = node.getName();

		int kind = node.getKind();
		if ( kind  == Obj.Con )
		{
			if ( ident.equals("null") )
				return;
			
			if ( node.getType().equals(Functions.boolType) )
			{
				context.cantPassAddop = true;
				context.cantPassMulop = true;
			};
			
			// Check if constant's type is different than expected one.
			if ( !node.getType().equals(context.expected) && !context.allowNullReference )
				this.terminate( Functions.getUncompatibileTypesMessage(context.expected, node.getType()) + " on constant '" + ident + "'" );
			
			// Return constant.
			context.constReturn = node;
		} else if ( kind == Obj.Meth )
		{
			if ( isFromFactorDesignator() && !hasFactorParensAfter() )
				this.terminate("Cannot use method's name without method invocation");
			else
			{
				// Check return value of function.
				if ( node.getType() == Tab.noType )
				{			
					this.terminate("Method '" + ident + "' returns 'void' but should return type of '" + Functions.getVariableTypeAsString(context.expected) + "'");
				} else if ( !context.expected.equals(node.getType()) || node.getType().isRefType() )
				{
					this.terminate("Method '" + ident + "' does not return expected value for the expression");
				};
			};
		} else if ( kind == Obj.Var )
		{
			Struct s = node.getType();
			if ( !s.isRefType() )
			{						
				if ( context.isLenInvocation )
					this.terminate("Cannot use variable with type '" + Functions.getVariableTypeAsString(node) + "' on method 'int len(<Type>[])'");
				
				if ( context.expected.equals(Tab.intType) && (s.getKind() != Struct.Int) )
				{
					this.terminate( Functions.getUncompatibileTypesMessage(context.expected, s) + " of variable '" + ident + "'" );
				} else if ( context.expected.equals(Tab.charType) && (s.getKind() != Struct.Char) )
				{
					this.terminate( Functions.getUncompatibileTypesMessage(context.expected, s) + " of variable '" + ident + "'" );
				} else if ( context.expected.equals(Functions.boolType) && (s.getKind() != Struct.Bool) )
				{
					this.terminate( Functions.getUncompatibileTypesMessage(context.expected, s) + " of variable '" + ident + "'" );
				};
			} else	// Is reference type
			{
				if ( node.getType().getKind() == Struct.Class )
				{
					if ( context.multiple )						
					{
						this.terminate( "Cannot use reference '" + node.getName() + "' inside expression" );
						return;
					};
					
					if ( context.isExpectedReference )
					{
						String classname = ClassUtil.getClassNameFromObj(node);
						String baseclass = ClassUtil.getClassNameFromStruct(context.expected);					
						if ( !classname.equals(baseclass) && !ClassUtil.isDerived(baseclass, classname) )						
							terminate("Class '" + classname + "' is not extended from class '" + baseclass + "'");						
						return;
					};
					
					if ( !context.expected.compatibleWith( designatorContext.current.getType() ) )
					{
						this.terminate( Functions.getUncompatibileTypesMessage(context.expected, designatorContext.current.getType()) );
						return;
					};					
				};				
				
			};
			
			if ( !designatorContext.current.getType().equals(context.expected) )							
				this.terminate( Functions.getUncompatibileTypesMessage( context.expected, designatorContext.current.getType() ) );
		};		
	}
	
}