// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class ForStatement extends Matched {

    private ForStatementStart ForStatementStart;
    private ForDesignatorStatement ForDesignatorStatement;
    private ForCondition ForCondition;
    private ForDesignatorStatement ForDesignatorStatement1;
    private ForStatementEnd ForStatementEnd;

    public ForStatement (ForStatementStart ForStatementStart, ForDesignatorStatement ForDesignatorStatement, ForCondition ForCondition, ForDesignatorStatement ForDesignatorStatement1, ForStatementEnd ForStatementEnd) {
        this.ForStatementStart=ForStatementStart;
        if(ForStatementStart!=null) ForStatementStart.setParent(this);
        this.ForDesignatorStatement=ForDesignatorStatement;
        if(ForDesignatorStatement!=null) ForDesignatorStatement.setParent(this);
        this.ForCondition=ForCondition;
        if(ForCondition!=null) ForCondition.setParent(this);
        this.ForDesignatorStatement1=ForDesignatorStatement1;
        if(ForDesignatorStatement1!=null) ForDesignatorStatement1.setParent(this);
        this.ForStatementEnd=ForStatementEnd;
        if(ForStatementEnd!=null) ForStatementEnd.setParent(this);
    }

    public ForStatementStart getForStatementStart() {
        return ForStatementStart;
    }

    public void setForStatementStart(ForStatementStart ForStatementStart) {
        this.ForStatementStart=ForStatementStart;
    }

    public ForDesignatorStatement getForDesignatorStatement() {
        return ForDesignatorStatement;
    }

    public void setForDesignatorStatement(ForDesignatorStatement ForDesignatorStatement) {
        this.ForDesignatorStatement=ForDesignatorStatement;
    }

    public ForCondition getForCondition() {
        return ForCondition;
    }

    public void setForCondition(ForCondition ForCondition) {
        this.ForCondition=ForCondition;
    }

    public ForDesignatorStatement getForDesignatorStatement1() {
        return ForDesignatorStatement1;
    }

    public void setForDesignatorStatement1(ForDesignatorStatement ForDesignatorStatement1) {
        this.ForDesignatorStatement1=ForDesignatorStatement1;
    }

    public ForStatementEnd getForStatementEnd() {
        return ForStatementEnd;
    }

    public void setForStatementEnd(ForStatementEnd ForStatementEnd) {
        this.ForStatementEnd=ForStatementEnd;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ForStatementStart!=null) ForStatementStart.accept(visitor);
        if(ForDesignatorStatement!=null) ForDesignatorStatement.accept(visitor);
        if(ForCondition!=null) ForCondition.accept(visitor);
        if(ForDesignatorStatement1!=null) ForDesignatorStatement1.accept(visitor);
        if(ForStatementEnd!=null) ForStatementEnd.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ForStatementStart!=null) ForStatementStart.traverseTopDown(visitor);
        if(ForDesignatorStatement!=null) ForDesignatorStatement.traverseTopDown(visitor);
        if(ForCondition!=null) ForCondition.traverseTopDown(visitor);
        if(ForDesignatorStatement1!=null) ForDesignatorStatement1.traverseTopDown(visitor);
        if(ForStatementEnd!=null) ForStatementEnd.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ForStatementStart!=null) ForStatementStart.traverseBottomUp(visitor);
        if(ForDesignatorStatement!=null) ForDesignatorStatement.traverseBottomUp(visitor);
        if(ForCondition!=null) ForCondition.traverseBottomUp(visitor);
        if(ForDesignatorStatement1!=null) ForDesignatorStatement1.traverseBottomUp(visitor);
        if(ForStatementEnd!=null) ForStatementEnd.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ForStatement(\n");

        if(ForStatementStart!=null)
            buffer.append(ForStatementStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ForDesignatorStatement!=null)
            buffer.append(ForDesignatorStatement.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ForCondition!=null)
            buffer.append(ForCondition.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ForDesignatorStatement1!=null)
            buffer.append(ForDesignatorStatement1.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ForStatementEnd!=null)
            buffer.append(ForStatementEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ForStatement]");
        return buffer.toString();
    }
}
