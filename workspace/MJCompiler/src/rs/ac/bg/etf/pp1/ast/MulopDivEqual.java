// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class MulopDivEqual extends MulopRight {

    private String D1;

    public MulopDivEqual (String D1) {
        this.D1=D1;
    }

    public String getD1() {
        return D1;
    }

    public void setD1(String D1) {
        this.D1=D1;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("MulopDivEqual(\n");

        buffer.append(" "+tab+D1);
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [MulopDivEqual]");
        return buffer.toString();
    }
}
