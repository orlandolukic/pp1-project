package rs.ac.bg.etf.pp1.print;

public abstract class Printer<T> {
	
	protected T snode;
	protected StringBuilder str;
	
	public Printer(T snode)
	{
		this.snode = snode;
		this.str = new StringBuilder();
	}
	
	@Override
	public String toString()
	{
		print();
		return str.toString();
	}
	
	/**
	 * Prints content of syntax node.
	 * 
	 * @param snode Syntax node.
	 */
	public abstract void print();
}
