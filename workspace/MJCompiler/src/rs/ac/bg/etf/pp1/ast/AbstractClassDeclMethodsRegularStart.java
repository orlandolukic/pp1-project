// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class AbstractClassDeclMethodsRegularStart implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private AccessRights AccessRights;

    public AbstractClassDeclMethodsRegularStart (AccessRights AccessRights) {
        this.AccessRights=AccessRights;
        if(AccessRights!=null) AccessRights.setParent(this);
    }

    public AccessRights getAccessRights() {
        return AccessRights;
    }

    public void setAccessRights(AccessRights AccessRights) {
        this.AccessRights=AccessRights;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(AccessRights!=null) AccessRights.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(AccessRights!=null) AccessRights.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(AccessRights!=null) AccessRights.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractClassDeclMethodsRegularStart(\n");

        if(AccessRights!=null)
            buffer.append(AccessRights.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractClassDeclMethodsRegularStart]");
        return buffer.toString();
    }
}
