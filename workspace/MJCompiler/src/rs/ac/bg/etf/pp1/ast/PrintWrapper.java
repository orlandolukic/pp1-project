// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class PrintWrapper implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private PrintWrapperAll PrintWrapperAll;

    public PrintWrapper (PrintWrapperAll PrintWrapperAll) {
        this.PrintWrapperAll=PrintWrapperAll;
        if(PrintWrapperAll!=null) PrintWrapperAll.setParent(this);
    }

    public PrintWrapperAll getPrintWrapperAll() {
        return PrintWrapperAll;
    }

    public void setPrintWrapperAll(PrintWrapperAll PrintWrapperAll) {
        this.PrintWrapperAll=PrintWrapperAll;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(PrintWrapperAll!=null) PrintWrapperAll.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(PrintWrapperAll!=null) PrintWrapperAll.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(PrintWrapperAll!=null) PrintWrapperAll.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("PrintWrapper(\n");

        if(PrintWrapperAll!=null)
            buffer.append(PrintWrapperAll.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [PrintWrapper]");
        return buffer.toString();
    }
}
