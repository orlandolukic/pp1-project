// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class AbstractClassDeclMethodsAbstract extends AbstractClassDeclMethods {

    private AbstractClassDeclMethods AbstractClassDeclMethods;
    private AbstractMethodDecl AbstractMethodDecl;

    public AbstractClassDeclMethodsAbstract (AbstractClassDeclMethods AbstractClassDeclMethods, AbstractMethodDecl AbstractMethodDecl) {
        this.AbstractClassDeclMethods=AbstractClassDeclMethods;
        if(AbstractClassDeclMethods!=null) AbstractClassDeclMethods.setParent(this);
        this.AbstractMethodDecl=AbstractMethodDecl;
        if(AbstractMethodDecl!=null) AbstractMethodDecl.setParent(this);
    }

    public AbstractClassDeclMethods getAbstractClassDeclMethods() {
        return AbstractClassDeclMethods;
    }

    public void setAbstractClassDeclMethods(AbstractClassDeclMethods AbstractClassDeclMethods) {
        this.AbstractClassDeclMethods=AbstractClassDeclMethods;
    }

    public AbstractMethodDecl getAbstractMethodDecl() {
        return AbstractMethodDecl;
    }

    public void setAbstractMethodDecl(AbstractMethodDecl AbstractMethodDecl) {
        this.AbstractMethodDecl=AbstractMethodDecl;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(AbstractClassDeclMethods!=null) AbstractClassDeclMethods.accept(visitor);
        if(AbstractMethodDecl!=null) AbstractMethodDecl.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(AbstractClassDeclMethods!=null) AbstractClassDeclMethods.traverseTopDown(visitor);
        if(AbstractMethodDecl!=null) AbstractMethodDecl.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(AbstractClassDeclMethods!=null) AbstractClassDeclMethods.traverseBottomUp(visitor);
        if(AbstractMethodDecl!=null) AbstractMethodDecl.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractClassDeclMethodsAbstract(\n");

        if(AbstractClassDeclMethods!=null)
            buffer.append(AbstractClassDeclMethods.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(AbstractMethodDecl!=null)
            buffer.append(AbstractMethodDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractClassDeclMethodsAbstract]");
        return buffer.toString();
    }
}
