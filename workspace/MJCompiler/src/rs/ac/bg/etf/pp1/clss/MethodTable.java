package rs.ac.bg.etf.pp1.clss;

import java.util.Iterator;
import java.util.LinkedList;

import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.ast.SyntaxNode;
import rs.ac.bg.etf.pp1.context.MethodTableContext;

public class MethodTable {
	
	private ClassDeclaration current;
	private LinkedList<MethodTableContext> list;
	
	public MethodTable(ClassDeclaration root)
	{
		list = new LinkedList<MethodTableContext>();
		this.current = root;
	}
	
	public Iterator<MethodTableContext> iterator()
	{
		return list.iterator();
	}
	
	public void append(MethodTableContext c)
	{
		if ( c != null )
			list.add(c);	
	}
	
	public void prepend(MethodTableContext c)
	{
		if ( c != null )
			list.add(0, c);
	}
	
	public int size()
	{
		return list.size();
	}
	
	public MethodTableContext getEntry(int i)
	{
		if ( i >= list.size() || i < 0 )
			return null;
		
		return list.get(i);
	}
	
	public MethodTableContext getLast()
	{
		return list.getLast();
	}
	
	public MethodTableContext getFirst()
	{
		return list.getFirst();
	}
	
	public MethodTableContext[] getMethodsAsArray()
	{
		MethodTableContext[] arr = new MethodTableContext[list.size()];
		Iterator<MethodTableContext> it = list.iterator();
		int i = 0;
		while( it.hasNext() )
			arr[i++] = it.next();
		return arr;
	}
	
	public ClassMethod methodExists( String name )
	{
		Iterator<MethodTableContext> it = list.iterator();
		MethodTableContext m;
		while( it.hasNext() )
		{
			m = it.next();
			if ( m.method.getMethodName().equals(name) )
				return m.method;
		}
		return null;
	}
	
	/**
	 * Creates method table.
	 * @param root
	 */
	public void initMethodTable(ClassDeclaration root, SyntaxNode node)
	{
		_rec_loadTable( root, node );
	}
	
	private void _rec_loadTable( ClassDeclaration decl, SyntaxNode node )
	{
		if ( decl == null )
			return;
		
		_rec_loadTable( decl.getSuperClass(), node );
		
		SemanticAnalyzer sa = SemanticAnalyzer.__instance__();
		if ( !sa.passed() )
			return;
		
		// Add methods.		
		Iterator<ClassMethod> it = decl.getAllMethods().getIterator();
		ClassMethod m;
		MethodTableContext mtc;
		ClassFormParam[] params;
		
		// Go through all methods inside given class declaration "decl"
		while( it.hasNext() )
		{
			m = it.next();
			params = m.getFormalParameters();
			mtc = null;
			if ( isAlreadyInTheList(m) )
				mtc = getContext(m);
			
			// Method is found in the list!
			if ( mtc != null )
			{
				// Check return value.
				if ( mtc.method.isReturnValuesOfReferenceType( m ) )
				{
					ClassFormParam p1 = mtc.method.getReturnValue();
					ClassFormParam p2 = m.getReturnValue();
					
					if ( !ClassDeclarationList.getInstance().isDerived(p1.getFormalParameterClassName(), p2.getFormalParameterClassName()) )
						sa.report_error("Return type is incompatibile with "
								+ "'" + mtc.originClass.getName() + "." + 
								ClassUtil.getMethodSignature(mtc.originClass, mtc.method, false, false) + "' on line " + m.getLine(), null);
					
				};
				
				if ( !mtc.method.isCompatibileReturnValue( m ) )
				{	
					if ( sa.passed() )						
						sa.report_error( 
								Functions.getUncompatibileTypesMessage(mtc.method.getReturnValue().getType(), m.getReturnValue().getType()) 
								+ " on line " + m.getLine() + ".", null
						);
													
				} else
				{				
					mtc.implementationClass = m.isAbstractMethod() ? null : decl;				
					
					if ( m.getAccessType() < mtc.method.getAccessType() )
					{
						list.remove(mtc);
						sa.report_error("Cannot reduce the visibility of the inherited method from " + mtc.originClass.getName() + " on line " + m.getLine() + ".", null);
					} else
					{
						mtc.method = m;					
					};	
				};
			} else	
			{
				MethodTableContext context = new MethodTableContext();
				context.implementationClass = m.isAbstractMethod() ? null : decl;
				context.originClass = decl;	
				context.method = m;
				list.add(context);
			};
		};
	}
	
	public void checkMethodTable( ClassDeclaration root, SyntaxNode node )
	{
		if ( root.isAbstractClass() )
			return;
		
		// Check if there are unimplemented methods.
		SemanticAnalyzer sa = SemanticAnalyzer.__instance__();
		Iterator<MethodTableContext> it = list.iterator();
		MethodTableContext c;
		while( it.hasNext() )
		{
			c = it.next();
			if ( c.implementationClass == null )
				sa.report_error("Concrete class '" + root.getName() + "' should implement method '" + ClassUtil.getMethodSignature(root, c.method, false) + "'", node);
		};
	}
	
	public boolean isAlreadyInTheList( ClassMethod method )
	{
		Iterator<MethodTableContext> it = list.iterator();
		MethodTableContext c;
		ClassMethod m;
		while( it.hasNext() )
		{
			c = it.next();
			m = c.method;
			if ( m.equals(method) )
				return true;
		};
		return false;
	}
	
	public MethodTableContext getContext( String name, ClassFormParam[] params )
	{
		Iterator<MethodTableContext> it = list.iterator();
		MethodTableContext c;
		while( it.hasNext() )
		{
			c = it.next();
			if ( c.method.getObj().getName().equals(name) )
			{
				if ( ClassMethod.equalParameters(c.method.getFormalParameters(), params) )
					return c;
			};
		};
		return null;
	}
	
	public MethodTableContext getContext( ClassMethod m )
	{
		Iterator<MethodTableContext> it = list.iterator();
		MethodTableContext c;
		while( it.hasNext() )
		{
			c = it.next();
			if ( c.method.getObj().getName().equals(m.getObj().getName()) )
			{
				if ( ClassMethod.equalParameters(c.method.getFormalParameters(), m.getFormalParameters()) )
					return c;
			};
		};
		return null;
	}
	
	@Override
	public String toString() 
	{
		
		StringBuilder str = new StringBuilder();
		
		str.append( "\nMETHOD TABLE: " + ( current.isAbstractClass() ? "abstract " : "" ) + "class " + current.getName() +  "\n" );
		
		appendSeparator(str);
		
		String format = "%1$15s %2$3s %7$20s %3$16s %2$3s %4$15s %2$3s %8$10s %2$3s %5$15s %2$3s %6$25s\n";
		str.append( String.format(format, 
				"Access Right",
				" ", 
				"ReturnType", 
				"Name", 
				"OriginClass",
				"Implementation Class",
				"Abstract/Concrete",
				"ParamNo") 
		);

		appendSeparator(str);
		
		Iterator<MethodTableContext> it = list.iterator();
		MethodTableContext c;
		int i = 0;
		while( it.hasNext() )
		{			
			c = it.next();
			
			str.append( 
					String.format(format, 
						Functions.getAccessRight( c.method.getAccessType() ), 
						" ", 
						 c.method.getReturnValue(), 
						c.method.getMethodName(), 
						c.originClass != null ? c.originClass.getName() : "null",
						c.implementationClass != null ? c.implementationClass.getName() : "null",
						c.method.isAbstractMethod() ? "abstract" : "",
						c.method.getFormalParametersNumber()
					) 
			);
			i++;
		};
		
		if ( i == 0 )
			str.append("\tNo methods present.\n");
		
		appendSeparator(str);
		
		return str.toString();
	}
	
	private void appendSeparator(StringBuilder str)
	{
		str.append("====================================================================" +
				"==============================================================================\n");
	}
}
