package rs.ac.bg.etf.pp1.checkers;

import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.context.ForeachLoopCheckerContext;
import rs.ac.bg.etf.pp1.des.DesignatorForeachLoop;
import rs.ac.bg.etf.pp1.gen.T_ComplexOperator;
import rs.ac.bg.etf.pp1.util.GetStartNode;
import rs.ac.bg.etf.pp1.util.HasComplexOperators;
import rs.ac.bg.etf.pp1.util.Stack;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class ForeachLoopChecker extends VisitorAdaptor {

	private SemanticAnalyzer sa;
	private Stack<ForeachLoopCheckerContext> stack;
	
	public ForeachLoopCheckerContext top()
	{
		return stack.top();
	}
	
	public ForeachLoopChecker(SemanticAnalyzer sa)
	{
		this.sa = sa;
		this.stack = new Stack<ForeachLoopCheckerContext>();
	}
	
	private void checkDesignator( Designator d )
	{
		DesignatorForeachLoop s = new DesignatorForeachLoop(sa, stack, this);
		d.traverseBottomUp(s);
	}
	
	public void visit( ForeachStatementStart start )
	{
		stack.push( new ForeachLoopCheckerContext() );
	}
	
	public void visit( ForeachStatement end )
	{
		stack.pop();
		if ( !sa.passed() )
			return;
	}
	
	public void visit( ForeachParensContent c )
	{
		if ( !sa.passed() )
			return;
		
		DesignatorForeachLoop g = new DesignatorForeachLoop(sa, stack, this);
		String ident = c.getI1();
		Designator d = c.getDesignator();
		Obj o = Tab.find(ident);
		
		if ( o == Tab.noObj )
		{
			sa.report_error("Variable '" + ident + "' cannot be used in foreach statement because it's not defined", c.getParent());
		} else
		{
			if ( o.getKind() != Obj.Var )
				sa.report_error("Foreach loop needs iterator to be a variable", c.getParent());
			else
			{
				// Check if designator is array.
				g.setCheckType(1);
				g.setValidObj(o);
				d.traverseBottomUp(g);
			};
		};
	}
	
	public void visit( EqualExpr e )
	{
		checkDesignator( e.getEqualExprStart().getDesignator() );
	}
	
	public void visit(PlusPlusExpr ppe)
	{
		checkDesignator(ppe.getDesignator());
	}
	
	public void visit(MinusMinusExpr mme)
	{
		checkDesignator(mme.getDesignator());
	}
	
	public void visit( ExprNoError e )
	{
		HasComplexOperators hco = new HasComplexOperators();
		hco.performCheck(e);
		if ( hco.cond() )
		{
			GetStartNode gsn = new GetStartNode();
			SyntaxNode startValueNode;
			
			try {
				// Finds start node from which evaluation begins.
				e.traverseTopDown(gsn);
				startValueNode = gsn.getFactor();
				if ( startValueNode == null )
					throw new Exception("Foreach: Start node could not be found");
				
				// Find all expressions linked with complex operator.
				T_ComplexOperator t = new T_ComplexOperator(startValueNode);
				e.traverseBottomUp(t);
				SyntaxNode[] nodes = t.getAllNodes();				
				for (int i=0; i<nodes.length; i++)
				{
					FactorDesignator fd = (FactorDesignator) nodes[i];
					checkDesignator( fd.getDesignator() );
				};
			} catch( Exception ex ) {
				ex.printStackTrace();
			};
		};
	}
	
	public void visit(ForStatement fs)
	{
		ForLoopChecker flc = new ForLoopChecker(sa);
		fs.traverseBottomUp(flc);
	}
}
