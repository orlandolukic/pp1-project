package rs.ac.bg.etf.pp1.clss;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.util.Scopes;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class ClassMethod {
	
	public static final int PRIVATE = 0,
							PROTECTED = 1,
							PUBLIC = 2;
	
	public static boolean equalParameters( ClassFormParam[] p1, ClassFormParam[] p2 )
	{
		if ( p1.length != p2.length )
			return false;
		
		int i=0;
		for (; i<p1.length; i++)
		{
			if ( !p1[i].equals(p2[i]) )
				return false;
		};
		
		return true;
	}
	
	private Obj method;
	private int accessType;
	private boolean isVirtual;
	private int line;
	private ClassFormParam returnValue;
	private LinkedList<ClassFormParam> params;
	private int refactorVirtTablePosition;
	
	public ClassMethod( ClassDeclaration classDecl, Obj method, int accessType, boolean isVirtual, ClassFormParam returnValue, int line)
	{
		this.method = method;
		this.accessType = accessType;
		this.isVirtual = isVirtual;
		this.line = line;
		this.returnValue = returnValue;
		this.params = new LinkedList<ClassFormParam>();
		this.refactorVirtTablePosition = -1;
		
		// Add this to the class fields.
		classDecl.getSymbolTableEntry().getType().getMembersTable().insertKey(method);
		Tab.openScope();
		Scopes.put( ClassUtil.getScopeNameForMethod(classDecl, method.getName()), Tab.currentScope() );
		Obj v = Tab.insert( Obj.Var, "this", ClassUtil.getStructForClassname(classDecl.getName(), false) );
		v.setFpPos(0);
		method.setLevel(1);
		Tab.chainLocalSymbols(method);
	}
	
	public void setRefactorPositionInVirtualTable( int position )
	{
		this.refactorVirtTablePosition = position;
	}
	
	public void setStartAddress( int addr )
	{
		if ( addr > 0 )
			method.setAdr(addr);
	}
	
	public int getStartAddress()
	{
		return method.getAdr();
	}
	
	public ClassFormParam getReturnValue()
	{
		return returnValue;
	}
	
	public Obj getObj()
	{
		return method;
	}
	
	public void setAccessType( int type )
	{
		if ( type >= 0 && type <= 2 )
			accessType = type;
	}
	
	public int getAccessType()
	{
		return accessType;
	}	
	
	public String getMethodName()
	{
		return method.getName();
	}
	
	public void setVirtual(boolean val)
	{
		isVirtual = val;
	}
	
	public boolean isAbstractMethod()
	{
		return isVirtual;
	}
	
	public Struct getReturnType()
	{
		return method.getType();
	}
	
	public int getFormalParametersNumber()
	{
		return params.size();
	}
	
	public int getLine()
	{
		return line;
	}
	
	public void addFormalParam(ClassFormParam param)
	{
		params.add(param);
	}
	
	public ClassFormParam[] getFormalParameters()
	{
		ClassFormParam[] arr = new ClassFormParam[params.size()];
		Iterator<ClassFormParam> it = params.iterator();
		int i = 0;
		while( it.hasNext() )
			arr[i++] = it.next();
	
		return arr;
	}
	
	public boolean parameterExists( ClassFormParam param ) 
	{
		Iterator<ClassFormParam> it = params.iterator();
		ClassFormParam c;
		while( it.hasNext() )
		{
			c = it.next();
			if ( c.equalsNameType(param) )
				return true;
		};
		
		return false;
	}
	
	public boolean parameterExists( String name ) 
	{
		Iterator<ClassFormParam> it = params.iterator();
		ClassFormParam c;
		while( it.hasNext() )
		{
			c = it.next();
			if ( c.getName().equals(name) )
				return true;
		};
		
		return false;
	}
	
	public void chainParametersIntoSymbolTable()
	{
		Iterator<ClassFormParam> it = params.iterator();
		ClassFormParam c;
		int i = 1;
		Obj o;
		while( it.hasNext() )
		{
			c = it.next();
			o = Tab.insert( Obj.Var, c.getName(), c.getType() );
			o.setFpPos(i);
			i++;
		};
		method.setLevel(i);
		Tab.chainLocalSymbols(method);
	}
	
	/**
	 * Adds all parameters to the table entry of the given class
	 * @param decl
	 */
	public void methodDeclarationFinished( ClassDeclaration decl )
	{
		if ( SemanticAnalyzer.__instance__().passed() )
		{
			SemanticAnalyzer sa = SemanticAnalyzer.__instance__();
			if ( decl.getAllMethods().isAlreadyDefined( this, method.getName(), getFormalParameters() ) )
			{
				sa.report_error("Duplicate method '" + 
						ClassUtil.getMethodSignature(decl, this, true) + "' in type '" + decl.getName() + "' on line " + line + ".", null);
			};
		};

		Tab.closeScope();
	}
	
	@Override
	public boolean equals(Object obj) 
	{
		if ( obj instanceof ClassMethod )
		{
			ClassMethod m = (ClassMethod) obj;
			return this.method.getName().equals( m.method.getName() ) 			// Two methods have the same name
					&&
					this.checkFormalParameters(m);								// Two methods have the same parameters.
		};
		
		return false;
	}
	
	public boolean haveTheSameParameters( ClassMethod m )
	{
		return checkParameters(m);
	}
	
	private boolean checkParameters(ClassMethod m1)
	{
		Obj o1 = this.method, o2 = m1.method;
		if ( o1.getLevel() != o2.getLevel() )
			return false;
		Collection<Obj> localsO1 = o1.getLocalSymbols(),
						localsO2 = o2.getLocalSymbols();
		Iterator<Obj> itO1 = localsO1.iterator(),
					  itO2 = localsO2.iterator();
		Obj t1, t2;
		int fp = o1.getLevel(), 
			i = 0;
		while( itO1.hasNext() && i < fp )
		{
			 t1 = itO1.next(); 
			 t2 = itO2.next();
			 if ( !t1.getType().equals(t2.getType()) )
				 return false;
			 i++;
		};
		return true;
	}
	
	public boolean equalReturnValue( ClassMethod m1 )
	{
		return this.returnValue.equalsReturnValue(m1.getReturnValue());
	}
	
	private boolean checkFormalParameters( ClassMethod m1 )
	{
		if ( m1.getFormalParametersNumber() != this.getFormalParametersNumber() )
			return false;
		
		ClassFormParam[] arr1 = getFormalParameters(), arr2 = m1.getFormalParameters();
		for (int i=0, n = getFormalParametersNumber(); i<n; i++)
		{
			if ( !arr1[i].equalsType( arr2[i] ) )
				return false;
		};
		
		return true;
	}
	
	public boolean isCompatibileReturnValue( ClassMethod m )
	{
		ClassFormParam param = m.getReturnValue();
		if ( param.isClassReference() && returnValue.isClassReference() )
		{
			String c2 = param.getFormalParameterClassName(),
				   c1 = returnValue.getFormalParameterClassName();
			
			if ( ClassDeclarationList.getInstance().isDerived(c1, c2) )
				return true;
			
		} else if ( param.isClassReference() && !returnValue.isClassReference() || !param.isClassReference() && returnValue.isClassReference() )
			return false;
		else 
		{
			return param.getType().compatibleWith( returnValue.getType() );
		};		
		
		return false;
	}
	
	public boolean isReturnValuesOfReferenceType( ClassMethod m )
	{
		return this.returnValue.isClassReference() && m.getReturnValue().isClassReference();
	}
	
	/**
	 * When class declaration is finished, go through all the methods inside class and add refactor code.
	 */
	public void addRefactorCode()
	{
		Code.loadConst( this.method.getAdr() );
		Code.put( Code.putfield );
		Code.put2( this.refactorVirtTablePosition );
	}
}
