package rs.ac.bg.etf.pp1.gen;

import java.util.LinkedList;

import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.clss.ClassUtil;
import rs.ac.bg.etf.pp1.des.DesignatorFinder;
import rs.ac.bg.etf.pp1.des.DesignatorTDExpr;
import rs.ac.bg.etf.pp1.util.CGExprContext;
import rs.ac.bg.etf.pp1.util.HasComplexOperators;
import rs.ac.bg.etf.pp1.util.IsMethodCheck;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.*;
import rs.etf.pp1.symboltable.concepts.*;

/**
 * Class used to generate code for expression.
 * <br>Can generate only expressions without complex operators.
 * 
 * @author ol150390d
 *
 */
public class TD_Expr {
	
	protected CGExprContext context;
	private LinkedList<SyntaxNode> list;
	
	public TD_Expr()
	{
		this.context = null;
		this.list = new LinkedList<SyntaxNode>();
	}
	
	public boolean isVar()
	{
		return context.isVar;
	}	
	
	public boolean isConst()
	{
		return context.isConst;
	}
	
	public boolean isExpr()
	{
		return !context.isVar && !context.isConst;
	}
	
	public Obj getObj()
	{
		return context.var;
	}
	
	public Struct getType()
	{
		return context.type;
	}
	
	public CGExprContext getContext()
	{
		return context;
	}
	
	public void setContext( CGExprContext context )
	{
		this.context = context;
	}
	
	public void finish( int status ) throws TDExprException
	{
		throw new TDExprException( status );
	}
	
	public void createNewContext(int side) 
	{
		this.context = new CGExprContext(side);
		this.context.methodCodeGeneration = new MethodCodeGeneration();
		this.context.methodCodeGeneration.setGenerationOfActualParameter(true);
	}
	
	public void returnPrevContext(CGExprContext c)
	{
		this.context = c;
	}
	
	/**
	 * ========================================================================================================================
	 * 		Visit methods
	 * ========================================================================================================================
	 */
	
	public void visit(ExprNoError e)
	{
		// First, check if expression has complex operators.
		HasComplexOperators hco = new HasComplexOperators();
		e.traverseTopDown(hco);
		boolean hasComplexOperators = hco.cond();
		
		if ( !hasComplexOperators )
		{
			NegativeExpr ne = e.getNegativeExpr();
			Term term = e.getTerm();
			AddopTerm at = e.getAddopTerm();
			
			context.potentiallyVarConst = at instanceof NoAddopTerm;
			
			try {
				this.visit(ne);
				this.visit(term);
				this.visit(at);
			} catch( TDExprException ex )
			{			
				subFirstFactor();
			};
		} else
			throw new RuntimeException("Cannot generate code for complex operator with class TD_Expr.java");
	}
	
	public void visitFactor(Factor f)
	{
		try {
			this.visit(f);
		} catch( Exception e ) {};
	}
	
	private void visit( NegativeExpr ne ) throws TDExprException
	{
		context.hasSign = ne instanceof NegativeExprMinus;
		if ( context.hasSign )
		{
			Code.load( Functions.constantInt(0) );
		};
	}
	
	private void visit(Term term) throws TDExprException
	{
		Factor f = term.getFactor();
		MulopFactor mf = term.getMulopFactor();
		
		context.potentiallyVarConst &= mf instanceof NoMulopFactor;
		context.hasInnerExpr = f instanceof FactorWithExpr;
		
		this.visit(f);
		this.visit(mf);
	}
	
	public void visit(AddopTerm at) throws TDExprException
	{
		AddopTerm f = at;
		Addop t_a = null;
		Term t_term = null;

		if ( f instanceof AddopTermAll )
		{
			t_a = ((AddopTermAll) f).getAddop();
			t_term = ((AddopTermAll) f).getTerm();
			f = ((AddopTermAll) at).getAddopTerm();
			visit(f);
		} else
			f = null;
		
		if ( f != null )
		{
			this.visit(t_term);
			Functions.generateAddopOperator(t_a);
		};
	}
	
	public void visit(Factor factor) throws TDExprException
	{
		if ( factor instanceof FactorConstDeclType )
			this.visit_((FactorConstDeclType)factor);
		
		else if ( factor instanceof FactorDesignator )
			this.visit_((FactorDesignator)factor);
		
		else if ( factor instanceof FactorNew )
			this.visit_((FactorNew)factor);
		
		else if ( factor instanceof FactorWithExpr )
			this.visit_((FactorWithExpr)factor);
		
		// Make subtraction for first factor.
		subFirstFactor();
	}
	
	private void visit(MulopFactor factor) throws TDExprException
	{
		MulopFactor f = factor;
		Mulop t_m = null;
		Factor t_factor = null;

		if ( f instanceof MulopFactorAll )
		{
			t_m = ((MulopFactorAll) f).getMulop();
			t_factor = ((MulopFactorAll) f).getFactor();
			f = ((MulopFactorAll) f).getMulopFactor();
			visit(f);
		} else
			f = null;
		
		if ( f != null )
		{
			this.visit(t_factor);
			Functions.generateMulopOperator(t_m);
		};
	}
	
	private void visit_(FactorConstDeclType f) throws TDExprException
	{
		ExprConstDeclType x = f.getExprConstDeclType();
		context.isConst = context.potentiallyVarConst;
		
		if ( x instanceof EBoolConst )				// Boolean const.
		{
			Code.load( Functions.constantInt( ((EBoolConst) x).getVal().equals("false") ? 0 : 1 ) );
			context.type = Functions.boolType;		
			finish(TDExprException.FINISHED);
		} else if ( x instanceof ECharConst )		// Char const.
		{
			Code.load( Functions.constantChar(((ECharConst) x).getVal().charAt(1)) );
			if ( context.potentiallyVarConst )
			{			
				context.type = Tab.charType;				
				finish(TDExprException.FINISHED);
			};
		} else if ( x instanceof ENumberConst )		// Number const.
		{
			Code.load( Functions.constantInt(((ENumberConst) x).getVal()) );
			if ( context.potentiallyVarConst )
			{
				context.type = Tab.intType;
				finish(TDExprException.FINISHED);
			};
		};
	}
	
	private void visit_(FactorNew f) throws TDExprException
	{
		TD_Expr tde = new TD_Expr();
		tde.createNewContext(CGExprContext.SIDE_EXPR);
		
		FactorExpr fe = f.getFactorExpr();
		Struct type = Functions.getTypeBySubstitution(f.getType());
		int bw = type.getKind() == Struct.Char ? 0 : 1;
		boolean isArray = false;
		Type t = f.getType();
		
		// Get number of elements.
		if ( fe instanceof FactorExprAll )
		{
			isArray = true;
			tde.visit((ExprNoError) ((FactorExprAll) fe).getExpr());
		};
		
		if ( context.side == CGExprContext.SIDE_RIGHT )
		{			
			if ( isArray )
			{
				Code.put(Code.newarray);
				Code.put(bw);
			} else	// Object.
			{
				String typename = ((TIdent) t).getType();
				rs.ac.bg.etf.pp1.clss.ClassDeclaration decl = ClassUtil.getClassDeclarationByName(typename);
				Code.put( Code.new_ );
				Code.put2( ( decl.getClassFieldsNumber()+1 ) << 2 );
				
				// Set first field to be the virtual table pointer.
				Code.put( Code.dup );
				Code.loadConst( decl.getStartAddressVirtualTable(-1) );
				Code.put( Code.putfield ); Code.put2(0);
			}
		};
	}
	
	private void visit_(FactorWithExpr f)
	{		
		// Create new context.
		CGExprContext prev = this.context;
		createNewContext(CGExprContext.SIDE_EXPR);
		
		this.visit( (ExprNoError) f.getExpr() );
		
		returnPrevContext(prev);
	}
	
	private void visit_(FactorDesignator fd) throws TDExprException
	{
		Designator d = fd.getDesignator();
		FactorParens fp = fd.getFactorParens();
		
		this.visit(d);
		this.visit(fp);		
	}
	
	protected void visit(Designator d) throws TDExprException
	{	
		DesignatorTDExpr c = new DesignatorTDExpr(this);
		c.setSide(DesignatorFinder.RIGHT_SIDE);		
		c.traverse(d);
		
		// Generate code to get variable.
		c.generateVariable();
		if ( c.isFinished() && c.getObject().getKind() != Obj.Meth )
			finish(TDExprException.FINISHED);
	}
	
	private void visit(FactorParens fp) throws TDExprException
	{		
		// Handle function call.
		if ( fp instanceof FactorParensPresent )
		{
			generateParameters( ((FactorParensPresent) fp).getActParsWrapper() );
			
			Functions.functionCallGenerateCode( context.methodCodeGeneration, fp.getParent(), ((FactorParensPresent) fp).getActParsWrapper() );
		
			if ( context.potentiallyVarConst )
			{
				Designator d = ((FactorDesignator) (fp.getParent())).getDesignator();
				DesignatorFinder finder = new DesignatorFinder(null);
				finder.setCheck(false);
				finder.setSide( DesignatorFinder.RIGHT_SIDE );
				finder.traverse(d);				
				context.type = finder.getObject().getType();
				finish(TDExprException.FINISHED);
			}
		};
	}
	
	public void generateParameters( ActParsWrapper wrapper )
	{
		if ( wrapper instanceof ActParsPresent )
			visit( (ActPars) ((ActParsPresent) wrapper).getActPars() );
	}
	
	private void visit(ActPars aps)
	{
		ActParam p = aps.getActParam();
		ActParsMore m = aps.getActParsMore();
		visit(p);
		visit(m);
	}
	
	private void visit(ActParsMore m)
	{
		ActParsMore f = m;
		ActParam t_param = null;
		
		if ( f instanceof ActParsMorePresent )
		{
			t_param = ((ActParsMorePresent) f).getActParam();
			f = ((ActParsMorePresent) f).getActParsMore();
			visit(f);
		} else
			f = null;
		
		if ( f != null )
		{
			this.visit(t_param);
		};
	}
	
	private void visit(ActParam param)
	{
		if ( !context.generateActualParam || list.contains(param) )
			return;
		
		HasComplexOperators hco = new HasComplexOperators();
		hco.performCheck( param.getExpr() );
		if ( !hco.cond() )
		{
			// Check if parameter is actually a method.
			ExprNoError e = (ExprNoError) param.getExpr();		
			CGExprContext context = this.context;
			this.createNewContext(CGExprContext.SIDE_EXPR); 
			visit(e);
			this.context = context;
		} else
		{
			ComplexOperator co = new ComplexOperator();
			co.generate(param.getExpr());
		};
		list.add(param);
	}
	
	private void subFirstFactor()
	{
		// SUB!
		if ( context.hasSign && context.firstFactor )
		{
			context.firstFactor = false;
			Code.put(Code.sub);
		};
	}
	
	/**
	 * ========================================================================================================================
	 * 		Visit methods for complex operators
	 * ========================================================================================================================
	 */
	
	public void visitGlobal(SyntaxNode snode)
	{
		try {
			if ( snode instanceof Factor )
				this.visit((Factor)snode);
		} catch( Exception e ) {};
	}
	
}


/**
 * Exception class for TD_Expr.
 * 
 * @author ol150390d
 */
class TDExprException extends Exception
{
	private static final long serialVersionUID = -1954516285730446645L;
	public static final int FINISHED = 0;
	public static final int POP = 1;
	
	private int status;
	
	public TDExprException(int status)
	{
		this.status = status;
	}
	
	public int getStatus()
	{
		return status;
	}
}
