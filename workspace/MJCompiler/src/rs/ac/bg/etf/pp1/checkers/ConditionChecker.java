package rs.ac.bg.etf.pp1.checkers;

import rs.ac.bg.etf.pp1.ExprChecker;
import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.context.ConditionCheckerContext;
import rs.ac.bg.etf.pp1.util.GetTypeOfExpr;
import rs.etf.pp1.symboltable.concepts.*;

public class ConditionChecker extends VisitorAdaptor {

	private SemanticAnalyzer sa;
	private ConditionCheckerContext context;
	
	public ConditionChecker(SemanticAnalyzer sa)
	{
		this.sa = sa;
		context = new ConditionCheckerContext();
	}
	
	public ConditionCheckerContext getConditionContext()
	{
		return context;
	}
	
	public void visit(CondFact cf)
	{	
		if ( !sa.passed() || context.depth > 1 )
			return;
		
		context.tempCondition = false;
		ExprNoError leftExpr = (ExprNoError) cf.getExpr();
		CondFactVolatile volatilePart = cf.getCondFactVolatile();
		Invertor inv = cf.getInvertor();
		boolean isInverted = inv instanceof InvertorPresent;
		
		if ( isInverted && volatilePart instanceof CondFactNotVolatile )
		{
			sa.report_error("Cannot use operator '!' on expression", ((ExprNoError)cf.getExpr()).getTerm());
			return;
		};
		
		if ( volatilePart instanceof CondFactIsVolatile )
		{
			ExprChecker ec = new ExprChecker(Functions.boolType);
			boolean ok = ec.process(leftExpr);
			if ( !ok )
			{
				sa.report_checker(ec, leftExpr.getTerm());
				return;
			};
			
			Obj val = ec.getContext().constReturn;
			Obj meth = ec.getContext().methReturn;
			// Caught constant boolean expression.
			if ( val != null )
			{
				int value = val.getAdr();
				boolean boolVal = value == 0 ? false : true;
				if ( isInverted )
					boolVal = !boolVal;
				if ( context.condFactNumber == 0 )
					context.startFactCondition = boolVal;
				context.tempCondition = boolVal;
				
				if ( context.condFactNumber > 0 )
					context.factCondition &= context.tempCondition;
				
			} else if ( meth != null )
			{
				context.factConstantCondition &= false;
			};
		} else		// Relop Expr
		{
			Relop r = ((CondFactNotVolatile) volatilePart).getRelop();
			boolean allowedReferenceOperator = r instanceof REQ || r instanceof RNEQ;
			
			// Check first && second expression.
			ExprNoError rightExpr = (ExprNoError) ((CondFactNotVolatile) volatilePart).getExpr();
			GetTypeOfExpr left = new GetTypeOfExpr();
			GetTypeOfExpr right = new GetTypeOfExpr();
			leftExpr.traverseBottomUp(left);
			rightExpr.traverseBottomUp(right);
			
			Struct leftStruct = left.getType();
			Struct rightStruct = right.getType();
			
			// Check allowed operators.
			if ( leftStruct.isRefType() && !allowedReferenceOperator )
			{
				sa.report_error("Cannot use relation operator '" + Functions.getRelopOperator(r) 
				+  "' for two references", rightExpr.getTerm());
				return;
			};
			
			// Perform check!
			if ( !leftStruct.compatibleWith(rightStruct) && 
					( 
							(!leftStruct.isRefType() && rightStruct.isRefType()) || 
							(leftStruct.isRefType() && !rightStruct.isRefType()) || 
							(!leftStruct.isRefType() && !rightStruct.isRefType()) 
					)
			)
			{
				sa.report_error("Left-hand side expression of type '" + Functions.getVariableTypeAsString(leftStruct) +  "'"
						+ " is not compatibile with right-hand side expression of type '" 
						+ Functions.getVariableTypeAsString(rightStruct) + "'"
						, leftExpr.getTerm());
				return;
			};
		
			context.factConstantCondition = false;			
		};
		context.condFactNumber++;
	}
	
	public void visit(CondFactMoreNotPresent cfmnp)
	{
		if ( !sa.passed() || context.depth > 1 )
			return;
		
		if ( context.condFactNumber == 1 )
			context.factCondition &= context.startFactCondition;
	}
	
	/**
	 * ... && CondFact (*)
	 */
	public void visit(CondFactMorePresent cfm)
	{
		if ( !sa.passed() || context.depth > 1 )
			return;
		
		context.factCondition &= context.tempCondition;
	}
	
	/**
	 * ... || CondTerm ...
	 */
	public void visit(CondTermMorePresent ctm)
	{	
		if ( !sa.passed() || context.depth > 1 )
			return;
		
		context.condTermNumber++;
		context.termCondition |= context.condFactNumber == 1 ? context.startFactCondition : context.factCondition;
		context.isConstantCondition &= context.factConstantCondition;
		context.condFactNumber = 0;
		context.factCondition = true;
		context.factConstantCondition = true;
	}
	
	public void visit(CondTermMoreNotPresent ctmnp)
	{
		if ( !sa.passed() || context.depth > 1 )
			return;
		
		if ( context.condFactNumber == 1 || context.condTermNumber == 0 && ctmnp.getParent() instanceof Condition )
		{
			context.termCondition |= context.startFactCondition;
			context.isConstantCondition &= context.factConstantCondition;
		};
		context.condFactNumber = 0;
		context.factCondition = true;
		context.factConstantCondition = true;
	}
	
	public void visit(IfStart i)
	{
		context.depth++;
	}
	
	public void visit(MatchedIfElse i)
	{
		context.depth--;
	}
	
	public void visit(UnmatchedIf i)
	{
		context.depth--;
	}
	
	public void visit(UnmatchedIfElse i)
	{
		context.depth--;
	}
	
	public void visit(ConditionNoError c)
	{
		if ( !sa.passed() || context.depth > 1 )
			return;
		
		context.condition = context.termCondition;
		context.isAlwaysFalse = context.isConstantCondition && !context.condition;
		context.isAlwaysTrue = context.isConstantCondition && context.condition;
		
		if ( context.depth == 1 )
		{
			SyntaxNode node = null;
			UnmatchedIfElse n1 = c.getParent().getParent() instanceof UnmatchedIfElse ? (UnmatchedIfElse) c.getParent().getParent() : null;
			MatchedIfElse n2 = c.getParent().getParent() instanceof MatchedIfElse ? (MatchedIfElse) c.getParent().getParent() : null;
			
			if ( c.getParent().getParent() instanceof UnmatchedIfElse )
				node = n1.getMatched();
			else if ( c.getParent().getParent() instanceof MatchedIfElse )
				node = n2.getMatched();
			
			if ( context.isAlwaysFalse() )
				sa.report_error("Dead code", node);
		};
	}
	
	public void visit(FactorNew fn)
	{
		if ( !sa.passed() || context.depth >= 1 )
			return;
		
		sa.report_error("Cannot use operator 'new' in condition check", fn);
	}
	
	/**
	 * if ( ... ) { ... } else (*) { ... }
	 */
	public void visit(ElseBegin e)
	{
		if ( !sa.passed() || context.depth > 1 )
			return;
		
		if ( context.depth == 1 && context.isAlwaysTrue() )
		{
			SyntaxNode node = null;
			UnmatchedIfElse n1 = e.getParent() instanceof UnmatchedIfElse ? (UnmatchedIfElse) e.getParent() : null;
			MatchedIfElse n2 = e.getParent() instanceof MatchedIfElse ? (MatchedIfElse) e.getParent() : null;
			
			if ( e.getParent() instanceof UnmatchedIfElse )
				node = n1.getUnmatched();
			else if ( e.getParent() instanceof MatchedIfElse )
				node = n2.getMatched1();
				
			sa.report_error("Dead code", node);
		};
	}
}
