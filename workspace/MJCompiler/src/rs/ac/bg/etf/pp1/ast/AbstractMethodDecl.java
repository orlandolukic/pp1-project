// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class AbstractMethodDecl implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private AbstractMethodDeclStart AbstractMethodDeclStart;
    private FormParsWrapper FormParsWrapper;

    public AbstractMethodDecl (AbstractMethodDeclStart AbstractMethodDeclStart, FormParsWrapper FormParsWrapper) {
        this.AbstractMethodDeclStart=AbstractMethodDeclStart;
        if(AbstractMethodDeclStart!=null) AbstractMethodDeclStart.setParent(this);
        this.FormParsWrapper=FormParsWrapper;
        if(FormParsWrapper!=null) FormParsWrapper.setParent(this);
    }

    public AbstractMethodDeclStart getAbstractMethodDeclStart() {
        return AbstractMethodDeclStart;
    }

    public void setAbstractMethodDeclStart(AbstractMethodDeclStart AbstractMethodDeclStart) {
        this.AbstractMethodDeclStart=AbstractMethodDeclStart;
    }

    public FormParsWrapper getFormParsWrapper() {
        return FormParsWrapper;
    }

    public void setFormParsWrapper(FormParsWrapper FormParsWrapper) {
        this.FormParsWrapper=FormParsWrapper;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(AbstractMethodDeclStart!=null) AbstractMethodDeclStart.accept(visitor);
        if(FormParsWrapper!=null) FormParsWrapper.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(AbstractMethodDeclStart!=null) AbstractMethodDeclStart.traverseTopDown(visitor);
        if(FormParsWrapper!=null) FormParsWrapper.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(AbstractMethodDeclStart!=null) AbstractMethodDeclStart.traverseBottomUp(visitor);
        if(FormParsWrapper!=null) FormParsWrapper.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractMethodDecl(\n");

        if(AbstractMethodDeclStart!=null)
            buffer.append(AbstractMethodDeclStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(FormParsWrapper!=null)
            buffer.append(FormParsWrapper.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractMethodDecl]");
        return buffer.toString();
    }
}
