// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class PrintStatement extends Matched {

    private PrintWrapper PrintWrapper;

    public PrintStatement (PrintWrapper PrintWrapper) {
        this.PrintWrapper=PrintWrapper;
        if(PrintWrapper!=null) PrintWrapper.setParent(this);
    }

    public PrintWrapper getPrintWrapper() {
        return PrintWrapper;
    }

    public void setPrintWrapper(PrintWrapper PrintWrapper) {
        this.PrintWrapper=PrintWrapper;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(PrintWrapper!=null) PrintWrapper.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(PrintWrapper!=null) PrintWrapper.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(PrintWrapper!=null) PrintWrapper.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("PrintStatement(\n");

        if(PrintWrapper!=null)
            buffer.append(PrintWrapper.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [PrintStatement]");
        return buffer.toString();
    }
}
