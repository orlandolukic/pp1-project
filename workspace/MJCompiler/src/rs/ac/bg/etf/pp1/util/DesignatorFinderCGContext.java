package rs.ac.bg.etf.pp1.util;

import rs.ac.bg.etf.pp1.ast.DesignatorArrayAccess;
import rs.ac.bg.etf.pp1.ast.DesignatorObjectAccess;
import rs.ac.bg.etf.pp1.ast.SyntaxNode;
import rs.ac.bg.etf.pp1.clss.ClassDeclaration;
import rs.etf.pp1.symboltable.concepts.Obj;

public class DesignatorFinderCGContext {
	
	public Obj obj;
	public SyntaxNode node;
	public ClassDeclaration decl;
	public boolean toLoad;
	public boolean ok;
	public boolean isLast;
	public int ind;
	public boolean isThis;
	
	public DesignatorFinderCGContext( Obj obj, ClassDeclaration decl, boolean toLoad, SyntaxNode node, boolean isThis )
	{
		this(obj, decl, toLoad, node);
		this.isThis = isThis;
	}
	
	public DesignatorFinderCGContext( Obj obj, ClassDeclaration decl, boolean toLoad, SyntaxNode node )
	{
		this.obj = obj;
		this.node = node;
		this.decl = decl;
		this.toLoad = toLoad;
		this.isLast = false;
		this.ok = true;
		this.isThis = false;
		this.ind = 0;
		
		/*
		int a = 20;
		int niz[];
		niz = new int[100];
		for (int i=0; i<100; i++) niz[i] = 1;
		niz[a+=4] *= niz[a] -= niz[a-=2] += a;
		System.out.print(niz[a]);
		*/
	}
	
	public boolean isArrayAccess()
	{
		return node instanceof DesignatorArrayAccess;
	}
	
	public boolean isObjectPropertyAccess()
	{
		return node instanceof DesignatorObjectAccess;
	}
	
}
