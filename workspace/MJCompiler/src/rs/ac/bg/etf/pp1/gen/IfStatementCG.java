package rs.ac.bg.etf.pp1.gen;

import java.util.Iterator;
import java.util.LinkedList;

import rs.ac.bg.etf.pp1.CodeGenerator;
import rs.ac.bg.etf.pp1.ExprChecker;
import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.context.ConditionCheckerContext;
import rs.ac.bg.etf.pp1.util.CGExprContext;
import rs.ac.bg.etf.pp1.util.GetTypeOfExpr;
import rs.ac.bg.etf.pp1.util.Stack;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class IfStatementCG extends VisitorAdaptor {
	
	private CodeGenerator cg;	
	private Stack<ConditionCheckerContext> stack;
	private int created;
	
	public IfStatementCG(CodeGenerator cg)
	{
		this.cg = cg;
		this.stack = new Stack<ConditionCheckerContext>();
		this.stack.push( new ConditionCheckerContext(true) );
		this.created = 0;
	}
	
	public ConditionCheckerContext top()
	{
		return stack.top();
	}
	
	private void init()
	{
		this.created++;
		if ( created == 1 )
			return;
		
		stack.push( new ConditionCheckerContext(true) );
	}
	
	/**
	 * Backpatch all adresses.
	 */
	private void backpatch(LinkedList<Integer> reference)
	{
		int i;
		Iterator<Integer> it = reference.iterator();
		while( it.hasNext() )
		{
			i = it.next().intValue();
			Code.fixup(i);
		};
	}
	
	public void generateCondition(Condition cond)
	{
		init();
		_generateCondition(cond);
	}
	
	private void _generateCondition(Condition cond)
	{
		// Generate condition.
		cond.traverseBottomUp(this);
		
		// Add jump to else part.
		Code.putJump(0);
	}
	
	public void generateAll( UnmatchedIf i )
	{
		init();
		
		// Generate condition first.
		_generateCondition( i.getIfCondition().getCondition() );
		top().refactorElsePart.add( new Integer(Code.pc-2) );
		
		// Set all true conditions to lead to this point.
		backpatch( top().refactorIfPart );
		
		// Generate TRUE part of the condition.
		i.getStatementListOptions().traverseBottomUp(this);
		
		// Refactor address to lead to the end of the if statement.
		backpatch( top().refactorElsePart );
	}
	
	public void generateAll( UnmatchedIfElse i )
	{
		init();
		
		// Generate condition first.
		_generateCondition( i.getIfCondition().getCondition() );
		top().refactorElsePart.add( new Integer(Code.pc-2) );
		
		// Set all true condition to lead to this part.
		backpatch( top().refactorIfPart );
		
		// Generate TRUE part of the condition.
		i.getMatched().traverseBottomUp(this);
		Code.putJump(0);
		int fixAddr = Code.pc - 2;
		
		// Refactor address to lead to else part of the if statement.
		backpatch( top().refactorElsePart );
		
		// Generate FALSE part of the condition.
		i.getUnmatched().traverseBottomUp(this);
		
		// Fix jump after TRUE part.
		Code.fixup(fixAddr);
	}
	
	public void generateAll( MatchedIfElse i )
	{
		init();
		
		// Generate condition first.
		_generateCondition( i.getIfCondition().getCondition() );
		//backpatch( top().refactorNextCondition );
		top().refactorElsePart.add( new Integer(Code.pc-2) );
		
		System.out.println( "Code.pc = " + (Code.pc) );
		
		// Set all true condition to lead to this point.
		backpatch( top().refactorIfPart );
		
		// Generate TRUE part of the condition.
		i.getMatched().traverseBottomUp(this);
		Code.putJump(0);
		int fixAddr = Code.pc-2;
		
		// Refactor addresses to lead to else part of if statement.
		backpatch( top().refactorElsePart );
		
		// Generate FALSE part of the condition.
		i.getMatched1().traverseBottomUp(this);
		
		// Fix jump after TRUE part.
		Code.fixup(fixAddr);
	}
	
	public void visit( IfStart i )
	{		
		top().ifDepth++;
		top().allowCodeGeneration = false;
	}
	
	public void visit(UnmatchedIfElse i)
	{		
		top().ifDepth--;
		if ( shouldntGenerateCode() )
			return;
		
		generateAll(i);
		ConditionCheckerContext c = stack.pop();
		top().allowCodeGeneration = true;
	}
	
	public void visit(UnmatchedIf i)
	{	
		top().ifDepth--;
		if ( shouldntGenerateCode() )
			return;
		
		generateAll(i);
		ConditionCheckerContext c = stack.pop();
		top().allowCodeGeneration = true;
	}
	
	public void visit(MatchedIfElse i)
	{	
		top().ifDepth--;
		if ( shouldntGenerateCode() )
			return;
		
		generateAll(i);
		stack.pop();
		top().allowCodeGeneration = true;
	}
	
	public void visit(ForStatement f)
	{
		top().forDepth--;
		if ( shouldntGenerateCode() )
			return;		
				
		if ( top().forDepth == 0 && top().ifDepth == 0 )
		{
			ForStatementCG g = new ForStatementCG(cg);
			g.generate(f);
			top().allowCodeGeneration = true;
		};
	}
	
	public void visit(ForStatementStart s)
	{
		top().forDepth++;
	}
	
	public void visit(ForeachStatementStart start)
	{
		top().foreachDepth++;
	}
	
	public void visit(ForeachStatement st)
	{
		top().foreachDepth--;
		if ( shouldntGenerateCode() )
			return;
		
		ForeachStatementCG fcg = new ForeachStatementCG(cg);
		fcg.generate(st);
	}
	
	
	private boolean shouldntGenerateCode()
	{
		return top().forDepth > 0 || top().ifDepth > 0 || top().foreachDepth > 0;
	}
	
	/**
	 * ===============================================================================================================
	 * 		VISIT METHODS
	 * ===============================================================================================================
	 */
	
	public void visit(EqualExpr ee)
	{
		if ( shouldntGenerateCode() )
			return;
		
		ee.traverseBottomUp(cg);
	}
	
	public void visit(PlusPlusExpr ppe)
	{
		if ( shouldntGenerateCode() )
			return;
		
		ppe.traverseBottomUp(cg);
	}
	
	public void visit(MinusMinusExpr mme)
	{
		if ( shouldntGenerateCode() )
			return;
		
		mme.traverseBottomUp(cg);
	}
	
	public void visit(FuncCall fc)
	{
		if ( shouldntGenerateCode() )
			return;
		
		fc.traverseBottomUp(cg);
	}
	
	public void visit(ReadStatement rs)
	{
		if ( shouldntGenerateCode() )
			return;
		
		rs.traverseBottomUp(cg);
	}
	
	public void visit(PrintStatement ps)
	{
		if ( shouldntGenerateCode() )
			return;
		
		ps.traverseBottomUp(cg);
	}
	
	/**
	 * break; (*)
	 */
	public void visit(BreakStatement bs)
	{
		if ( shouldntGenerateCode() )
			return;
		
		Code.putJump( 0 );
		if ( top().forLoopContext != null )
			top().forLoopContext.refactorEndForLoop.add( new Integer(Code.pc-2) );
		else
			top().foreachLoopContext.refactorEndForLoop.add( new Integer(Code.pc - 2));
	}
	
	/**
	 * continue; (*)
	 */
	public void visit(ContinueStatement cs)
	{
		if ( shouldntGenerateCode() )
			return;
	
		Code.putJump( 0 );
		top().forLoopContext.refactorIterationExpression.add( new Integer(Code.pc-2) );
	}
	
	/**
	 * return <Expr>; (*)
	 */
	public void visit(ReturnExpr re)
	{
		if ( shouldntGenerateCode() )
			return;
		
		re.traverseBottomUp(cg);
	}
	
	/**
	 * ===============================================================================================================
	 * 		CONDITION CODE GENERATION
	 * ===============================================================================================================
	 */
	
	private void invertValue(boolean inverted)
	{
		if ( !inverted )
			return;
		
		int addrPatchLabInv0, addrPatchLabInv1, end;
		Code.load( Functions.constantInt(0) );
		Functions.generateJMP(Code.eq, false, 0);
		addrPatchLabInv0 = Code.pc - 2;
		Code.putJump(0);
		addrPatchLabInv1 = Code.pc - 2;
		Code.fixup(addrPatchLabInv0);
		Code.load( Functions.constantInt(1) );
		Code.putJump(0);
		end = Code.pc - 2;
		Code.fixup(addrPatchLabInv1);
		Code.load( Functions.constantInt(0) );
		Code.fixup(end);
	}
	
	public void visit(CondFact fact)
	{
		if ( shouldntGenerateCode() )
			return;
		
		if ( top().condFactNumber == 0 )
		{
			backpatch( top().refactorNextCondition );
			top().refreshNextConditionRefactorList();
			top().resetedNextCondition = true;
		};
			
		ExprNoError leftExpr = (ExprNoError) fact.getExpr();
		CondFactVolatile volatilePart = fact.getCondFactVolatile();
		Invertor inv = fact.getInvertor();
		boolean isInverted = inv instanceof InvertorPresent;
		
		if ( volatilePart instanceof CondFactIsVolatile )
		{
			ExprChecker ec = new ExprChecker(Functions.boolType);
			ec.process(leftExpr);			
			Obj val = ec.getContext().constReturn;
			
			// Caught constant boolean expression.
			if ( val != null )
			{
				int value = val.getAdr();
				if ( isInverted )
					value = value == 0 ? 1 : 0;
				
				// Load single variable
				Code.load( Functions.constantInt(value) );
				Code.load( Functions.constantInt(1) );
				
				// Generate equals relation operator.
				Functions.generateJMP(Code.eq, top().isInverse, 0);
				top().relopOperator = Code.eq;

			} else
			{
				TD_Expr e = new TD_Expr();
				e.createNewContext(CGExprContext.SIDE_EXPR);
				e.visit(leftExpr);
				invertValue(isInverted);
				
				// if ( boolVal == 1 ) ...
				Code.load( Functions.constantInt(1) );
				
				// Generate equals relation operator.
				Functions.generateJMP(Code.eq, top().isInverse, 0);
				top().relopOperator = Code.eq;
			};
		} else		// Relop Expr
		{
			Relop r = ((CondFactNotVolatile) volatilePart).getRelop();
			
			TD_Expr x = new TD_Expr();
			x.createNewContext(CGExprContext.SIDE_EXPR);
			x.visit(leftExpr);
			
			// Check first && second expression.
			ExprNoError rightExpr = (ExprNoError) ((CondFactNotVolatile) volatilePart).getExpr();
			x.createNewContext(CGExprContext.SIDE_EXPR);
			x.visit(rightExpr);
			
			// Generate relation operator.
			Functions.generateJMP(r, top().isInverse, 0);
			top().relopOperator = Functions.getCodeForRelop(r);
		};
		
		if ( top().condFactNumber == 0 )
		{
			top().addrStartExpr = Code.pc - 3;
			top().relopFirstOperator = top().relopOperator;
		};
		
		top().condFactNumber++;
	}
	
	public void visit( CondFactMoreNotPresent c )
	{
		if ( shouldntGenerateCode() )
			return;
		
		if ( c.getParent() instanceof CondTerm ) 
		{
			top().refactorIfPart.add( new Integer(Code.pc-2) );
			resetFact();
		} else
		{
			top().isInverse = true;
		};
	}
	
	public void visit(CondFactMorePresent c)
	{
		if ( shouldntGenerateCode() )
			return;
		
		top().isInverse = true;
		top().refactorNextCondition.add( new Integer(Code.pc-2) );
		
		if ( top().condFactNumber == 2 )
		{		
			// Correct operator for previous operand.
			int pc = Code.pc;
			Code.pc = top().addrStartExpr;
			Functions.generateJMP(top().relopFirstOperator, true, 0);
			top().refactorNextCondition.add( new Integer(Code.pc-2) );
			Code.pc = pc;
		};
	}
	
	public void visit(CondTerm ct)
	{
		if ( shouldntGenerateCode() )
			return;
		
		top().condTermNumber++;
		// APPENDED C PHASE
		if ( top().refactorNextCondition.size() > 0 )
			Functions.appendLists(top().refactorElsePart, top().refactorNextCondition);
		resetFact();
	}
	
	public void visit(Condition c)
	{
		if ( shouldntGenerateCode() )
			return;
		
		if ( top().condTermNumber == 1 )
		{
			Functions.appendLists( top().refactorElsePart , top().refactorNextCondition );			
		};
	}
	
	private void resetFact()
	{
		if ( top().condFactNumber > 1 )
		{
			Code.putJump(0);
			top().refactorIfPart.add( new Integer( Code.pc - 2 ) );
		};
		top().isInverse = false;
		top().condFactNumber = 0;
	}
	
	
}
