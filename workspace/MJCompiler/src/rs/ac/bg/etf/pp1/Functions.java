package rs.ac.bg.etf.pp1;

import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.clss.ClassDeclarationList;
import rs.ac.bg.etf.pp1.clss.ClassField;
import rs.ac.bg.etf.pp1.clss.ClassUtil;
import rs.ac.bg.etf.pp1.des.DesignatorFinder;
import rs.ac.bg.etf.pp1.gen.MethodCodeGeneration;
import rs.ac.bg.etf.pp1.gen.TD_Expr;
import rs.ac.bg.etf.pp1.util.CGExprContext;
import rs.ac.bg.etf.pp1.util.ComplexOperatorContext;

public class Functions {
	
	private static boolean forceTypeName = false;
	
	public static final boolean DEBUG = true;
	public static Obj boolObj = null;
	public static Struct boolType = new Struct( Struct.Bool );
	public static final int PLUS = 0,
							MINUS = 1,
							TIMES = 2,
							DIV = 3,
							MOD = 4;
	
	public static String getNameForType(int type)
	{
		switch( type )
		{
		case SemanticAnalyzer.BOOL:
			return "boolean";
			
		case SemanticAnalyzer.INT:
			return "int";
			
		case SemanticAnalyzer.CHAR:
			return "char";
		};
		
		return "--not-found--";
	}
	
	public static Struct getStructForExprConstDeclType( ExprConstDeclType e )
	{
		if ( e instanceof ENumberConst )
			return getStructRegular(Tab.intType, false);
		else if ( e instanceof ECharConst )
			return getStructRegular(Tab.charType, false);
		else if ( e instanceof EBoolConst )
			return getStructRegular(Functions.boolType, false);
		
		return Tab.noType;
	}
	
	public static Obj getObjForExprConstDeclType( ExprConstDeclType f )
	{
		String s = null;
		Obj retVal = null;
		
		if ( f instanceof EBoolConst )
		{
			s = ((EBoolConst) f).getVal();
			retVal = Functions.constantInt( new Integer(s.equals("false") ? 0 : 1) );
		} else if ( f instanceof ECharConst ) 
		{
			s = ((ECharConst) f).getVal();
			retVal = Functions.constantChar( s.charAt(1) );
		} else
			retVal = Functions.constantInt( ((ENumberConst)f).getVal() );
		
		return retVal;
	}
	
	public static String getNameForStruct(int type)
	{
		switch( type )
		{
		case Struct.Bool:
			return forceTypeName ? "bool" : "boolean";
			
		case Struct.Int:
			return "int";
			
		case Struct.Char:
			return "char";
		};
		
		return "void";
	}
	
	public static int getValueFromConstant(FactorConstDeclType fc)
	{
		ExprConstDeclType e = fc.getExprConstDeclType();
		if ( e instanceof ENumberConst )
			return ((ENumberConst) e).getVal();
		return 0;
	}
	
	public static Struct getStructForSymTable(int type, boolean isArray)
	{
		switch( type )
		{
		case SemanticAnalyzer.BOOL:
			return !isArray ? Functions.boolType : new Struct( Struct.Array, Functions.boolType );
		
		case SemanticAnalyzer.INT:
			return !isArray ? Tab.intType : new Struct( Struct.Array, Tab.intType );
			
		case SemanticAnalyzer.CHAR:
			return !isArray ? Tab.charType : new Struct( Struct.Array, Tab.charType );
		};
		
		return Tab.noType;
	}
	
	public static Struct getStructRegular( Struct t, boolean isArray )
	{
		switch( t.getKind() )
		{
		case Struct.Bool:
			return !isArray ? Functions.boolType : new Struct( Struct.Array, Functions.boolType );
		
		case Struct.Int:
			return !isArray ? Tab.intType : new Struct( Struct.Array, Tab.intType );
			
		case Struct.Char:
			return !isArray ? Tab.charType : new Struct( Struct.Array, Tab.charType );
			
		case Struct.Class:
			String clssname = ClassUtil.getClassNameFromStruct(t);
			Struct s = ClassDeclarationList.getInstance().getClassType(clssname);
			return !isArray ? s : new Struct( Struct.Array, s );
		};
		
		return Tab.noType;
	}
	
	public static int getStructIntValueForSymTable(int type)
	{
		switch(type)
		{
		case SemanticAnalyzer.BOOL:
			return Struct.None;
			
		case SemanticAnalyzer.INT:
			return Struct.Int;
			
		case SemanticAnalyzer.CHAR:
			return Struct.Char;
		};
		
		return Struct.None;
	}
	
	public static int getStructIntBasedOnTName(Type t)
	{
		if ( t instanceof TInteger )
			return Struct.Int;
		else if ( t instanceof TBoolean )			
			return Struct.Bool;
		else if ( t instanceof TChar )
			return Struct.Char;
		else if ( t instanceof TIdent )
		{
			String typeName = ((TIdent) t).getType();
			return Tab.find(typeName).getType().getKind();
		} else
			return -1;
	}
	
	public static String getAddopOperator(Addop a)
	{
		if ( a instanceof AddopLeftOperand )
		{
			AddopLeft d = ((AddopLeftOperand) a).getAddopLeft();
			if ( d instanceof AddopPlus )
				return "+";
			else if ( d instanceof AddopMinus )
				return "-";
		} else if ( a instanceof AddopRightOperand )
		{
			AddopRight d = ((AddopRightOperand) a).getAddopRight();
			if ( d instanceof AddopPlusEqual )
				return "+=";
			else if ( d instanceof AddopMinusEqual )
				return "-=";
		};
		
		return "";			
	}
	
	public static String getRelopOperator(Relop r)
	{
		if ( r instanceof REQ )
			return "==";
		else if ( r instanceof RNEQ )
			return "!=";
		else if ( r instanceof RGT )
			return ">";
		else if ( r instanceof RGTE )
			return ">=";
		else if ( r instanceof RLT )
			return "<";
		else if ( r instanceof RLTE )
			return "<=";
		
		return "";
	}
	
	public static String getMulopOperator(Mulop m)
	{
		if ( m instanceof MulopLeftOperand )
		{
			MulopLeft d = ((MulopLeftOperand) m).getMulopLeft();
			if ( d instanceof MulopTimes )
				return "*";
			else if ( d instanceof MulopDiv )
				return "/";
			else if ( d instanceof MulopMod )
				return "%";
		} else if ( m instanceof MulopRightOperand )
		{
			MulopRight d = ((MulopRightOperand) m).getMulopRight();
			if ( d instanceof MulopTimesEqual )
				return "*=";
			else if ( d instanceof MulopDivEqual )
				return "/=";
			else if ( d instanceof MulopModEqual )
				return "%=";
		};
		
		return "";			
	}
	
	/**
	 * Gets uncompatibile message for 2 types.
	 * 
	 * @param expected
	 * @param given
	 * @return Message for uncompatibile types.
	 */
	public static String getUncompatibileTypesMessage(Struct expected, Struct given)
	{
		StringBuilder str = new StringBuilder();		
		str.append("Expected type '");	
		str.append( getVariableTypeAsString(expected) );
		str.append("' is not compatibile with type '");		
		str.append( getVariableTypeAsString(given) );
		str.append("'");		
		
		return str.toString();
	}
	
	public static Struct getStructByStructKind(int kind)
	{
		return null;
	}
	
	public static double eval(final String str) {
	    return new Object() {
	        int pos = -1, ch;

	        void nextChar() {
	            ch = (++pos < str.length()) ? str.charAt(pos) : -1;
	        }

	        boolean eat(int charToEat) {
	            while (ch == ' ') nextChar();
	            if (ch == charToEat) {
	                nextChar();
	                return true;
	            }
	            return false;
	        }

	        double parse() {
	            nextChar();
	            double x = parseExpression();
	            if (pos < str.length()) throw new RuntimeException("Unexpected: " + (char)ch);
	            return x;
	        }

	        // Grammar:
	        // expression = term | expression `+` term | expression `-` term
	        // term = factor | term `*` factor | term `/` factor | term `%` factor
	        // factor = `+` factor | `-` factor | `(` expression `)`
	        //        | number | functionName factor | factor `^` factor

	        double parseExpression() {
	            double x = parseTerm();
	            for (;;) {
	                if      (eat('+')) x += parseTerm(); // addition
	                else if (eat('-')) x -= parseTerm(); // subtraction
	                else return x;
	            }
	        }

	        double parseTerm() {
	            double x = parseFactor();
	            for (;;) {
	                if      (eat('*')) x *= parseFactor(); // multiplication
	                else if (eat('/')) x /= parseFactor(); // division
	                else if (eat('%')) x = x % parseFactor(); // modulus
	                else return x;
	            }
	        }

	        double parseFactor() {
	            if (eat('+')) return parseFactor(); // unary plus
	            if (eat('-')) return -parseFactor(); // unary minus

	            double x;
	            int startPos = this.pos;
	            if (eat('(')) { // parentheses
	                x = parseExpression();
	                eat(')');
	            } else if ((ch >= '0' && ch <= '9') || ch == '.') { // numbers
	                while ((ch >= '0' && ch <= '9') || ch == '.') nextChar();
	                x = Double.parseDouble(str.substring(startPos, this.pos));
	            } else if (ch >= 'a' && ch <= 'z') { // functions
	                while (ch >= 'a' && ch <= 'z') nextChar();
	                String func = str.substring(startPos, this.pos);
	                x = parseFactor();
	                if (func.equals("sqrt")) x = Math.sqrt(x);
	                else if (func.equals("sin")) x = Math.sin(Math.toRadians(x));
	                else if (func.equals("cos")) x = Math.cos(Math.toRadians(x));
	                else if (func.equals("tan")) x = Math.tan(Math.toRadians(x));
	                else throw new RuntimeException("Unknown function: " + func);
	            } else {
	                throw new RuntimeException("Unexpected: " + (char)ch);
	            }

	            if (eat('^')) x = Math.pow(x, parseFactor()); // exponentiation

	            return x;
	        }
	    }.parse();
	}
	
	public static Obj localVariable(int localNum)
	{
		Obj o = new Obj(Obj.Var, "$", Tab.nullType);
		o.setAdr(localNum);
		return o;
	}
	
	public static Obj thisVariable()
	{
		Obj o = new Obj(Obj.Var, "this", Tab.nullType);
		o.setAdr(0);
		return o;
	}
	
	public static Obj constantInt(int value)
	{
		Obj o = new Obj(Obj.Con, "$", Tab.intType);
		o.setAdr(value);
		return o;
	}
	
	public static Obj constantChar(char value)
	{
		Obj o = new Obj(Obj.Con, "$", Tab.charType);
		o.setAdr(value);
		return o;
	}
	
	public static Obj constantElem(Struct elemType)
	{
		Obj o = new Obj(Obj.Elem, "$", elemType);
		return o;
	}
	
	public static void fillWithBlanks(char[] ref, int width)
	{
		if ( width > ref.length )
		{
			int gotof = width-ref.length;
			for (int i=0; i<gotof; i++)
			{				
				Code.load(Functions.constantChar(' '));
				Code.load(Functions.constantInt(0));
				Code.put(Code.bprint);
			};
		};
	}
	
	public static Struct getTypeBySubstitution(Type t)
	{
		if ( t instanceof TInteger )
			return Tab.intType;
		else if ( t instanceof TChar )
			return Tab.charType;
		else if ( t instanceof TBoolean )
			return boolType;
		else if ( t instanceof TIdent )
		{
			// Get type from symbol table.
			Obj f = Tab.find(((TIdent) t).getType());
			if ( f != Tab.noObj && f.getKind() == Obj.Type )
				return f.getType();			
		};
		
		return Tab.noType;
	}
	
	public static void functionCallCheck( SemanticAnalyzer sa, Obj method, SyntaxNode snode )
	{		
		if ( method == null || method == Tab.noObj )
			sa.report_error("Method '" + method.getName() + "' is not defined. Trying to invoke method", snode);
		else
		{
			// Check if symbol is function (method).
			if ( method.getKind() != Obj.Meth )
				sa.report_error("Global method with name '" + method.getName() + "' is not defined! Error", snode);
			else
			{				
				if ( snode instanceof FactorDesignator )
				{
					String desPrinted = DesignatorFinder.printDesignator(((FactorDesignator) snode).getDesignator(), null);
					FactorDesignator fd = (FactorDesignator) snode;
					sa.report_info(
							"Detected method invocation: " + desPrinted + "(" + 
							printFormalParametersForMethod( desPrinted ) + ") inside expression on right side"
							, fd);
				} else if ( snode instanceof FuncCall )
				{
					FuncCall fc = (FuncCall) snode;						
					
					sa.report_info(
							"Detected method invocation: " + method.getName() + "(" + 
							printFormalParametersForMethod(method) + ")"
							, fc);
				};				
				//System.out.println("Perform check on method call");
			};
		};
	}
	
	public static String getVariableAccessRight(AccessRights ar)
	{
		if ( ar instanceof AccessPrivate )
			return "private";
		else if ( ar instanceof AccessProtected )
			return "protected";
		else if ( ar instanceof AccessPublic )
			return "public";
		return "--err";		
	}
	
	public static String getAccessRight(int ar)
	{
		if ( ar == ClassField.PRIVATE )
			return "private";
		else if ( ar == ClassField.PROTECTED )
			return "protected";
		else if ( ar == ClassField.PUBLIC )
			return "public";
		return "--err";		
	}
	
	public static int getAccessRightBySubstitution( AccessRights ar )
	{
		if ( ar instanceof AccessPrivate )
			return ClassField.PRIVATE;
		else if ( ar instanceof AccessProtected )
			return ClassField.PROTECTED;
		else if ( ar instanceof AccessPublic )
			return ClassField.PUBLIC;
		return ClassField.PUBLIC;	
	}
	
	public static void functionCallGenerateCode( MethodCodeGeneration mcg, SyntaxNode snode, ActParsWrapper params )
	{
		Obj entry = Tab.noObj;
		boolean classMethod = false;
		DesignatorFinder finder = null;
		
		if ( snode instanceof FuncCall )
		{
			finder = new DesignatorFinder(null);
			finder.setCheck(false);
			finder.traverse( ((FuncCall) snode).getFuncCallStart().getDesignator() );
			entry =	finder.getObject();
			if ( finder.isClassReference() )
				classMethod = true;		
			
		} else if ( snode instanceof FactorDesignator )
		{
			FactorDesignator d = (FactorDesignator) snode;	
			
			finder = new DesignatorFinder(null);
			finder.setCheck(false);
			finder.traverse( d.getDesignator() );
			entry = finder.getObject();
			classMethod = finder.isClassReference();	
		};
		
		/*	
		
		// Generate parameters.
		mcg.setGenerationOfActualParameter(true);
		params.traverseBottomUp(mcg);
		mcg.setGenerationOfActualParameter(false);
		*/
		
		if ( entry != Tab.noObj )
		{
			if ( !classMethod )
			{
				int dest_adr=entry.getAdr()-Code.pc; 
				Code.put( Code.call );
				Code.put2( dest_adr );
			} else
			{			
				String name = entry.getName();
				char c;
				
				// GET POINTER TO VIRTUAL TABLE
				// Duplicate object's reference in order to get pointer to virtual table.
				finder.generateThroughTheList(false);				
				Code.put( Code.getfield );
				Code.put2(0);
				
				Code.put( Code.invokevirtual );
				
				for (int i=0, n=name.length(); i<n; i++)
				{
					c = name.charAt(i);
					Code.put4( c );
				};
				Code.put4(-1);
			}
		} else
		{
			System.out.println("CG: Method not found or returns void. Cannot generate code.");
		};
	}
	
	public static String printFormalParametersForMethod( String methodName )
	{		
		Obj method = Tab.find( methodName );
		return printFormalParametersForMethod(method);
	}
	
	public static String printFormalParametersForMethod( Obj method )
	{
		StringBuilder str = new StringBuilder();
		Collection<Obj> s = method.getLocalSymbols();
		Iterator<Obj> it = s.iterator();
		int index = 0, len = method.getLevel();
		boolean prevForce = forceTypeName;
		forceTypeName = true;
		SemanticAnalyzer sa = SemanticAnalyzer.__instance__();
		boolean omitted = false;
		int cnt = 0;
		
		while(it.hasNext() && index < len)
		{
			Obj param = it.next();
			if ( !omitted && ( sa.classIsInside || sa.exprDesignator != null && sa.exprDesignator.isClassReference() ) )
			{
				index++;
				omitted = true;
				continue;
			};
			
			if ( cnt > 0 )
				str.append(",");
			
			str.append( getVariableTypeAsString( param ) );
			
			index++;
			cnt++;
		};
		forceTypeName = prevForce;
		
		return str.toString();
	}
	
	public static void print(Struct type, int width)
	{
		switch( type.getKind() )
		{
		// Print integer.
		case Struct.Int:
			Code.load( constantInt(width) );
			Code.put(Code.print);
			break;
			
		// Print boolean.
		case Struct.Bool:
			char[] ref = null;
			final char[] trueSt = { 't', 'r', 'u', 'e' };
			final char[] falseSt = { 'f', 'a', 'l', 's', 'e' };
			
			Code.load( constantInt(0) );
			Code.putFalseJump(Code.eq, 0);
			int patchAddr = Code.pc - 2;
			
			// Put code for boolVar == false.
			ref = falseSt;
			fillWithBlanks(ref, width);
			for (int i=0; i<ref.length; i++)
			{				
				Code.load( constantChar(ref[i]) );
				Code.load( constantInt(0) );
				Code.put(Code.bprint);
			};
			Code.putJump(0);
			int patchAddrExitIf = Code.pc - 2;
			Code.fixup(patchAddr);
			
			// Put code for boolVar == true.
			ref = trueSt;
			fillWithBlanks(ref, width);
			for (int i=0; i<ref.length; i++)
			{				
				Code.load( constantChar(ref[i]) );
				Code.load( constantInt(0) );
				Code.put(Code.bprint);
			};
			Code.fixup(patchAddrExitIf);
			break;
			
		// Print char.
		case Struct.Char:
			Code.load( constantInt(width) );
			Code.put( Code.bprint );
			break;
			
		// Print array's element.
		case Struct.Array:
			Struct elemType = type.getElemType();
			print( elemType, width );
			break;
		};
	}
	
	public static String getPluralForWord(String w, int value)
	{
		switch(w)
		{
		case "params":
			switch(value)
			{
			case 1:
				return "parameter";
				
			default:
				return "parameters";
			}
		
		default:
		};
		
		return "";
	}
	
	public static boolean isArray( Obj obj )
	{
		return obj.getKind() == Obj.Var && obj.getType().getKind() == Struct.Array;
	}
	
	public static String getVariableTypeAsString( Obj variable )
	{
		return getVariableTypeAsString( variable.getType() );
		
	}
	
	public static String getVariableTypeAsString( Struct struct )
	{
		StringBuilder str = new StringBuilder();
		int kind = -1;
		String className = null;
		boolean isArray = struct.getKind() == Struct.Array;
		boolean isClass = struct.getKind() == Struct.Class;
		
		if ( isArray )
		{
			if ( struct.getElemType().getKind() == Struct.Class )
			{
				className = ClassUtil.getClassNameFromStruct( struct.getElemType() );
				isClass = true;
			} else				
				kind = struct.getKind();
			
		} else if ( isClass )
		{
			className = ClassUtil.getClassNameFromStruct( struct );
		} else
			kind = struct.getKind();
			
		//kind = isArray ? struct.getElemType().getKind() : struct.getKind();
		if ( !isClass )
			str.append( getNameForStruct( kind ) );
		else
			str.append( className );
		
		if ( isArray )
			str.append("[]");
		
		return str.toString();
	}
	
	public static String getMethodSignature(Obj method)
	{
		String methodName = method.getName();
		StringBuilder str = new StringBuilder();
		forceTypeName = true;
		str.append( Functions.getVariableTypeAsString(method) );
		str.append( " " );
		str.append( method.getName() );
		str.append( "(" );
		forceTypeName = false;
		str.append( Functions.printFormalParametersForMethod(methodName) );
		str.append( ")" );
		return str.toString();
	}
	
	public static void addBuiltInMethods()
	{
		// Add method 
		// ========================================
		// 		char chr(int i);
		//		int ord(char c);
		// ========================================
		Obj chr = Tab.find( "chr" );
		Obj ord = Tab.find( "ord" );
		Obj len = Tab.find( "len" );	
		chr.setAdr( Code.pc );
		ord.setAdr( Code.pc );
		
		Code.put(Code.enter);
		Code.put(1);							// Parameters size.
		Code.put(0);							// Local variables of this method.
		
		Code.put( Code.load_n + 0 );			// Return value chr(i).
		
		Code.put( Code.exit );
		Code.put( Code.return_ );
		
		// Add method 
		// ========================================
		// 		int len(<Type> arr[]);
		// ========================================
		len.setAdr( Code.pc );
		
		Code.put(Code.enter);
		Code.put(1);							// Parameters size.
		Code.put(0);							// Local variables of this method.
		
		Code.put( Code.load_n + 0 );
		Code.put( Code.arraylength );			// Put length of the array on expression stack. 
		
		Code.put( Code.exit );
		Code.put( Code.return_ );
	}
	
	/*
	public static void generateIncDecForArray(Obj entry, DesignatorOptExpr doe, boolean increment)
	{
		Struct s = entry.getType();
		Obj elemType = constantElem( s.getElemType() );
		
		if ( doe instanceof DesignatorOpt )
		{
			// Load address.
			Code.load( entry );
			
			// Load index.
			TD_Expr e = new TD_Expr();
			e.createNewContext(CGExprContext.SIDE_EXPR);
			e.visit( (ExprNoError) ((DesignatorOpt) doe).getExpr() );
			
			// Duplicate array's address & index for manipulation.
			Code.put( Code.dup2 );
			
			// Load element on expression stack.
			Code.load( elemType );
			
			// Increment element.
			Code.load( constantInt(1) );
			Code.put( increment ? Code.add : Code.sub );
			
			// Store element.
			Code.store( elemType );				
		};
	}
	*/
	
	public static void generateMulopOperator(Mulop m)
	{
		if ( m instanceof MulopLeftOperand )
		{
			MulopLeft d = ((MulopLeftOperand) m).getMulopLeft();
			generateMulopLeft(d);
		} else if ( m instanceof MulopRightOperand )
		{
			MulopRight d = ((MulopRightOperand) m).getMulopRight();
			generateMulopRight(d);
		};
	}
	
	public static void generateAddopOperator(Addop a)
	{
		if ( a instanceof AddopLeftOperand )
		{
			AddopLeft d = ((AddopLeftOperand) a).getAddopLeft();
			generateAddopLeft( d );
		} else if ( a instanceof AddopRightOperand )
		{
			AddopRight d = ((AddopRightOperand) a).getAddopRight();
			generateAddopRight(d);
		};
	}
	
	public static void generateOperator(Assignop a)
	{
		if ( a instanceof AssignopAddop )
		{
			AssignopAddop f = (AssignopAddop) a;
			generateAddopRight( f.getAddopRight() );
		} else if ( a instanceof AssignopMulop )
		{
			AssignopMulop m = (AssignopMulop) a;
			generateMulopRight( m.getMulopRight() );
		};
	}
	
	public static boolean isComplexAddopOperator(Addop a)
	{
		return a instanceof AddopRightOperand;
	}
	
	public static boolean isComplexMulopOperator(Mulop m)
	{
		return m instanceof MulopRightOperand;
	}
	
	public static void generateAddopLeft( AddopLeft d )
	{
		if ( d instanceof AddopPlus )
			Code.put(Code.add);
		else
			Code.put(Code.sub);
	}
	
	public static void generateAddopRight( AddopRight d )
	{
		if ( d instanceof AddopPlusEqual )
			Code.put(Code.add);
		else
			Code.put(Code.sub);
	}
	
	public static void generateMulopLeft( MulopLeft d )
	{
		if ( d instanceof MulopTimes )
			Code.put(Code.mul);
		
		else if ( d instanceof MulopDiv )
			Code.put(Code.div);
		
		else if ( d instanceof MulopMod )
			Code.put(Code.rem);
	}
	
	public static void generateMulopRight( MulopRight d )
	{
		if ( d instanceof MulopTimesEqual )
			Code.put(Code.mul);
		
		else if ( d instanceof MulopDivEqual )
			Code.put(Code.div);
		
		else if ( d instanceof MulopModEqual )
			Code.put(Code.rem);
	}
	
	public static void generateAssignOperator(Assignop a)
	{
		generateAssignOperator( getOperatorType(a) );	
	}
	
	public static void generateComplexOperator(ComplexOperatorContext context)
	{
		switch( context.entry.getKind() )
		{
		case Obj.Var:
			switch( context.entry.getType().getKind() )
			{
			case Struct.Int:
				Code.load( context.entry );				
				
				Factor f = (Factor) context.snode;
				TD_Expr er = new TD_Expr();
				er.createNewContext(CGExprContext.SIDE_EXPR);
				er.getContext().isComplexOperator = true;
				er.getContext().complexContext = context;
				try {
					er.visit(f);
				} catch( Exception e ) {}
				
				/*
				if ( context.isMulop )
					generateMulopOperator(context.mulop);
				else if ( context.isAddop )
					generateAddopOperator(context.addop);	
					*/			
				
				// Duplicate stack's value.
				Code.put( Code.dup );
				
				// Store variable.
				Code.store( context.entry );
				
				// POP value from stack.
				if ( context.depth == 0 )
					Code.put( Code.pop );
				break;
			};
			break;
			
		case Obj.Fld:
			break;
		}
	}
	
	public static int getOperatorType( Assignop a )
	{
		int v = -1;
		
		if ( isAddop(a) )
		{
			AddopRight ar = ((AssignopAddop)a).getAddopRight();
			if ( ar instanceof AddopPlusEqual )
				v = PLUS;
			else 
				v = MINUS;
		} else if ( isMulop(a) )
		{
			MulopRight mr = ((AssignopMulop)a).getMulopRight();
			if ( mr instanceof MulopTimesEqual )
				v = TIMES;
			else if ( mr instanceof MulopDivEqual )
				v = DIV;
			else if ( mr instanceof MulopModEqual )
				v = MOD;
		};
		return v;	
	}
	
	public static void generateAssignOperator(int type)
	{
		switch( type )
		{
		case PLUS:
			Code.put( Code.add );
			break;
			
		case MINUS:
			Code.put( Code.sub );
			break;
			
		case TIMES:
			Code.put( Code.mul );
			break;
			
		case MOD:
			Code.put( Code.rem );
			break;
		}
	}
	
	public static boolean isAddop(Assignop a)
	{
		return a instanceof AssignopAddop;
	}
	
	public static boolean isMulop(Assignop a)
	{
		return a instanceof AssignopMulop;
	}
	
	public static void storeVariable(Obj entry, boolean isArrayAccess)
	{
		// Store variable.
		switch( entry.getKind() )
		{
		case Obj.Var:
			switch( entry.getType().getKind() )
			{		
			case Struct.Array:
				if ( !isArrayAccess )
					Code.store( entry );
				else
					Code.store( Functions.constantElem( entry.getType().getElemType() ) );
				break;
				
			case Struct.Class:		
				if ( isArrayAccess )
					Code.store( Functions.constantElem(Tab.intType) );
				else
					Code.store( entry );
				break;
				
			case Struct.Char:
			case Struct.Int:
			case Struct.Bool:			
				if ( isArrayAccess )
					Code.store( Functions.constantElem(entry.getType()) );
				else
					Code.store( entry );
				break;
			};
			break;
			
		case Obj.Fld:
			Code.store( entry );			
			break;
		};
	}
	
	public static void generateJMP(Relop r, boolean inverse, int addr)
	{
		if ( r instanceof REQ )
			generateJMP(Code.eq, inverse, addr);
		else if ( r instanceof RNEQ )
			generateJMP(Code.ne, inverse, addr);
		else if ( r instanceof RGT )
			generateJMP(Code.gt, inverse, addr);
		else if ( r instanceof RGTE  )
			generateJMP(Code.ge, inverse, addr);
		else if ( r instanceof RLT )
			generateJMP(Code.lt, inverse, addr);
		else if ( r instanceof RLTE )
			generateJMP(Code.le, inverse, addr);	
	}
	
	public static int getCodeForRelop(Relop r)
	{
		if ( r instanceof REQ )
			return Code.eq;
		else if ( r instanceof RNEQ )
			return Code.ne;
		else if ( r instanceof RGT )
			return Code.gt;
		else if ( r instanceof RGTE  )
			return Code.ge;
		else if ( r instanceof RLT )
			return Code.lt;
		else if ( r instanceof RLTE )
			return Code.le;
		return -1;
	}
	
	public static void generateJMP( int addr )
	{
		Code.put( Code.jmp );
		Code.put2( Code.pc - addr );
	}
	
	public static void generateJMP(int code, boolean inverse, int addr)
	{
		if ( inverse )
			Code.putFalseJump(code, addr);
		else
		{
			Code.put( Code.jcc + code );
			Code.put2(addr);
		};
	}
	
	public static<T> void appendLists(LinkedList<T> l1, LinkedList<T> l2)
	{
		Iterator<T> it = l2.iterator();
		while( it.hasNext() )
		{
			T g = it.next();
			l1.add(g);
		};
	}
	
	public static void addStaticData( int position, int value )
	{
		Code.put( Code.const_ );
		Code.put4( value );
		
		Code.put( Code.putstatic );
		Code.put2( position );
	}
	
}
