package rs.ac.bg.etf.pp1.util;

import java.util.HashMap;

import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

/**
 * List of all scopes.
 * 
 * @author ol150390d
 *
 */
public class Scopes 
{	
	private static HashMap<String, Scope> map;
	
	static
	{
		map = new HashMap<String, Scope>();
	}
	
	public static boolean put(String name, Scope scope)
	{
		if ( map.containsKey(name) )
			return false;
		else
		{
			map.put(name, scope);
			return true;
		}
	}
	
	public static Scope get(String name)
	{
		return map.get(name);
	}
	
	public static void changeSymbolTableScope(String name)
	{
		Tab.currentScope = Scopes.get(name);
	}
}
