// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class MethodDecl implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    public rs.etf.pp1.symboltable.concepts.Obj obj = null;

    private MethodDeclStart MethodDeclStart;
    private FormParsWrapper FormParsWrapper;
    private MethodDeclFormParsEnd MethodDeclFormParsEnd;
    private MethodVarDecl MethodVarDecl;
    private StatementList StatementList;

    public MethodDecl (MethodDeclStart MethodDeclStart, FormParsWrapper FormParsWrapper, MethodDeclFormParsEnd MethodDeclFormParsEnd, MethodVarDecl MethodVarDecl, StatementList StatementList) {
        this.MethodDeclStart=MethodDeclStart;
        if(MethodDeclStart!=null) MethodDeclStart.setParent(this);
        this.FormParsWrapper=FormParsWrapper;
        if(FormParsWrapper!=null) FormParsWrapper.setParent(this);
        this.MethodDeclFormParsEnd=MethodDeclFormParsEnd;
        if(MethodDeclFormParsEnd!=null) MethodDeclFormParsEnd.setParent(this);
        this.MethodVarDecl=MethodVarDecl;
        if(MethodVarDecl!=null) MethodVarDecl.setParent(this);
        this.StatementList=StatementList;
        if(StatementList!=null) StatementList.setParent(this);
    }

    public MethodDeclStart getMethodDeclStart() {
        return MethodDeclStart;
    }

    public void setMethodDeclStart(MethodDeclStart MethodDeclStart) {
        this.MethodDeclStart=MethodDeclStart;
    }

    public FormParsWrapper getFormParsWrapper() {
        return FormParsWrapper;
    }

    public void setFormParsWrapper(FormParsWrapper FormParsWrapper) {
        this.FormParsWrapper=FormParsWrapper;
    }

    public MethodDeclFormParsEnd getMethodDeclFormParsEnd() {
        return MethodDeclFormParsEnd;
    }

    public void setMethodDeclFormParsEnd(MethodDeclFormParsEnd MethodDeclFormParsEnd) {
        this.MethodDeclFormParsEnd=MethodDeclFormParsEnd;
    }

    public MethodVarDecl getMethodVarDecl() {
        return MethodVarDecl;
    }

    public void setMethodVarDecl(MethodVarDecl MethodVarDecl) {
        this.MethodVarDecl=MethodVarDecl;
    }

    public StatementList getStatementList() {
        return StatementList;
    }

    public void setStatementList(StatementList StatementList) {
        this.StatementList=StatementList;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(MethodDeclStart!=null) MethodDeclStart.accept(visitor);
        if(FormParsWrapper!=null) FormParsWrapper.accept(visitor);
        if(MethodDeclFormParsEnd!=null) MethodDeclFormParsEnd.accept(visitor);
        if(MethodVarDecl!=null) MethodVarDecl.accept(visitor);
        if(StatementList!=null) StatementList.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(MethodDeclStart!=null) MethodDeclStart.traverseTopDown(visitor);
        if(FormParsWrapper!=null) FormParsWrapper.traverseTopDown(visitor);
        if(MethodDeclFormParsEnd!=null) MethodDeclFormParsEnd.traverseTopDown(visitor);
        if(MethodVarDecl!=null) MethodVarDecl.traverseTopDown(visitor);
        if(StatementList!=null) StatementList.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(MethodDeclStart!=null) MethodDeclStart.traverseBottomUp(visitor);
        if(FormParsWrapper!=null) FormParsWrapper.traverseBottomUp(visitor);
        if(MethodDeclFormParsEnd!=null) MethodDeclFormParsEnd.traverseBottomUp(visitor);
        if(MethodVarDecl!=null) MethodVarDecl.traverseBottomUp(visitor);
        if(StatementList!=null) StatementList.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("MethodDecl(\n");

        if(MethodDeclStart!=null)
            buffer.append(MethodDeclStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(FormParsWrapper!=null)
            buffer.append(FormParsWrapper.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodDeclFormParsEnd!=null)
            buffer.append(MethodDeclFormParsEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MethodVarDecl!=null)
            buffer.append(MethodVarDecl.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(StatementList!=null)
            buffer.append(StatementList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [MethodDecl]");
        return buffer.toString();
    }
}
