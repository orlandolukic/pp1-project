// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class AbstractClassDeclNoError extends AbstractClassDecl {

    private AbstractClassDeclStart AbstractClassDeclStart;
    private VarDeclList VarDeclList;
    private AbstractClassDeclMethodsWrapper AbstractClassDeclMethodsWrapper;

    public AbstractClassDeclNoError (AbstractClassDeclStart AbstractClassDeclStart, VarDeclList VarDeclList, AbstractClassDeclMethodsWrapper AbstractClassDeclMethodsWrapper) {
        this.AbstractClassDeclStart=AbstractClassDeclStart;
        if(AbstractClassDeclStart!=null) AbstractClassDeclStart.setParent(this);
        this.VarDeclList=VarDeclList;
        if(VarDeclList!=null) VarDeclList.setParent(this);
        this.AbstractClassDeclMethodsWrapper=AbstractClassDeclMethodsWrapper;
        if(AbstractClassDeclMethodsWrapper!=null) AbstractClassDeclMethodsWrapper.setParent(this);
    }

    public AbstractClassDeclStart getAbstractClassDeclStart() {
        return AbstractClassDeclStart;
    }

    public void setAbstractClassDeclStart(AbstractClassDeclStart AbstractClassDeclStart) {
        this.AbstractClassDeclStart=AbstractClassDeclStart;
    }

    public VarDeclList getVarDeclList() {
        return VarDeclList;
    }

    public void setVarDeclList(VarDeclList VarDeclList) {
        this.VarDeclList=VarDeclList;
    }

    public AbstractClassDeclMethodsWrapper getAbstractClassDeclMethodsWrapper() {
        return AbstractClassDeclMethodsWrapper;
    }

    public void setAbstractClassDeclMethodsWrapper(AbstractClassDeclMethodsWrapper AbstractClassDeclMethodsWrapper) {
        this.AbstractClassDeclMethodsWrapper=AbstractClassDeclMethodsWrapper;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(AbstractClassDeclStart!=null) AbstractClassDeclStart.accept(visitor);
        if(VarDeclList!=null) VarDeclList.accept(visitor);
        if(AbstractClassDeclMethodsWrapper!=null) AbstractClassDeclMethodsWrapper.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(AbstractClassDeclStart!=null) AbstractClassDeclStart.traverseTopDown(visitor);
        if(VarDeclList!=null) VarDeclList.traverseTopDown(visitor);
        if(AbstractClassDeclMethodsWrapper!=null) AbstractClassDeclMethodsWrapper.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(AbstractClassDeclStart!=null) AbstractClassDeclStart.traverseBottomUp(visitor);
        if(VarDeclList!=null) VarDeclList.traverseBottomUp(visitor);
        if(AbstractClassDeclMethodsWrapper!=null) AbstractClassDeclMethodsWrapper.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("AbstractClassDeclNoError(\n");

        if(AbstractClassDeclStart!=null)
            buffer.append(AbstractClassDeclStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarDeclList!=null)
            buffer.append(VarDeclList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(AbstractClassDeclMethodsWrapper!=null)
            buffer.append(AbstractClassDeclMethodsWrapper.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [AbstractClassDeclNoError]");
        return buffer.toString();
    }
}
