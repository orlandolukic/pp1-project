// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class NumberConst extends ConstDeclType {

    private NumerusSign NumerusSign;
    private int val;

    public NumberConst (NumerusSign NumerusSign, int val) {
        this.NumerusSign=NumerusSign;
        if(NumerusSign!=null) NumerusSign.setParent(this);
        this.val=val;
    }

    public NumerusSign getNumerusSign() {
        return NumerusSign;
    }

    public void setNumerusSign(NumerusSign NumerusSign) {
        this.NumerusSign=NumerusSign;
    }

    public int getVal() {
        return val;
    }

    public void setVal(int val) {
        this.val=val;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(NumerusSign!=null) NumerusSign.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(NumerusSign!=null) NumerusSign.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(NumerusSign!=null) NumerusSign.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("NumberConst(\n");

        if(NumerusSign!=null)
            buffer.append(NumerusSign.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(" "+tab+val);
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [NumberConst]");
        return buffer.toString();
    }
}
