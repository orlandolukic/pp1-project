package rs.ac.bg.etf.pp1.des;

import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.checkers.ForeachLoopChecker;
import rs.ac.bg.etf.pp1.context.ForeachLoopCheckerContext;
import rs.ac.bg.etf.pp1.util.Stack;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class DesignatorForeachLoop extends DesignatorFinder {

	private int check = 0;
	private String mssg;
	private Obj validObj;
	private Stack<ForeachLoopCheckerContext> stack;
	private ForeachLoopChecker checker;
	
	public DesignatorForeachLoop(SemanticAnalyzer sa, Stack<ForeachLoopCheckerContext> stack, ForeachLoopChecker checker) {
		super(sa);
		this.stack = stack;
		this.checker = checker;
	}
	
	public void setMssg(String mssg)
	{
		this.mssg = mssg;
	}
	
	public void setCheckType(int type)
	{
		check = type;
	}
	
	public void setValidObj(Obj o)
	{
		validObj = o;
	}
	
	@Override 
	protected void designatorFinished()
	{
		if ( check == 0 )
		{
			ForeachLoopCheckerContext cit;
			Obj o = designatorContext.current;
			if ( o.getType().getKind() == Struct.Array )
				return;
		
			stack.iteratorStart();
			while( stack.hasNext() )
			{
				cit = stack.next();
				if ( o.equals( cit.ident ) )
				{
					sa.report_error("Iteration variable '" + o.getName() + "' could not be modified", designatorContext.getNode());
					break;
				};
			};
		} else if ( check == 1 )
		{
			if ( designatorContext.current.getType().getKind() != Struct.Array )
				sa.report_error("Variable '" + print(null) + "' used for iteration needs to be an array", designatorContext.getNode());
			else
			{
				// Check compatibility with identifier.
				if ( !designatorContext.current.getType().getElemType().compatibleWith( validObj.getType() ) )
					sa.report_error( Functions.getUncompatibileTypesMessage(designatorContext.current.getType().getElemType(), validObj.getType()) , node());
				else
				{
					checker.top().ident = validObj;
					sa.report_info("FOREACH loop detected", node());
				};
			};
		}
	}
	
	@Override
	protected String undefinedVariableMessage()
	{
		if ( mssg != null )
			return mssg;
		
		return "Variable '" + designatorContext.current.getName() + "' cannot be used in foreach statement because it's not defined";
	}
	

}
