package rs.ac.bg.etf.pp1;

import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.clss.ClassUtil;
import rs.ac.bg.etf.pp1.context.MethodInvocationContext;
import rs.ac.bg.etf.pp1.des.DesignatorCGIncDec;
import rs.ac.bg.etf.pp1.des.DesignatorEqualExpr;
import rs.ac.bg.etf.pp1.des.DesignatorFinder;
import rs.ac.bg.etf.pp1.des.DesignatorReadCheck;
import rs.ac.bg.etf.pp1.gen.*;
import rs.ac.bg.etf.pp1.util.*;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class CodeGenerator extends VisitorAdaptor {
	
	private static CodeGenerator INSTANCE;
	public static CodeGenerator __INSTANCE__()
	{
		return INSTANCE;
	}
	
	public boolean ignoreMethodGeneration;
	public int complexOperatorDepth = 0;
	public int definedMethods = 0;
	public boolean isPureFuncCall;
	public int nestedFuncCall;
	public int nestedFuncCallMax;
	public boolean isForLoop;
	public boolean isIfStatement;
	public int forLoopCnt;
	public int ifStatementCnt;
	public Stack<MethodInvocationContext> stackFunctionCall;
	public int mainPc;
	public boolean generateIfAndFor;
	public int foreachLoopCnt;
	public int refactorMainPcJump;
	public boolean generateLeft;
	
	public boolean classIsInside;
	public rs.ac.bg.etf.pp1.clss.ClassDeclaration classInstance;
	
	public CodeGenerator()
	{
		INSTANCE = this;
		this.isForLoop = false;
		this.isIfStatement = false;
		forLoopCnt = 0;
		ifStatementCnt = 0;
		this.nestedFuncCall = 0;
		this.ignoreMethodGeneration = false;
		generateIfAndFor = false;
		this.stackFunctionCall = new Stack<MethodInvocationContext>();
	}
	
	public void incComplexOperatorDepth(int depth)
	{
		if ( (this.complexOperatorDepth + depth) >= 0 )
			this.complexOperatorDepth += depth;
	}
	
	public int getComplexOperatorDepth()
	{
		return this.complexOperatorDepth;
	}
	
	public boolean popFromExpStack()
	{
		return this.complexOperatorDepth == 0;
	}
	
	public int getMainPC()
	{
		return this.mainPc;
	}
	
	private boolean shouldntGenerateCode()
	{
		return forLoopCnt > 0 || ifStatementCnt > 0 || foreachLoopCnt > 0;
	}
	
	/**
	 * End of the program.
	 */
	public void visit( Program p )
	{

	}
	
	public void visit(ProgName progName)
	{
		Scopes.changeSymbolTableScope( progName.getProgname() );
	}
	
	public void visit( MethodDeclStart methodDeclStart )
	{
		if ( definedMethods == 0 )
			Functions.addBuiltInMethods();
		
		String methodName = methodDeclStart.getMethodName();
		if ( methodName.equals("main") )
		{
			mainPc = Code.pc;
			int pc = Code.pc;
			Code.pc = refactorMainPcJump;
			Code.put2( mainPc - Code.pc + 1 );
			Code.pc = pc;
		};
		definedMethods++;
		
		Obj entry;
		
		// Set current scope.
		SemanticAnalyzer sa = SemanticAnalyzer.__instance__();
		if ( sa.classIsInside )
		{
			ClassUtil.changeScopeForMethod( sa.classInstance, methodName );
			sa.classCurrentMethod = sa.classInstance.getAllMethods().getMethod(methodName);
			entry = sa.classCurrentMethod.getObj();			
		} else
		{
			Scopes.changeSymbolTableScope(methodName);
			entry = Tab.find(methodName);
		};
		
		// Set start address of method.
		entry.setAdr(Code.pc);
		
		Code.put( Code.enter );
		
		Code.put( entry.getLevel() );											// Parameters size.
		Code.put( entry.getLocalSymbols().size() );								// Local variables of this method.
	}
	
	/**
	 * void __methodName__() { ... } (*)
	 */
	public void visit( MethodDecl methodDecl )
	{
		MethReturnValueCheck check = new MethReturnValueCheck();
		methodDecl.traverseTopDown(check);
		
		if ( !check.hasReturnStatement() )
		{
			Code.put(Code.exit);
			Code.put(Code.return_);
		};
		
		SemanticAnalyzer sa = SemanticAnalyzer.__instance__();
		sa.classCurrentMethod = null;
	}
	
	/**
	 * read( ... ); (*)
	 */
	public void visit(ReadStatement readStatement)
	{
		if ( shouldntGenerateCode() )
			return;
		
		DesignatorReadCheck df = new DesignatorReadCheck(null, false);
		Designator d = readStatement.getDesignator();
		df.setLoadField(false);
		df.setCheck(false);
		d.traverseBottomUp(df);
	}
	
	/**
	 * print (*) ( ... );
	 */
	public void visit(PrintStart ps)
	{
		nestedFuncCall++;
	}
	
	/**
	 * print( ..., ... ); (*)
	 */
	public void visit(PrintStatement printStatement)
	{		
		nestedFuncCall--;
		if ( shouldntGenerateCode() )
			return; 
		
		ExprNoError e = (ExprNoError) printStatement.getPrintWrapper().getPrintWrapperAll().getExpr();
		PrintNumAddon p = printStatement.getPrintWrapper().getPrintWrapperAll().getPrintNumAddon();
		int width = 0;
		
		if ( p instanceof PrintNumAddonNoEps )
			width = ((PrintNumAddonNoEps) p).getN1();
	
		HasComplexOperators hco = new HasComplexOperators();
		hco.performCheck(e);
		if ( !hco.cond() )
		{
			// Generate code for the expression.
			TD_Expr vis = new TD_Expr();
			vis.createNewContext(CGExprContext.SIDE_EXPR);
			vis.visit(e);
	
			Functions.print(vis.getType(), width);
		} else
		{
			ComplexOperator co = new ComplexOperator();
			co.generate(e);
			
			Functions.print(Tab.intType, width);
		};
	}
	
	/**
	 * return (*) Expr; 
	 */
	public void visit(ReturnExprStart s)
	{
		if ( shouldntGenerateCode() )
			return;
		
		ignoreMethodGeneration = true;
	}
	
	/**
	 * return Expr; (*) 
	 */
	public void visit(ReturnExpr re)
	{	
		if ( shouldntGenerateCode() )
			return;
		/*
		RetVal retVal = re.getRetVal();
		if ( retVal instanceof RetValType )
		{
			// Pass return value on expression stack.
			ExprNoError exp = (ExprNoError) ((RetValType) retVal).getExpr();
			TD_Expr g = new TD_Expr();
			g.createNewContext(CGExprContext.SIDE_EXPR);
			g.visit(exp);
		};
		*/

		RetVal retVal = re.getRetVal();
		if ( retVal instanceof RetValType )
		{
			IsMethodCheck i = new IsMethodCheck();
			// Pass return value on expression stack.
			ExprNoError exp = (ExprNoError) ((RetValType) retVal).getExpr();
			
			HasComplexOperators hco = new HasComplexOperators();
			hco.performCheck(exp);
			if ( !hco.cond() )
			{
				// Pass return value on expression stack.
				TD_Expr g = new TD_Expr();
				g.createNewContext(CGExprContext.SIDE_EXPR);
				g.visit(exp);
			} else
			{
				ComplexOperator co = new ComplexOperator();
				co.generate(exp);
			};
		};
		
		// Exit function.
		Code.put(Code.exit);
		Code.put(Code.return_);
		ignoreMethodGeneration = false;
	}
	
	/**
	 * __varname__++ (*) ;
	 */
	public void visit(PlusPlusExpr ppe)
	{
		if ( shouldntGenerateCode() )
			return;
		
		Designator d = ppe.getDesignator();		
		DesignatorCGIncDec df = new DesignatorCGIncDec(null, false, 1);
		df.traverse(d);		
	}
	
	/**
	 * __varname__-- (*) ;
	 */
	public void visit(MinusMinusExpr mme)
	{
		if ( shouldntGenerateCode() )
			return;
		
		Designator d = mme.getDesignator();
		DesignatorCGIncDec df = new DesignatorCGIncDec(null, false, -1);
		df.traverse(d);
	}
	
	/**
	 * __var__ (*) (=|+=|-=|*=|/=|%=) Expr ;
	 */
	public void visit(EqualExprStart e)
	{		
		if ( shouldntGenerateCode() )
			return;
		
		this.ignoreMethodGeneration = true;
		generateLeft = true;
	}
	
	/**
	 * __var__ (=|+=|-=|*=|/=|%=) new Factor[] (*) ;
	 */
	public void visit(FactorNew fn)
	{
		generateLeft = false;
	}
	
	/**
	 * __var__ (=|+=|-=|*=|/=|%=) Expr (*) ;
	 */
	public void visit(EqualExpr e)
	{
		if ( shouldntGenerateCode() )
			return;
		
		Designator d = e.getEqualExprStart().getDesignator();
		DesignatorEqualExpr dee = new DesignatorEqualExpr( e.getAssignop(), (ExprNoError)e.getExpr());
		dee.setCheck(false);
		dee.traverse(d);
		
		this.ignoreMethodGeneration = false;
	}
	
	public void visit(FuncCall fc)
	{
		if ( shouldntGenerateCode() )
			return;
		
		isPureFuncCall = false;
		
		// Generate code.
		MethodCodeGeneration mcg = new MethodCodeGeneration();
		fc.traverseBottomUp(mcg);
		
		nestedFuncCall--;		
	}
	
	public void visit(FuncCallStart s)
	{
		if ( shouldntGenerateCode() )
			return;
		
		isPureFuncCall = true;
		nestedFuncCall++;
	}
	
	public void visit(FactorParensStart f)
	{		
		if ( ignoreMethodGeneration || shouldntGenerateCode() )
			return;
		
		nestedFuncCall++;
	}
	
	public void visit(FactorParensEnd f)
	{		
		if ( ignoreMethodGeneration || shouldntGenerateCode() )
			return;
		
		nestedFuncCall--;
		
		if ( nestedFuncCall == 0 )
		{		
			// Generate code.
			MethodCodeGeneration mcg = new MethodCodeGeneration();
			f.getParent().getParent().traverseBottomUp(mcg);
		};
	}
	
	public void visit(ForStatementStart f)
	{
		isForLoop = true;
		forLoopCnt++;
	}
	
	public void visit(ForStatement fs)
	{				
		forLoopCnt--;
		if ( shouldntGenerateCode() )
			return;
		
		if ( forLoopCnt == 0 )
		{	
			// Generate code for FOR statement.
			ForStatementCG f = new ForStatementCG(this);
			f.generate(fs);
		};
	}
	
	public void visit( ForeachStatementStart st )
	{
		foreachLoopCnt++;
	}
	
	public void visit(ForeachStatement st)
	{
		foreachLoopCnt--;
		if ( shouldntGenerateCode() )
			return;
		
		ForeachStatementCG fsc = new ForeachStatementCG(this);
		fsc.generate(st);
	}
	
	public void visit(IfStart i)
	{		
		isIfStatement = true;
		ifStatementCnt++;
	}
	
	public void visit(UnmatchedIf i)
	{		
		ifStatementCnt--;
		if ( ifStatementCnt == 0 && forLoopCnt == 0 )
		{			
			isIfStatement = false;
			
			// Generate code for IF statement.
			IfStatementCG j = new IfStatementCG(this);
			j.generateAll(i);
		};
	}
	
	public void visit(UnmatchedIfElse i)
	{		
		ifStatementCnt--;
		if ( ifStatementCnt == 0 && forLoopCnt == 0 )
		{						
			// Generate if statement.
			IfStatementCG j = new IfStatementCG(this);
			j.generateAll(i);
		};
	}
	
	public void visit(MatchedIfElse i)
	{		
		ifStatementCnt--;
		if ( ifStatementCnt == 0 && forLoopCnt == 0 )
		{
			isIfStatement = false;
			
			// Generate if condition.
			IfStatementCG j = new IfStatementCG(this);
			j.generateAll(i);
		};
	}
	
	// ===================================================================================================
	// 		Class Declaration
	// ===================================================================================================
	
	public void visit( DeclarationsEnd end )
	{
		// Creates virtual tables.
		ClassUtil.createVirtualTables( this, SemanticAnalyzer.__instance__().varNumber, getMainPC() );
	}
	
	public void visit(ClassDeclStart c)
	{
		SemanticAnalyzer sa = SemanticAnalyzer.__instance__();
		sa.classIsInside = true;
		sa.classInstance = ClassUtil.getClassDeclarationByName( c.getI1() );
	}
	
	public void visit(ClassDeclNoError e)
	{	
		SemanticAnalyzer sa = SemanticAnalyzer.__instance__();
		sa.classIsInside = false;
		sa.classInstance = null;
	}
	
	public void visit( AbstractClassDeclStart s )
	{
		SemanticAnalyzer sa = SemanticAnalyzer.__instance__();
		sa.classIsInside = true;		
		sa.classInstance = ClassUtil.getClassDeclarationByName( s.getI1() );
	}
	
	public void visit( AbstractClassDeclNoError end )
	{	
		SemanticAnalyzer sa = SemanticAnalyzer.__instance__();
		// Reset all variables.
		sa.classIsInside = false;
		sa.classInstance = null;
	}

}
