package rs.ac.bg.etf.pp1.context;

import rs.ac.bg.etf.pp1.ast.SyntaxNode;
import rs.ac.bg.etf.pp1.des.DesignatorFinder;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class MethodInvocationContext {
	
	public int parametersRequired;
	public int parametersProvided;
	public Obj methodObj;
	public SyntaxNode designator;
	public Obj[] params;
	public boolean started;
	public boolean isClassMethod;
	public DesignatorFinder exprDesignator;
	
	public MethodInvocationContext( String methodName )
	{
		this( Tab.find( methodName ) );
	}
	
	public MethodInvocationContext( Obj entry )
	{
		methodObj = entry;
		parametersProvided = 0;
		parametersRequired = entry.getLevel();
		this.started = false;
		this.isClassMethod = false;
		this.exprDesignator = null;
	}
	
	public boolean hasParameters()
	{
		return params != null;
	}
	
	public boolean enoughGeneratedParameters()
	{
		return parametersProvided == parametersRequired;
	}
}
