// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class DesignatorArrayAccess extends DesignatorFact {

    private DesignatorOptStart DesignatorOptStart;
    private Expr Expr;
    private DesignatorOptEnd DesignatorOptEnd;

    public DesignatorArrayAccess (DesignatorOptStart DesignatorOptStart, Expr Expr, DesignatorOptEnd DesignatorOptEnd) {
        this.DesignatorOptStart=DesignatorOptStart;
        if(DesignatorOptStart!=null) DesignatorOptStart.setParent(this);
        this.Expr=Expr;
        if(Expr!=null) Expr.setParent(this);
        this.DesignatorOptEnd=DesignatorOptEnd;
        if(DesignatorOptEnd!=null) DesignatorOptEnd.setParent(this);
    }

    public DesignatorOptStart getDesignatorOptStart() {
        return DesignatorOptStart;
    }

    public void setDesignatorOptStart(DesignatorOptStart DesignatorOptStart) {
        this.DesignatorOptStart=DesignatorOptStart;
    }

    public Expr getExpr() {
        return Expr;
    }

    public void setExpr(Expr Expr) {
        this.Expr=Expr;
    }

    public DesignatorOptEnd getDesignatorOptEnd() {
        return DesignatorOptEnd;
    }

    public void setDesignatorOptEnd(DesignatorOptEnd DesignatorOptEnd) {
        this.DesignatorOptEnd=DesignatorOptEnd;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(DesignatorOptStart!=null) DesignatorOptStart.accept(visitor);
        if(Expr!=null) Expr.accept(visitor);
        if(DesignatorOptEnd!=null) DesignatorOptEnd.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(DesignatorOptStart!=null) DesignatorOptStart.traverseTopDown(visitor);
        if(Expr!=null) Expr.traverseTopDown(visitor);
        if(DesignatorOptEnd!=null) DesignatorOptEnd.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(DesignatorOptStart!=null) DesignatorOptStart.traverseBottomUp(visitor);
        if(Expr!=null) Expr.traverseBottomUp(visitor);
        if(DesignatorOptEnd!=null) DesignatorOptEnd.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("DesignatorArrayAccess(\n");

        if(DesignatorOptStart!=null)
            buffer.append(DesignatorOptStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expr!=null)
            buffer.append(Expr.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(DesignatorOptEnd!=null)
            buffer.append(DesignatorOptEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [DesignatorArrayAccess]");
        return buffer.toString();
    }
}
