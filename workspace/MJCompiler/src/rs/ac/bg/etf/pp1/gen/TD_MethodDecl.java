package rs.ac.bg.etf.pp1.gen;

import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.ast.VisitorAdaptor;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class TD_MethodDecl extends VisitorAdaptor {
	
	private int varDecl = 0;
	private boolean hasReturnStatement;
	private Struct returnType;
	
	public TD_MethodDecl()
	{
		this.hasReturnStatement = false;
		this.returnType = Tab.noType;
	}
	
	/**
	 * Visits var declaration of method.
	 */
	public void visit(VarDecl varDecl)
	{
		this.varDecl++;
	}
	
	/**
	 * void __methodName__ (*)
	 */
	public void visit(MethodDeclStart methodDeclStart)
	{
		// Change later.
		this.hasReturnStatement = false;
	}
	
	/**
	 * =======================================================================================================================
	 * 		Getters
	 * =======================================================================================================================
	 * 
	 */
	
	public int getVarDeclNum()
	{
		return varDecl;
	}
	
	public boolean hasReturnStatement()
	{
		return this.hasReturnStatement;
	}
	
	public Struct getReturnType()
	{
		return this.returnType;
	}
}
