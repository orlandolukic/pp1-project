// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class CondTermMorePresent extends CondTermMore {

    private CondTermMore CondTermMore;
    private CondTerm CondTerm;

    public CondTermMorePresent (CondTermMore CondTermMore, CondTerm CondTerm) {
        this.CondTermMore=CondTermMore;
        if(CondTermMore!=null) CondTermMore.setParent(this);
        this.CondTerm=CondTerm;
        if(CondTerm!=null) CondTerm.setParent(this);
    }

    public CondTermMore getCondTermMore() {
        return CondTermMore;
    }

    public void setCondTermMore(CondTermMore CondTermMore) {
        this.CondTermMore=CondTermMore;
    }

    public CondTerm getCondTerm() {
        return CondTerm;
    }

    public void setCondTerm(CondTerm CondTerm) {
        this.CondTerm=CondTerm;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(CondTermMore!=null) CondTermMore.accept(visitor);
        if(CondTerm!=null) CondTerm.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(CondTermMore!=null) CondTermMore.traverseTopDown(visitor);
        if(CondTerm!=null) CondTerm.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(CondTermMore!=null) CondTermMore.traverseBottomUp(visitor);
        if(CondTerm!=null) CondTerm.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("CondTermMorePresent(\n");

        if(CondTermMore!=null)
            buffer.append(CondTermMore.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(CondTerm!=null)
            buffer.append(CondTerm.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [CondTermMorePresent]");
        return buffer.toString();
    }
}
