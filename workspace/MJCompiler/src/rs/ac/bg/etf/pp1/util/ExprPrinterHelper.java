package rs.ac.bg.etf.pp1.util;

import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.ast.*;

public class ExprPrinterHelper extends VisitorAdaptor {

	private Stack<StringBuilder> stack;
	private StringBuilder initial;
	
	public ExprPrinterHelper(StringBuilder str)
	{
		this.initial = str;
		this.stack = new Stack<StringBuilder>();
	}
	
	private void push()
	{
		this.stack.push(initial);
		this.initial = new StringBuilder();
	}
	
	private void pop(String before, String after)
	{
		StringBuilder f = stack.pop();
		if ( before != null )
			f.append(before);
		f.append(initial.toString());
		if ( after != null )
			f.append(after);
		initial = f;
	}
	
	public void prepareExpr()
	{
		pop(null, null);
	}
	
	/**
	 * =======================================================================================================
	 * 	Visit methods
	 * =======================================================================================================
	 */
	
	public void visit(ExprNoError e)
	{
		
	}
	
	public void visit(NegativeExprMinus nem)
	{
		push();
		this.initial.append( "-" );
	}
	
	public void visit(NegativeExprNoMinus nm)
	{
		push();
	}
	
	public void visit(FactorConstDeclType cnst)
	{
		ExprConstDeclType f = cnst.getExprConstDeclType();
		if ( f instanceof EBoolConst )
			this.initial.append( ((EBoolConst) f).getVal() );
		else if ( f instanceof ECharConst )
			this.initial.append( ((ECharConst) f).getVal() );
		else if ( f instanceof ENumberConst )
			this.initial.append( ((ENumberConst) f).getVal() );
	}
	
	public void visit(FactorWithExpr fwe)
	{
		pop("(", ")");
	}
	
	public void visit(FactorNew fn)
	{
		Type t = fn.getType();
		initial.append( "new " );
		initial.append( Functions.getNameForStruct( Functions.getTypeBySubstitution(t).getKind() ) );
	}
	
	public void visit(DesignatorStart start)
	{
		initial.append(start.getVarName());
	}
	
	public void visit(FactorExprAll fea)
	{
		pop("[", "]");
	}
	
	public void visit(DesignatorArrayAccess dopt)
	{
		pop("[", "]");
	}
	
	public void visit(DesignatorOptStart doptStart)
	{
		push();
	}
	
	public void visit(DesignatorObjectAccess oa)
	{
		initial.append(".");
		initial.append(oa.getI1());
	}
	
	public void visit(Designator d)
	{
		/*
		if ( d.getDesignatorOptExpr() instanceof DesignatorOpt )
			pop( d.getVarName(), null );
		else
			initial.append(d.getVarName());
			*/
	}
	
	public void visit(FactorParensStart s)
	{		
		initial.append( "(" );
		push();
	}
	
	public void visit(FactorParensEnd f)
	{
		pop( null, ")" );
	}
	
	public void visit(AddopPlus a)
	{
		addopOperator((Addop) a.getParent());
	}
	
	public void visit(AddopMinus a)
	{
		addopOperator((Addop) a.getParent());
	}
	
	public void visit(MulopTimes m)
	{
		mulopOperator((Mulop) m.getParent());
	}
	
	public void visit(MulopMod m)
	{
		mulopOperator((Mulop) m.getParent());
	}
	
	public void visit(MulopDiv m)
	{
		mulopOperator((Mulop) m.getParent());
	}
	
	private void addopOperator(Addop a)
	{
		initial.append( Functions.getAddopOperator(a) );
	}
	
	private void mulopOperator(Mulop m)
	{
		initial.append( Functions.getMulopOperator(m) );
	}
	
}
