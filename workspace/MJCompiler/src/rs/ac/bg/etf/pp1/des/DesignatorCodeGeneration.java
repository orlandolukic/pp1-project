package rs.ac.bg.etf.pp1.des;

import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.ast.DesignatorArrayAccess;
import rs.ac.bg.etf.pp1.ast.DesignatorObjectAccess;
import rs.ac.bg.etf.pp1.util.DesignatorFinderCGContext;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class DesignatorCodeGeneration extends DesignatorFinder {

	public DesignatorCodeGeneration() {
		super( SemanticAnalyzer.__instance__() );
	}
	
	@Override
	protected void designatorFinished() {
			
	}

}
