// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class ClassDeclNoError extends ClassDecl {

    private ClassDeclStart ClassDeclStart;
    private VarDeclList VarDeclList;
    private ClassDeclMethodsWrapper ClassDeclMethodsWrapper;

    public ClassDeclNoError (ClassDeclStart ClassDeclStart, VarDeclList VarDeclList, ClassDeclMethodsWrapper ClassDeclMethodsWrapper) {
        this.ClassDeclStart=ClassDeclStart;
        if(ClassDeclStart!=null) ClassDeclStart.setParent(this);
        this.VarDeclList=VarDeclList;
        if(VarDeclList!=null) VarDeclList.setParent(this);
        this.ClassDeclMethodsWrapper=ClassDeclMethodsWrapper;
        if(ClassDeclMethodsWrapper!=null) ClassDeclMethodsWrapper.setParent(this);
    }

    public ClassDeclStart getClassDeclStart() {
        return ClassDeclStart;
    }

    public void setClassDeclStart(ClassDeclStart ClassDeclStart) {
        this.ClassDeclStart=ClassDeclStart;
    }

    public VarDeclList getVarDeclList() {
        return VarDeclList;
    }

    public void setVarDeclList(VarDeclList VarDeclList) {
        this.VarDeclList=VarDeclList;
    }

    public ClassDeclMethodsWrapper getClassDeclMethodsWrapper() {
        return ClassDeclMethodsWrapper;
    }

    public void setClassDeclMethodsWrapper(ClassDeclMethodsWrapper ClassDeclMethodsWrapper) {
        this.ClassDeclMethodsWrapper=ClassDeclMethodsWrapper;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ClassDeclStart!=null) ClassDeclStart.accept(visitor);
        if(VarDeclList!=null) VarDeclList.accept(visitor);
        if(ClassDeclMethodsWrapper!=null) ClassDeclMethodsWrapper.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ClassDeclStart!=null) ClassDeclStart.traverseTopDown(visitor);
        if(VarDeclList!=null) VarDeclList.traverseTopDown(visitor);
        if(ClassDeclMethodsWrapper!=null) ClassDeclMethodsWrapper.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ClassDeclStart!=null) ClassDeclStart.traverseBottomUp(visitor);
        if(VarDeclList!=null) VarDeclList.traverseBottomUp(visitor);
        if(ClassDeclMethodsWrapper!=null) ClassDeclMethodsWrapper.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ClassDeclNoError(\n");

        if(ClassDeclStart!=null)
            buffer.append(ClassDeclStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(VarDeclList!=null)
            buffer.append(VarDeclList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(ClassDeclMethodsWrapper!=null)
            buffer.append(ClassDeclMethodsWrapper.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ClassDeclNoError]");
        return buffer.toString();
    }
}
