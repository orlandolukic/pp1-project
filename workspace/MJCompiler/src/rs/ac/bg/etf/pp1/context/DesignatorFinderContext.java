package rs.ac.bg.etf.pp1.context;

import rs.ac.bg.etf.pp1.ast.SyntaxNode;
import rs.ac.bg.etf.pp1.clss.ClassDeclaration;
import rs.etf.pp1.symboltable.concepts.*;

public class DesignatorFinderContext {

	public boolean stop;
	public Obj current;
	public Struct type;
	private SyntaxNode node;
	public boolean isArrayReference;
	public int arrAccess;
	public ClassDeclaration classDeclaration;
	public boolean isMethod;
	public boolean isNull;

	public DesignatorFinderContext()
	{
		stop = false;
		isMethod = false;
		isNull = false;
		isArrayReference = false;
		arrAccess = 0;
		classDeclaration = null;
	}
	
	public SyntaxNode getNode()
	{
		return node;
	}
	
	public void setNode(SyntaxNode node)
	{
		if ( this.node == null )
			this.node = node;
	}
}
