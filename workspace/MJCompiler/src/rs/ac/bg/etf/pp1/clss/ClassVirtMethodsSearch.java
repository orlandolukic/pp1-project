package rs.ac.bg.etf.pp1.clss;

public class ClassVirtMethodsSearch {
	
	public ClassDeclaration selfClass;
	public ClassMethod method;
	
	public ClassVirtMethodsSearch( ClassDeclaration d, ClassMethod m )
	{
		selfClass = d;
		method = m;
	}
}
