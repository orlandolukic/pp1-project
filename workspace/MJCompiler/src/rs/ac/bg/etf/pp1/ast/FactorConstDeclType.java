// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class FactorConstDeclType extends Factor {

    private ExprConstDeclType ExprConstDeclType;

    public FactorConstDeclType (ExprConstDeclType ExprConstDeclType) {
        this.ExprConstDeclType=ExprConstDeclType;
        if(ExprConstDeclType!=null) ExprConstDeclType.setParent(this);
    }

    public ExprConstDeclType getExprConstDeclType() {
        return ExprConstDeclType;
    }

    public void setExprConstDeclType(ExprConstDeclType ExprConstDeclType) {
        this.ExprConstDeclType=ExprConstDeclType;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ExprConstDeclType!=null) ExprConstDeclType.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ExprConstDeclType!=null) ExprConstDeclType.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ExprConstDeclType!=null) ExprConstDeclType.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("FactorConstDeclType(\n");

        if(ExprConstDeclType!=null)
            buffer.append(ExprConstDeclType.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [FactorConstDeclType]");
        return buffer.toString();
    }
}
