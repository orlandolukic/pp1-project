// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class ClassDeclMethodsWrapperPresent extends ClassDeclMethodsWrapper {

    private ClassDeclMethods ClassDeclMethods;

    public ClassDeclMethodsWrapperPresent (ClassDeclMethods ClassDeclMethods) {
        this.ClassDeclMethods=ClassDeclMethods;
        if(ClassDeclMethods!=null) ClassDeclMethods.setParent(this);
    }

    public ClassDeclMethods getClassDeclMethods() {
        return ClassDeclMethods;
    }

    public void setClassDeclMethods(ClassDeclMethods ClassDeclMethods) {
        this.ClassDeclMethods=ClassDeclMethods;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(ClassDeclMethods!=null) ClassDeclMethods.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(ClassDeclMethods!=null) ClassDeclMethods.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(ClassDeclMethods!=null) ClassDeclMethods.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("ClassDeclMethodsWrapperPresent(\n");

        if(ClassDeclMethods!=null)
            buffer.append(ClassDeclMethods.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [ClassDeclMethodsWrapperPresent]");
        return buffer.toString();
    }
}
