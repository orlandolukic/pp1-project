package rs.ac.bg.etf.pp1;

import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.clss.ClassDeclarationList;
import rs.ac.bg.etf.pp1.clss.ClassUtil;
import rs.ac.bg.etf.pp1.des.DesignatorFinder;
import rs.ac.bg.etf.pp1.des.DesignatorTraverse;
import rs.ac.bg.etf.pp1.util.ComplexOperatorWrapper;
import rs.ac.bg.etf.pp1.util.DesignatorFinderCGContext;
import rs.ac.bg.etf.pp1.util.ExprCheckerContext;
import rs.ac.bg.etf.pp1.util.HasComplexOperators;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class ExprChecker {

	public static final int NO_ERROR = -1, 
						    ERROR = 0,
						    WARNING = 1;
	private int type;
	private String mssg;
	private ExprCheckerContext context;
	private boolean isComplex;
	private boolean performComplexExpressionCheck;
	
	public ExprChecker( Struct expected )
	{
		this.type = NO_ERROR;
		this.mssg = "";
		this.isComplex = false;
		this.context = new ExprCheckerContext(expected, null);
		this.performComplexExpressionCheck = true;
	}
	
	public void doPerformComplexExpressionCheck(boolean value)
	{
		this.performComplexExpressionCheck = value;
	}
	
	public String getMessage()
	{
		return this.mssg;
	}
	
	public int getRaisedErrorType()
	{
		return this.type;
	}
	
	public void setError( int type, String message ) {
		this.type = type;
		this.mssg = message;
	}
	
	public boolean forcePrint()
	{
		return this.context.forcePrint;
	}
	
	public boolean isComplex()
	{
		return isComplex;
	}
	
	public void terminate(String mssg) throws Exception
	{
		this.type = ERROR;
		this.mssg = mssg;
		finish();
	}
	
	public void warn(String mssg) throws Exception
	{
		this.type = WARNING;
		this.mssg = mssg;
		finish();
	}
	
	public void finish() throws Exception
	{
		throw new Exception();
	}
	
	public ExprCheckerContext getContext()
	{
		return this.context;
	}
	
	public boolean process(ExprNoError expr)
	{
		boolean valid = true;
		
		this.context.root = expr;
		this.context.multiple = expr.getAddopTerm() instanceof AddopTermAll;
		
		try {
			this.process(expr.getNegativeExpr());
			this.process(expr.getTerm());
			this.process(expr.getAddopTerm());
			
			HasComplexOperators hco = new HasComplexOperators();
			expr.traverseTopDown(hco);
			boolean hasComplexOperators = hco.cond();
			
			// Check complex operator if it's equal statement.
			if ( performComplexExpressionCheck && hasComplexOperators )
			{
				ComplexOperatorWrapper check = new ComplexOperatorWrapper(false);
				expr.traverseBottomUp(check);
				check.finalCheck();
				
				if ( check.errorOccured() )
					terminate( check.getMessage() );
			};
		} catch( Exception e ) {
			valid = false;
		};
		
		return valid;
	}
	
	
	public void process(NegativeExpr negExpr) throws Exception
	{
		if ( negExpr instanceof NegativeExprMinus )
		{
			this.context.hasMinus = true;
			if ( !this.context.expected.equals(Tab.intType) )
			{
				this.terminate("Cannot use '-' with type different than Integer");
			};
		};
	}
	
	public void process(Term term) throws Exception
	{
		Factor f = term.getFactor();
		MulopFactor mf = term.getMulopFactor();
		
		context.multiple |= mf instanceof MulopFactorAll;
		
		this.process(f);
		this.process(mf);
	}
	
	public void process(AddopTerm addopTerm) throws Exception
	{
		AddopTerm f = addopTerm;
		Term t = null;
		Addop a = null;
		boolean entered = false;

		if ( f instanceof AddopTermAll )
		{
			a = ((AddopTermAll) f).getAddop();
			this.isComplex |= Functions.isComplexAddopOperator(a);
			
			if ( context.expected.equals(Functions.boolType) || context.cantPassAddop )
				this.terminate("Cannot use '" + Functions.getAddopOperator(a) + "' operator on boolean type");
			
			t = ((AddopTermAll) f).getTerm();
			f = ((AddopTermAll) f).getAddopTerm();
			this.process(f);
			entered = true;
		};
		
		if ( entered )
		{
			// Process term.
			this.process(t);
		};		
	
	}
	
	public void process(Factor f) throws Exception
	{
		if ( f instanceof FactorConstDeclType )
		{
			Struct given;
			if ( context.isEqualStatement && context.isReference )
				this.terminate("Cannot assign constant on reference type for variable");
			
			// If reference is expected but user provided constant.
			if ( context.isExpectedReference )
				this.terminate("Cannot use constant when reference expected");
			
			ExprConstDeclType c = ((FactorConstDeclType) f).getExprConstDeclType();	
			given = Functions.getStructForExprConstDeclType(c);
			
			if ( context.expected.getKind() == Struct.Array )
				this.terminate(Functions.getUncompatibileTypesMessage(context.expected, given));
			
			if ( !context.expected.compatibleWith(given) )
				this.terminate( Functions.getUncompatibileTypesMessage(context.expected, given) );
			
			// Return constant.
			context.constReturn = Functions.getObjForExprConstDeclType( ((FactorConstDeclType) f).getExprConstDeclType() );

		} else if ( f instanceof FactorWithExpr )		// ( Expr )
		{
			ExprNoError exp = (ExprNoError)(((FactorWithExpr) f).getExpr());
			ExprCheckerContext temp = this.context;
			this.context = new ExprCheckerContext(temp.expected, exp);
			if ( ! this.process( exp ) )
				throw new Exception();
			this.context = temp;
			if ( temp.constReturn == null )
				this.context.constReturn = temp.constReturn;
		} else if ( f instanceof FactorNew )			// NEW Type FactorExpr
		{			
			if ( this.context.multiple )
				this.terminate("Cannot create new object in expression");	
			
			if ( this.context.isEqualStatement && !this.context.isReference )
				this.terminate("Cannot allocate data for non-reference variable");
			
			FactorExpr fe = ((FactorNew) f).getFactorExpr();
			
			// Check if type is added into the symbol table.
			if ( ((FactorNew) f).getType() instanceof TIdent )
			{
				if ( !context.isExpectedReference )
					this.terminate("Cannot use reference when non-reference value is expected");
				
				String refName = ((TIdent) ((FactorNew) f).getType()).getType();
				Obj ty = Tab.find( refName );
			
				if ( ty == Tab.noObj )
					this.terminate("Type '" + refName + "' is not defined");
				
				// Check if expected class is the same as given one.
				String baseClass = context.classDeclaration.getName();
				if ( !baseClass.equals(refName) && !ClassUtil.isDerived(baseClass, refName) )
					this.terminate("Given type '" + refName + "' should be '" + baseClass + "' or any class derived from it");
			};
			
			if ( fe instanceof FactorExprAll )
			{
				ExprNoError exp = (ExprNoError)(((FactorExprAll) fe).getExpr());
				ExprCheckerContext temp = this.context;
				this.context = new ExprCheckerContext(Tab.intType, exp);
				if ( ! this.process( exp ) )
					throw new Exception();
				
				double val = -1;
				
				try {
					String expr = getExprAsString(exp);
					
					try {
						val = Functions.eval(expr);											
					} catch(Exception e)
					{
						throw new Exception("Value of the expression [" + expr + "] could not be determined");
					};					
					
				} catch (Exception e) {
					//this.terminate(e.getMessage());
					this.warn(e.getMessage());
				};
				
				if ( val < 0 )
					this.terminate("Could not allocate negative amount of data (" + ((int)(val)) + ")");
				
				this.context = temp;
			}
		} else if ( f instanceof FactorDesignator )
		{
			SemanticAnalyzer sa = SemanticAnalyzer.__instance__();
			Designator d = ((FactorDesignator) f).getDesignator();
			FactorParens fp = ((FactorDesignator) f).getFactorParens();
			
			DesignatorTraverse df = new DesignatorTraverse(sa, this);
			df.setReport(context.reportInDesignatorSearch);			
			df.traverse(d);
			if ( type == ERROR || type == WARNING )
				finish();
			
			if ( fp instanceof FactorParensPresent )	// Function call.
			{
				context.methReturn = df.getObject();
				sa.exprDesignator = df;
				/*ActParsWrapper apw = ((ActParsWrapper) ((FactorParensPresent) fp).getActParsWrapper());
				if ( apw instanceof ActParsPresent )
				{
					ActPars ap = ((ActParsPresent) apw).getActPars();
					
				}*/
				/*
				ExprNoError exp = (ExprNoError) ((DesignatorOpt) doe).getExpr();
				ExprCheckerContext temp = this.context;
				this.context = new ExprCheckerContext(Tab.intType, exp);
				if ( ! this.process( exp ) )
					throw new Exception();
				this.context = temp;*/
			};
		}
	}
	
	public void process(MulopFactor mf) throws Exception
	{
		MulopFactor x = mf;
		Factor f = null;
		Mulop m = null;
		boolean entered = false;
		
		if ( x instanceof MulopFactorAll )
		{
			m = ((MulopFactorAll) x).getMulop();
			this.isComplex |= Functions.isComplexMulopOperator(m);
			
			if ( context.expected.equals(Functions.boolType) || context.cantPassMulop )
				this.terminate("Cannot use '" + Functions.getMulopOperator(m) + "' operator on boolean type");
			
			f = ((MulopFactorAll) x).getFactor();
			x = ((MulopFactorAll) x).getMulopFactor();
			this.process(x);
			entered = true;
		};
		
		if ( entered )
		{
			// Process factor.
			this.process(f);
		};
		
	}
	
	public void process(FactorExpr facExpr) throws Exception
	{
		if ( facExpr instanceof FactorExprAll )
		{
			ExprNoError exp = (ExprNoError)(((FactorExprAll) facExpr).getExpr());
			ExprCheckerContext temp = this.context;
			this.context = new ExprCheckerContext(Tab.intType, exp);
			if ( ! this.process( exp ) )
				throw new Exception();
			this.context = temp;
		}
	}	
	
	/**
	 * ==============================================================================================================
	 * 		Check of ++ & -- operators
	 * ==============================================================================================================
	 */
	
	// ++
	public static void checkIncrementOperators(SemanticAnalyzer sa, DesignatorStatement ds, Designator d)
	{
		
	}
	
	private class DesignatorIncrementCheck extends DesignatorFinder
	{

		public DesignatorIncrementCheck(SemanticAnalyzer sa) {
			super(sa);
		}

		@Override
		protected void designatorStartVariable( DesignatorFinderCGContext c ) {			
			if ( c.obj.getName().equals("null") && ( !context.allowNullReference || context.multiple ) )
				this.terminate("Cannot use null object reference in expression");	
		}


		@Override
		protected void designatorFinished() {
			String varname = designatorContext.current.getName();
			Obj entry = designatorContext.current;
			
			int kind = entry.getKind();
			Struct s = designatorContext.current.getType();
			if ( kind == Obj.Con )
				SemanticAnalyzer.__instance__().report_error("Cannot change value of constant '" + varname + "'", designatorContext.getNode());
			else if ( kind == Obj.Meth )
				SemanticAnalyzer.__instance__().report_error("Cannot use method's name '" + varname + "' with '++' operator", designatorContext.getNode());
			else if ( kind == Obj.Var )
			{
				// Array or class
				if ( s.getKind() == Struct.Array )
				{
					if ( designatorContext.arrAccess == 0 )
					{
						SemanticAnalyzer.__instance__().report_error("Cannot use increment operator on array reference", designatorContext.getNode());
					} else
					{					
						if ( !s.getElemType().equals(Tab.intType) )
							SemanticAnalyzer.__instance__().report_error("Increment of array elements only work on integer arrays", designatorContext.getNode());
					};
				};
			};							
		}		
	}
	
	/**
	 * ==============================================================================================================
	 */
	
	private static StringBuilder builder;
	
	/**
	 * 
	 * Gets expression as string.
	 * 
	 * @param expr Expression to get.
	 * @return expression in human-readable format.
	 * @throws Exception In case expression could not be evaluated at the moment (i.e variable present)
	 */
	public static String getExprAsString(Expr expr) throws Exception
	{		
		builder = new StringBuilder();
		
		getExpr((ExprNoError) expr);
		
		return builder.toString();
	}
	
	private static void getExpr(ExprNoError expr) throws Exception
	{
		NegativeExpr negExpr = expr.getNegativeExpr();
		Term term = expr.getTerm();
		AddopTerm addopTerm = expr.getAddopTerm();
		
		getExpr(negExpr);
		getExpr(term);
		getExpr(addopTerm);
	}
	
	private static void getExpr(NegativeExpr negExpr) throws Exception
	{
		if ( negExpr instanceof NegativeExprMinus )
		{
			builder.append("-");
		};
	}
	
	public static void getExpr(Term term) throws Exception
	{
		Factor f = term.getFactor();
		MulopFactor mf = term.getMulopFactor();
		
		getExpr(f);
		getExpr(mf);
	}
	
	public static void getExpr(AddopTerm a) throws Exception
	{		
		AddopTerm f = a;
		Term t = null;
		boolean entered = false;

		if ( f instanceof AddopTermAll )
		{
			t = ((AddopTermAll) f).getTerm();
			f = ((AddopTermAll) f).getAddopTerm();
			getExpr(f);
			entered = true;
		};
		
		if ( entered )
		{
			// Process term.
			String sign = Functions.getAddopOperator(((AddopTermAll) a).getAddop());
			builder.append(sign);
			getExpr(t);
		};		
	}
	
	public static void getExpr(Factor f) throws Exception
	{
		if ( f instanceof FactorWithExpr )
		{
			builder.append("(");
			getExpr((ExprNoError) ((FactorWithExpr) f).getExpr());
			builder.append(")");
		} else if ( f instanceof FactorNew )
		{
			throw new Exception("Use of operator 'new' inside expression");
		} else if ( f instanceof FactorConstDeclType )
		{
			ExprConstDeclType e = ((FactorConstDeclType) f).getExprConstDeclType();
			if ( e instanceof EBoolConst )
			{
				throw new Exception("Using boolean inside expression");
			} else if ( e instanceof ECharConst )
			{
				//builder.append("0");
				throw new Exception("Using chars inside expression");
			} else if ( e instanceof ENumberConst )
			{
				builder.append(((ENumberConst) e).getVal());
			};
		} else if ( f instanceof FactorDesignator )
		{
			
			Designator d = ((FactorDesignator) f).getDesignator();
			DesignatorFinder finder = new DesignatorFinder(SemanticAnalyzer.__instance__());
			d.traverseBottomUp(finder);			
			Obj node = finder.getFactorsNum() == 1 ? finder.getFirstFactor() : finder.getObject();
		
			if ( node.getKind() == Obj.Var )
				throw new Exception("Potentially negative offset or index out of bounds");
			else if ( node.getKind() == Obj.Meth )
				throw new Exception("Method name should not be used inside expression");
			else if ( node.getKind() == Obj.Prog )
				throw new Exception("Program name should not be used inside expression");
			else if ( node.getKind() == Obj.Type )
				throw new Exception("Type name should not be used inside variable");
			else if ( node.getKind() == Obj.Con )
			{
				builder.append("(");
				builder.append(node.getAdr());
				builder.append(")");
			} else
				throw new Exception("Error occured making expression");					
		};
		
	}
	
	public static void getExpr(MulopFactor mf) throws Exception
	{
		MulopFactor x = mf;
		Factor f = null;
		Mulop m = null;
		boolean entered = false;
		if ( x instanceof MulopFactorAll )
		{
			f = ((MulopFactorAll) x).getFactor();
			m = ((MulopFactorAll) x).getMulop();
			x = ((MulopFactorAll) x).getMulopFactor();
			getExpr(x);
			entered = true;
		};
		
		if ( entered )
		{
			// Process factor.
			builder.append( Functions.getMulopOperator(m) );
			getExpr(f);
		};	
	}
}
