package rs.ac.bg.etf.pp1.util;

import rs.ac.bg.etf.pp1.clss.ClassUtil;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Scope;
import rs.etf.pp1.symboltable.concepts.Struct;

/**
 * @author nemanja.kojic
 *
 */
public class SymbolTableVisitor extends rs.etf.pp1.symboltable.visitors.SymbolTableVisitor {

	private boolean isType = false;
	private boolean isMeth = false;
	private boolean isFieldOfClass = false;
	private String clssName = null;
	private Stack<Integer> stackNumber;
	private Stack<String> stackClssName;
	private int num;
	protected StringBuilder output = new StringBuilder();
	protected final String indent = "   ";
	protected StringBuilder currentIndent = new StringBuilder();
	
	public SymbolTableVisitor()
	{
		stackNumber = new Stack<>();
		stackClssName = new Stack<>();
	}
	
	protected void nextIndentationLevel() {
		currentIndent.append(indent);
	}
	
	protected void previousIndentationLevel() {
		if (currentIndent.length() > 0)
			currentIndent.setLength(currentIndent.length()-indent.length());
	}
	
	
	/* (non-Javadoc)
	 * @see rs.etf.pp1.symboltable.test.SymbolTableVisitor#visitObjNode(symboltable.Obj)
	 */
	@Override
	public void visitObjNode(Obj objToVisit) {
		
		//output.append("[");
		isType = false;
		isFieldOfClass = false;
		switch (objToVisit.getKind()) {
		case Obj.Con:  output.append("Con "); break;
		case Obj.Var:  output.append("Var "); break;
		case Obj.Type: 
			output.append("Type "); 
			isType = true; 
			clssName = ClassUtil.getClassNameFromObj(objToVisit);		
			stackNumber.push(num);
			num = 0;
			break;
		case Obj.Meth: output.append("Meth "); isMeth = true; break;
		case Obj.Fld:  
			output.append("Fld "); 
			clssName = ClassUtil.getClassNameFromObj(objToVisit);	
			isFieldOfClass = clssName != null;
			break;
		case Obj.Prog: output.append("Prog "); break;
		}
		
		output.append(objToVisit.getName());
		output.append(": ");
		
		if (
				( Obj.Var == objToVisit.getKind() ) && "this".equalsIgnoreCase( objToVisit.getName() ) 
				|| 
				objToVisit.getKind() == Obj.Fld && objToVisit.getType().getKind() == Struct.Class
		   )
		{
			if ( isType || isFieldOfClass )
				output.append("Class " + clssName);
			else
				output.append("");
		} else
			objToVisit.getType().accept(this);
		
		output.append(", ");
		output.append(objToVisit.getAdr());
		output.append(", ");
		output.append(objToVisit.getLevel() + " ");
				
		if (objToVisit.getKind() == Obj.Prog || objToVisit.getKind() == Obj.Meth) {
			output.append("\n");
			nextIndentationLevel();
		}
		

		for (Obj o : objToVisit.getLocalSymbols()) {
			output.append(currentIndent.toString());
			o.accept(this);
			output.append("\n");
		}
		
		if (objToVisit.getKind() == Obj.Prog || objToVisit.getKind() == Obj.Meth) 
			previousIndentationLevel();
		
		
		switch (objToVisit.getKind()) 
		{		
		case Obj.Type: 
			isType = false; 
			num = stackNumber.pop();			
			break;
			
		case Obj.Meth:
			isMeth = false;
			break;
		}

		//output.append("]");
		
	}

	/* (non-Javadoc)
	 * @see rs.etf.pp1.symboltable.test.SymbolTableVisitor#visitScopeNode(symboltable.Scope)
	 */
	@Override
	public void visitScopeNode(Scope scope) {
		for (Obj o : scope.values()) {
			o.accept(this);
			output.append("\n");
		}
	}

	/* (non-Javadoc)
	 * @see rs.etf.pp1.symboltable.test.SymbolTableVisitor#visitStructNode(symboltable.Struct)
	 */
	@Override
	public void visitStructNode(Struct structToVisit) {
		
		switch (structToVisit.getKind()) {
		case Struct.None:
			if ( isMeth )
				output.append("void");
			else
				output.append("notype");
			break;
		case Struct.Int:
			output.append("int");
			break;
		case Struct.Char:
			output.append("char");
			break;
		case Struct.Bool:
			output.append("bool");
			break;
		case Struct.Array:
			output.append("Arr of ");
			
			switch (structToVisit.getElemType().getKind()) 
			{
				case Struct.None:
					output.append("notype");
					break;
				case Struct.Int:
					output.append("int");
					break;
				case Struct.Char:
					output.append("char");
					break;
				case Struct.Class:
					output.append("Class");
					break;
				case Struct.Bool:
					output.append("bool");
					break;
			}
			break;
		case Struct.Class:
			if ( isType )
			{
				output.append("Class [");
				nextIndentationLevel();
				for (Obj obj : structToVisit.getMembers()) {	
					
					num++;
					if ( num == 1 )
						continue;
					
					output.append("\n");
					output.append(currentIndent);
					obj.accept(this);					
				}
				output.append("\n");
				previousIndentationLevel();
				output.append(currentIndent + "]");
			} else
			{
				String str = ClassUtil.getClassNameFromStruct(structToVisit);
				output.append("Class " + str);
			};
			break;
		}

	}

	public String getOutput() {
		return output.toString();
	}
	
	
}
