package rs.ac.bg.etf.pp1.gen;

import java.util.LinkedList;

import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.des.DesignatorFinder;
import rs.ac.bg.etf.pp1.print.ExprPrinter;
import rs.ac.bg.etf.pp1.util.CGExprContext;
import rs.ac.bg.etf.pp1.util.HasComplexOperators;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class MethodCodeGeneration extends VisitorAdaptor {
	
	private boolean genActParam;
	private int calls;
	private LinkedList<SyntaxNode> visitedNodes;
	
	public MethodCodeGeneration()
	{
		visitedNodes = new LinkedList<SyntaxNode>();
		this.calls = 0;
		this.genActParam = false;
	}
	
	public void setGenerationOfActualParameter( boolean val )
	{
		genActParam = val;
	}
	
	private void resetVisitedNodes()
	{
		visitedNodes = new LinkedList<SyntaxNode>();
	}
	
	public LinkedList<SyntaxNode> getVisitedNodes()
	{
		return visitedNodes;
	}
	
	public void addVisitedNodes( LinkedList<SyntaxNode> list )
	{
		Functions.appendLists(visitedNodes, list);
	}
	
	public void visit(FuncCallStart fcs)
	{
		DesignatorFinder finder = new DesignatorFinder( null );
		finder.setCheck(false);
		finder.traverse( fcs.getDesignator() );
		finder.generateThroughTheList(false);
	}
	
	public void visit(FactorParensStart f)
	{	
		this.calls++;
	}
	
	public void visit(FactorParensEnd f)
	{
		this.calls--;
		if ( calls == 0 )
		{
			FactorParensPresent p = (FactorParensPresent) f.getParent();
			Functions.functionCallGenerateCode( this, f.getParent().getParent(), p.getActParsWrapper() );
		};			
	}
	
	public void visit(FuncCall f)
	{		
		Functions.functionCallGenerateCode( this, f, f.getActParsWrapper() );
		
		DesignatorFinder finder = new DesignatorFinder( null );
		finder.setCheck(false);
		finder.traverse( f.getFuncCallStart().getDesignator() );		
		Obj method = finder.getObject(); 
		
		// Add pop instruction if method returns something different than void.
		if ( method.getType().getKind() != Struct.None )
			Code.put( Code.pop );
	}
	
	public void visit(ActParam param)
	{
		if ( visitedNodes.contains(param) )
			return;
		
		ExprNoError e = (ExprNoError) param.getExpr();
		HasComplexOperators hco = new HasComplexOperators();
		hco.performCheck(e);

		if ( !hco.cond() )
		{
			TD_Expr exp = new TD_Expr();
			exp.createNewContext(CGExprContext.SIDE_EXPR);
			exp.getContext().generateActualParam = false;
			exp.visit(e);	
		} else
		{
			ComplexOperator co = new ComplexOperator();
			co.generate(param.getExpr());
		};

		if ( Functions.DEBUG )
			System.out.println("CG: pushing actual param {" + new ExprPrinter(e) + "}" );
		visitedNodes.add(param);
	}

}
