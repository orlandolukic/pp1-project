package rs.ac.bg.etf.pp1.util;

import rs.ac.bg.etf.pp1.ast.ExprNoError;
import rs.ac.bg.etf.pp1.clss.ClassDeclaration;
import rs.etf.pp1.symboltable.concepts.*;

public class ExprCheckerContext {
	
	public Struct expected;
	public boolean hasMinus;
	public boolean multiple;
	public boolean isReference;
	public boolean isEqualStatement;
	public boolean cantPassMulop;
	public boolean cantPassAddop;
	public boolean allowNullReference;
	public boolean onlyReference;
	public boolean forcePrint;
	public boolean isLenInvocation;
	public int complexOperatorDepth;
	public Obj constReturn;
	public Obj methReturn;
	public String varname;
	public ExprNoError root;
	public boolean reportInDesignatorSearch;
	public ClassDeclaration classDeclaration;
	public boolean isExpectedReference;
	
	public ExprCheckerContext(Struct expected, ExprNoError root)
	{
		this.reset(expected, root);
	}
	
	public void reset(Struct expected, ExprNoError root)
	{
		this.expected = expected;
		this.root = root;
		this.hasMinus = false;
		this.multiple = false;
		this.isReference = false;
		this.varname = null;
		this.isEqualStatement = false;
		this.cantPassAddop = false;
		this.cantPassAddop = false;
		this.allowNullReference = false;
		this.onlyReference = false;
		this.forcePrint = false;
		this.isLenInvocation = false;
		this.constReturn = null;
		this.reportInDesignatorSearch = true;
		this.classDeclaration = null;
		this.isExpectedReference = false;
	}
}
