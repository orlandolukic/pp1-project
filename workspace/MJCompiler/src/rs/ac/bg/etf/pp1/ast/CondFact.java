// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class CondFact implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private Invertor Invertor;
    private Expr Expr;
    private CondFactVolatile CondFactVolatile;

    public CondFact (Invertor Invertor, Expr Expr, CondFactVolatile CondFactVolatile) {
        this.Invertor=Invertor;
        if(Invertor!=null) Invertor.setParent(this);
        this.Expr=Expr;
        if(Expr!=null) Expr.setParent(this);
        this.CondFactVolatile=CondFactVolatile;
        if(CondFactVolatile!=null) CondFactVolatile.setParent(this);
    }

    public Invertor getInvertor() {
        return Invertor;
    }

    public void setInvertor(Invertor Invertor) {
        this.Invertor=Invertor;
    }

    public Expr getExpr() {
        return Expr;
    }

    public void setExpr(Expr Expr) {
        this.Expr=Expr;
    }

    public CondFactVolatile getCondFactVolatile() {
        return CondFactVolatile;
    }

    public void setCondFactVolatile(CondFactVolatile CondFactVolatile) {
        this.CondFactVolatile=CondFactVolatile;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(Invertor!=null) Invertor.accept(visitor);
        if(Expr!=null) Expr.accept(visitor);
        if(CondFactVolatile!=null) CondFactVolatile.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(Invertor!=null) Invertor.traverseTopDown(visitor);
        if(Expr!=null) Expr.traverseTopDown(visitor);
        if(CondFactVolatile!=null) CondFactVolatile.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(Invertor!=null) Invertor.traverseBottomUp(visitor);
        if(Expr!=null) Expr.traverseBottomUp(visitor);
        if(CondFactVolatile!=null) CondFactVolatile.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("CondFact(\n");

        if(Invertor!=null)
            buffer.append(Invertor.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expr!=null)
            buffer.append(Expr.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(CondFactVolatile!=null)
            buffer.append(CondFactVolatile.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [CondFact]");
        return buffer.toString();
    }
}
