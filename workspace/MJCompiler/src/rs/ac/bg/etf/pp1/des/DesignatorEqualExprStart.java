package rs.ac.bg.etf.pp1.des;

import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.Obj;

public class DesignatorEqualExprStart extends DesignatorFinder {

	public DesignatorEqualExprStart(SemanticAnalyzer sa) {
		super(sa);
	}
	
	@Override
	protected String undefinedVariableMessage()
	{
		return "Could not assign value to undefined variable";
	}
	
	@Override
	protected void designatorFinished()
	{
		Obj entry = designatorContext.current;
		
		if ( entry != Tab.noObj )
		{
			if ( entry.getKind() == Obj.Con )
				sa.report_error("Cannot change value of constant '" + entry.getName() + "'", substitution);
			else if ( entry.getKind() == Obj.Meth ) 
				sa.report_error("Cannot assign value to method '" + entry.getName() + "'", substitution);
			else if ( entry.getKind() != Obj.Var && entry.getKind() != Obj.Fld ) // Check if scanned token is not variable.
				sa.report_error("Name '" + entry.getName() + "' is not variable", substitution);
		
		};
	}

}
