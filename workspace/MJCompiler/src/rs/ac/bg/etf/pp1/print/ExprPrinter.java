package rs.ac.bg.etf.pp1.print;

import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.util.ExprPrinterHelper;

public class ExprPrinter extends Printer<ExprNoError> {

	public ExprPrinter(ExprNoError snode) {
		super(snode);
	}

	@Override
	public void print() {		
		// Traverse bottom up AST for ExprNoError node.
		try {
			ExprPrinterHelper eph = new ExprPrinterHelper(str);
			snode.traverseBottomUp(eph);
			eph.prepareExpr();
		} catch( Exception e ) {
			str.append("__EXPR_PRINT_ERROR__");
		};
	}

}
