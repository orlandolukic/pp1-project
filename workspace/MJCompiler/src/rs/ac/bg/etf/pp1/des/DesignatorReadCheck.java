package rs.ac.bg.etf.pp1.des;

import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.concepts.Obj;
import rs.etf.pp1.symboltable.concepts.Struct;

public class DesignatorReadCheck extends DesignatorFinder {

	public DesignatorReadCheck(SemanticAnalyzer sa, boolean check) {
		super(sa, check);
	}
	
	public DesignatorReadCheck( SemanticAnalyzer sa ) {
		super(sa, sa != null);		
	}
	
	@Override
	protected void designatorFinishedCodeGeneration() {
		
		if ( check() )
			return;
		
		// Generate.
		generateThroughTheList(false);
	}

	@Override
	protected void designatorFinished() 
	{					
		if ( check() )
		{
			Obj entry = designatorContext.current;
			int kind = entry.getKind();
			if ( kind == Obj.Con )
				sa.report_error("Cannot use read() method with constant '" + entry.getName() + "'", designatorContext.getNode());
			else if ( kind == Obj.Meth )
				sa.report_error("Cannot use read() method with method's name '" + entry.getName() + "'", designatorContext.getNode());
			else if ( kind == Obj.Var )
			{
				Struct type = designatorContext.current.getType();
				if ( type.getKind() == Struct.Array )
				{
					// Check if there are present array brackets [] ?
					if ( designatorContext.arrAccess == 0 )
						sa.report_error("Cannot use array reference '" + print(null) + "' as parameter for read() method", designatorContext.getNode());
		
				} else if ( type.getKind() == Struct.Class )
				{
					// Add when class is preset.
				};
			};
		} else 
		{			
			Obj entry = designatorContext.current;
			
			switch( entry.getType().getKind() )
			{
			case Struct.Bool:
				int patchSet1, patchSet0, patchEnd;
				Code.put(Code.read);
				Code.loadConst(1);
				Code.put( Code.jcc + Code.gt ); patchSet1 = Code.pc; Code.put2( 0 );
				Code.putJump(0); patchSet0 = Code.pc - 2;
				
				Code.fixup(patchSet1);
				Code.loadConst(1);
				storeVariable();
				Code.putJump(0); patchEnd = Code.pc - 2;
				
				Code.fixup(patchSet0);
				Code.loadConst(0);
				storeVariable();
				
				Code.fixup(patchEnd); 			
				break;
				
			case Struct.Int:			
				Code.put(Code.read);
				storeVariable();
				break;
				
			case Struct.Char:
				Code.put(Code.bread);
				storeVariable();
				break;
			};
		};
	}

}
