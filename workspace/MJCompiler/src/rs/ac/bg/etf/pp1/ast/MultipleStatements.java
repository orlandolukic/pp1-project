// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class MultipleStatements extends Matched {

    private MultipleStatementsStart MultipleStatementsStart;
    private StatementList StatementList;
    private MultipleStatementsEnd MultipleStatementsEnd;
    private SemiMaybe SemiMaybe;

    public MultipleStatements (MultipleStatementsStart MultipleStatementsStart, StatementList StatementList, MultipleStatementsEnd MultipleStatementsEnd, SemiMaybe SemiMaybe) {
        this.MultipleStatementsStart=MultipleStatementsStart;
        if(MultipleStatementsStart!=null) MultipleStatementsStart.setParent(this);
        this.StatementList=StatementList;
        if(StatementList!=null) StatementList.setParent(this);
        this.MultipleStatementsEnd=MultipleStatementsEnd;
        if(MultipleStatementsEnd!=null) MultipleStatementsEnd.setParent(this);
        this.SemiMaybe=SemiMaybe;
        if(SemiMaybe!=null) SemiMaybe.setParent(this);
    }

    public MultipleStatementsStart getMultipleStatementsStart() {
        return MultipleStatementsStart;
    }

    public void setMultipleStatementsStart(MultipleStatementsStart MultipleStatementsStart) {
        this.MultipleStatementsStart=MultipleStatementsStart;
    }

    public StatementList getStatementList() {
        return StatementList;
    }

    public void setStatementList(StatementList StatementList) {
        this.StatementList=StatementList;
    }

    public MultipleStatementsEnd getMultipleStatementsEnd() {
        return MultipleStatementsEnd;
    }

    public void setMultipleStatementsEnd(MultipleStatementsEnd MultipleStatementsEnd) {
        this.MultipleStatementsEnd=MultipleStatementsEnd;
    }

    public SemiMaybe getSemiMaybe() {
        return SemiMaybe;
    }

    public void setSemiMaybe(SemiMaybe SemiMaybe) {
        this.SemiMaybe=SemiMaybe;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(MultipleStatementsStart!=null) MultipleStatementsStart.accept(visitor);
        if(StatementList!=null) StatementList.accept(visitor);
        if(MultipleStatementsEnd!=null) MultipleStatementsEnd.accept(visitor);
        if(SemiMaybe!=null) SemiMaybe.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(MultipleStatementsStart!=null) MultipleStatementsStart.traverseTopDown(visitor);
        if(StatementList!=null) StatementList.traverseTopDown(visitor);
        if(MultipleStatementsEnd!=null) MultipleStatementsEnd.traverseTopDown(visitor);
        if(SemiMaybe!=null) SemiMaybe.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(MultipleStatementsStart!=null) MultipleStatementsStart.traverseBottomUp(visitor);
        if(StatementList!=null) StatementList.traverseBottomUp(visitor);
        if(MultipleStatementsEnd!=null) MultipleStatementsEnd.traverseBottomUp(visitor);
        if(SemiMaybe!=null) SemiMaybe.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("MultipleStatements(\n");

        if(MultipleStatementsStart!=null)
            buffer.append(MultipleStatementsStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(StatementList!=null)
            buffer.append(StatementList.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(MultipleStatementsEnd!=null)
            buffer.append(MultipleStatementsEnd.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(SemiMaybe!=null)
            buffer.append(SemiMaybe.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [MultipleStatements]");
        return buffer.toString();
    }
}
