// generated with ast extension for cup
// version 0.8
// 3/3/2020 14:57:52


package rs.ac.bg.etf.pp1.ast;

public class PrintWrapperAll implements SyntaxNode {

    private SyntaxNode parent;
    private int line;
    private PrintStart PrintStart;
    private Expr Expr;
    private PrintNumAddon PrintNumAddon;

    public PrintWrapperAll (PrintStart PrintStart, Expr Expr, PrintNumAddon PrintNumAddon) {
        this.PrintStart=PrintStart;
        if(PrintStart!=null) PrintStart.setParent(this);
        this.Expr=Expr;
        if(Expr!=null) Expr.setParent(this);
        this.PrintNumAddon=PrintNumAddon;
        if(PrintNumAddon!=null) PrintNumAddon.setParent(this);
    }

    public PrintStart getPrintStart() {
        return PrintStart;
    }

    public void setPrintStart(PrintStart PrintStart) {
        this.PrintStart=PrintStart;
    }

    public Expr getExpr() {
        return Expr;
    }

    public void setExpr(Expr Expr) {
        this.Expr=Expr;
    }

    public PrintNumAddon getPrintNumAddon() {
        return PrintNumAddon;
    }

    public void setPrintNumAddon(PrintNumAddon PrintNumAddon) {
        this.PrintNumAddon=PrintNumAddon;
    }

    public SyntaxNode getParent() {
        return parent;
    }

    public void setParent(SyntaxNode parent) {
        this.parent=parent;
    }

    public int getLine() {
        return line;
    }

    public void setLine(int line) {
        this.line=line;
    }

    public void accept(Visitor visitor) {
        visitor.visit(this);
    }

    public void childrenAccept(Visitor visitor) {
        if(PrintStart!=null) PrintStart.accept(visitor);
        if(Expr!=null) Expr.accept(visitor);
        if(PrintNumAddon!=null) PrintNumAddon.accept(visitor);
    }

    public void traverseTopDown(Visitor visitor) {
        accept(visitor);
        if(PrintStart!=null) PrintStart.traverseTopDown(visitor);
        if(Expr!=null) Expr.traverseTopDown(visitor);
        if(PrintNumAddon!=null) PrintNumAddon.traverseTopDown(visitor);
    }

    public void traverseBottomUp(Visitor visitor) {
        if(PrintStart!=null) PrintStart.traverseBottomUp(visitor);
        if(Expr!=null) Expr.traverseBottomUp(visitor);
        if(PrintNumAddon!=null) PrintNumAddon.traverseBottomUp(visitor);
        accept(visitor);
    }

    public String toString(String tab) {
        StringBuffer buffer=new StringBuffer();
        buffer.append(tab);
        buffer.append("PrintWrapperAll(\n");

        if(PrintStart!=null)
            buffer.append(PrintStart.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(Expr!=null)
            buffer.append(Expr.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        if(PrintNumAddon!=null)
            buffer.append(PrintNumAddon.toString("  "+tab));
        else
            buffer.append(tab+"  null");
        buffer.append("\n");

        buffer.append(tab);
        buffer.append(") [PrintWrapperAll]");
        return buffer.toString();
    }
}
