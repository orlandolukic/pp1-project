package rs.ac.bg.etf.pp1.context;

import java.util.LinkedList;

import rs.ac.bg.etf.pp1.ast.ForStatement;

public class ForLoopCheckerContext {

	public boolean hasCondition;
	public ConditionCheckerContext conditionContext;
	public int breakStatements;
	public boolean hasBreakStatement;
	public boolean hasReturnStatement;
	public int ifDepth;
	public boolean hasMultipleStatements;
	
	/**
	 * Variables used for code generation.
	 */
	public LinkedList<Integer> refactorIterationExpression;
	public LinkedList<Integer> refactorEndForLoop;
	public int addrIterationExpression;
	public int addrCheckCondition;
	public int addrEnd;
	public int addrBodyOfLoop;
	public ForStatement fs;
	public int forDepth;
	public int foreachDepth;
	public boolean allowCodeGeneration;
	
	public ForLoopCheckerContext( boolean generate )
	{
		this();
		
		if ( generate )
		{
			refactorEndForLoop = new LinkedList<Integer>();
			refactorIterationExpression = new LinkedList<Integer>();
			addrIterationExpression = 0;
			addrCheckCondition = 0;
			addrBodyOfLoop = 0;
			addrEnd = 0;
			fs = null;
			forDepth = 0;
			allowCodeGeneration = true;
			foreachDepth = 0;
		};
	}
	
	public ForLoopCheckerContext()
	{
		hasCondition = false;
		conditionContext = null;
		hasBreakStatement = false;
		hasReturnStatement = false;
		ifDepth = 0;
		hasMultipleStatements = false;
		breakStatements = 0;
	}
}

