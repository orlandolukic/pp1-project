package rs.ac.bg.etf.pp1.util;

import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedList;

import rs.ac.bg.etf.pp1.ExprChecker;
import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.context.MethodInvocationContext;
import rs.ac.bg.etf.pp1.des.DesignatorFinder;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class MethInvocationCheck extends VisitorAdaptor {
	
	private int paramIndex;
	private SemanticAnalyzer sa;
	private SyntaxNode p;
	private Obj[] params;
	private Obj method;
	private Stack<MethodInvocationContext> stack;
	private boolean error;
	private boolean omittedFirst;
	public LinkedList<SyntaxNode> nodes;
	
	public MethInvocationCheck(SemanticAnalyzer sa, MethodInvocationContext context, SyntaxNode snode)
	{
		this.method = context.methodObj;
		this.paramIndex = 0;
		this.sa = sa;
		this.p = snode;
		this.error = false;
		this.omittedFirst = false;
		
		// Prepare stack.
		this.stack = new Stack<MethodInvocationContext>();		
		this.stack.push(context);	
		prepareParameters();		
		context.parametersProvided = 0;
		nodes = new LinkedList<SyntaxNode>();
	}
	
	/**
	 * Visit each parameter in actual parameter's list.
	 * __methodname__( Expr (*), ... )
	 */
	public void visit(ActParam ap)
	{
		if ( nodes.contains(ap) )
			return;
		
		if ( error )
			return;
		
		boolean c = false, error = false;
		ExprNoError e = (ExprNoError) ap.getExpr();	
		Struct paramStruct;
		MethodInvocationContext context = stack.top();
		
		if ( !context.hasParameters() )
		{
			this.error = true;
			return;
		};
		
		int index = ++context.parametersProvided;
		if ( !omittedFirst && context.isClassMethod )
		{
			index = ++context.parametersProvided;
			omittedFirst = true;
		};
		
		paramStruct = context.params[index-1].getType();
		ExprChecker checker = null;
		
		switch( paramStruct.getKind() )
		{
		case Struct.Int:
			checker = new ExprChecker( Tab.intType );
			break;
			
		case Struct.Char:
			checker = new ExprChecker( Tab.charType );
			break;
			
		case Struct.Bool:
			checker = new ExprChecker( Functions.boolType );
			break;
			
		case Struct.Array:
			Struct elemType = paramStruct.getElemType();
			checker = new ExprChecker( Functions.getStructRegular( elemType, true) );		
			checker.getContext().isLenInvocation = this.method.getName().equals("len");
			break;
			
		case Struct.Class:
			checker = new ExprChecker( paramStruct );	
			break;
			
		default:
			error = true;
			sa.report_warning("Undefined type on method's actual parameter " + paramIndex + " check", p);			
		};
		
		if ( error )
			return;
		
		// Set array reference as expected value.
		checker.getContext().onlyReference = true;
		
		try {			
			c = checker.process(e);
			if ( !c )				
				sa.report_checker(checker, p);
		} catch( Exception ex ) {}
		nodes.add(ap);
	}
	
	public void visit(FactorParensStart fps)
	{
		if ( error )
			return;
		
		SemanticAnalyzer sa = SemanticAnalyzer.__instance__();
		FactorDesignator fd = (FactorDesignator) fps.getParent().getParent();
		DesignatorFinder df = new DesignatorFinder(SemanticAnalyzer.__instance__());
		df.traverse( fd.getDesignator() );
		
		MethodInvocationContext context = new MethodInvocationContext( df.getObject() );
		context.isClassMethod = sa.classIsInside || df.isClassReference();
		stack.push(context);
		prepareParameters();
	}
	
	public void visit(FactorParensEnd fpe)
	{
		if ( error )
			return;
		/*
		MethodInvocationContext context = stack.pop();
		*/
		stack.pop();
	}
	
	private void prepareParameters()
	{
		// Add all params in array.
		int index = 0;
		MethodInvocationContext context = stack.top();		
		int len = context.methodObj.getLevel();
		
		if ( len == 0 )
			return;
		
		context.params = new Obj[len];
		Obj o;
		Collection<Obj> sym = context.methodObj.getLocalSymbols();
		Iterator<Obj> it = sym.iterator();
		while( it.hasNext() && index < len )
		{
			o = it.next();
			context.params[index++] = o;
		};
	}
	
}
