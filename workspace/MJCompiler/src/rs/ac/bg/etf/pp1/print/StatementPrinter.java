package rs.ac.bg.etf.pp1.print;

import rs.ac.bg.etf.pp1.ast.*;

public class StatementPrinter extends Printer<StatementListOptions> {

	public StatementPrinter(StatementListOptions snode) {
		super(snode);
	}

	@Override
	public void print() 
	{
		if ( snode instanceof StatementMatched )
		{
			Matched sm = ((StatementMatched) snode).getMatched();
			if ( sm instanceof ReadStatement )
				print((ReadStatement) sm);
			else if ( sm instanceof PrintStatement )
				print(((PrintStatement) sm));
		}
		
	}
	
	/**
	 * Print read statement.
	 * @param rs
	 */
	private void print(ReadStatement rs)
	{
		str.append("read(");
		DesignatorPrinter printer = new DesignatorPrinter( rs.getDesignator() );
		str.append( printer );
		str.append(");");
	}
	
	/**
	 * Print print statement.
	 * @param rs
	 */
	private void print(PrintStatement rs)
	{
		str.append("print(");
		ExprPrinter printer = new ExprPrinter( (ExprNoError) rs.getPrintWrapper().getPrintWrapperAll().getExpr() );
		str.append( printer );
		str.append(");");
	}

}
