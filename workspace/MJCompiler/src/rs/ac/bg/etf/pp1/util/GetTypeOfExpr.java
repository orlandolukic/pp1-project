package rs.ac.bg.etf.pp1.util;

import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.des.DesignatorFinder;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class GetTypeOfExpr extends VisitorAdaptor {

	private boolean isReference;
	private Struct type;
	private Obj entry;
	private int methodInvocation;

	
	public GetTypeOfExpr()
	{
		type = Tab.noType;
		isReference = false;
		entry = null;
	}
	
	public Struct getType()
	{
		return type;
	}
	
	public Obj getEntryIfExists()
	{
		return entry;
	}
	
	public boolean isReferenceType()
	{
		return isReference;
	}
	
	public void visit(FactorConstDeclType f)
	{
		if ( type == Tab.noType && methodInvocation == 0 )
		{
			ExprConstDeclType e = f.getExprConstDeclType();
			if ( e instanceof EBoolConst )
				type = Functions.boolType;
			else if ( e instanceof ECharConst )
				type = Tab.charType;
			else if ( e instanceof ENumberConst )
				type = Tab.intType;
		};
	}
	
	public void visit(FactorDesignator fd)
	{
		DesignatorFinder df = new DesignatorFinder(SemanticAnalyzer.__instance__());
		fd.getDesignator().traverseBottomUp(df);
		
		Obj entry = df.getObject();
		if ( type == Tab.noType && methodInvocation == 0 )
		{			
			this.entry = entry;
			isReference = entry.getType().isRefType();
			type = entry.getType();
		};
	}
	
	public void visit(FactorParensStart s)
	{
		methodInvocation++;
	}
	
	public void visit(FactorParensEnd s)
	{
		methodInvocation--;
	}
}
