package rs.ac.bg.etf.pp1.util;

import rs.ac.bg.etf.pp1.gen.MethodCodeGeneration;
import rs.etf.pp1.symboltable.*;
import rs.etf.pp1.symboltable.concepts.*;

public class CGExprContext {

	public static final int SIDE_EXPR = -1;
	public static final int SIDE_RIGHT = 0;
	public static final int SIDE_LEFT = 1;
	
	public Struct type;
	public Obj var;
	public boolean isVar;
	public boolean isConst;
	
	public boolean finished;
	public boolean hasSign;
	public boolean potentiallyVarConst;
	public boolean hasInnerExpr;
	public boolean firstFactor;
	public boolean generateActualParam;
	public boolean generateArrayDuplication;
	public final int side;
	public boolean object;
	public MethodCodeGeneration methodCodeGeneration;
	
	/**
	 * Complex operator.
	 */
	public boolean isComplexOperator;
	public Obj complexOperatorObj;
	public ComplexOperatorContext complexContext;
	
	public CGExprContext()
	{
		this(SIDE_EXPR);
	}
	
	public CGExprContext(int side)
	{
		this.var = Tab.noObj;
		this.type = Tab.intType;
		this.isVar = false;
		this.isConst = false;
		
		this.finished = false;
		this.hasSign = false;
		this.potentiallyVarConst = false;
		this.generateArrayDuplication = false;
		this.hasInnerExpr = false;
		this.side = side;
		this.firstFactor = true;
		this.isComplexOperator = false;
		this.complexOperatorObj = null;
		this.complexContext = null;
		this.generateActualParam = true;
		this.object = false;
		this.methodCodeGeneration = null;
	}
}
