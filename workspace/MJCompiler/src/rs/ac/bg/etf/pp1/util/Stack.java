package rs.ac.bg.etf.pp1.util;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class Stack<T> implements Iterator<T> {
	
	private class StackElement<M>
	{
		public M elem;
		public StackElement<M> next;
		public StackElement( M element ) 
		{
			this.elem = element;
		}
	}
	
	private StackElement<T> itCurrent;
	private StackElement<T> first;
	private int elements;
	private boolean takenFirst;
	
	public Stack()
	{
		this.itCurrent = null;
		this.first = null;
		this.elements = 0;
	}
	
	public void push(T element)
	{
		StackElement<T> el = new StackElement<T>(element);
		if ( first == null )
			first = el;
		else
		{
			el.next = first;
			first = el;
		};
		this.elements++;
	}
	
	public T pop()
	{
		StackElement<T> el = null;
		
		if ( this.elements > 0 )
		{			
			el = first;
			first = first.next;
			elements--;
		};
		
		return el != null ? el.elem : null;
	}
	
	public int size()
	{
		return elements;
	}
	
	public T top()
	{
		if ( first != null )
			return first.elem;
		
		return null;
	}
	
	public T top(int offset)
	{
		if ( offset > 0 )
			return null;
		
		StackElement<T> iterator = first;
		int index = -offset;
		if ( index < this.elements )
		{
			int i = 0;
			while( i < index )
			{
				iterator = iterator.next;
				i++;
			};			
		};
		
		return iterator.elem;
	}
	
	public void iteratorStart()
	{
		this.itCurrent = first;
		takenFirst = true;
	}

	@Override
	public boolean hasNext() {		
		return itCurrent != null && itCurrent.next != null;
	}

	@Override
	public T next() {
		if ( !takenFirst )
			itCurrent = itCurrent.next;
		takenFirst = false;
		return itCurrent.elem;
	}
	

}
