package rs.ac.bg.etf.pp1.checkers;

import rs.ac.bg.etf.pp1.SemanticAnalyzer;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.context.ForLoopCheckerContext;
import rs.ac.bg.etf.pp1.util.Stack;

public class ForLoopChecker extends VisitorAdaptor {
	
	private SemanticAnalyzer sa;
	private Stack<ForLoopCheckerContext> stack; 
	
	public ForLoopChecker(SemanticAnalyzer sa)
	{
		this.sa = sa;
		this.stack = new Stack<ForLoopCheckerContext>();
	}
	
	public void visit(ForStatementStart fss)
	{
		this.stack.push( new ForLoopCheckerContext() );
		sa.report_info("FOR loop detected", fss.getParent());
	}
	
	private ForLoopCheckerContext top()
	{
		return stack.top();
	}
	
	/**
	 * When present condition.
	 */
	public void visit(ForConditionPresent fcp)
	{
		top().hasCondition = true;
		
		// Check condition
		ConditionChecker cc = new ConditionChecker(sa);
		fcp.getCondition().traverseBottomUp(cc);
		top().conditionContext = cc.getConditionContext();	
		
		if ( sa.passed() && top().conditionContext.isAlwaysFalse() )
			sa.report_error("Never entering for loop, condition equals false", fcp.getParent());
	}
	
	/**
	 * break; (*)
	 */
	public void visit(BreakStatement bs)
	{
		top().hasBreakStatement = true;
	}
	
	/**
	 * return Expr; (*)
	 */
	public void visit(ReturnExpr rs)
	{
		top().hasReturnStatement = true;
	}
	
	public void visit(MultipleStatementsStart s)
	{
		top().hasMultipleStatements = true;
	}
	
	/**
	 * Executes on the end of check!
	 * 
	 * for ( ...; ...; ... ) { ... } (*)
	 */
	public void visit(ForStatement fs)
	{
		ForLoopCheckerContext c = stack.pop();
		if ( !sa.passed() )
			return;
		
		sa.stmtForwardUnreachable = false;
		/*
		if ( !c.hasBreakStatement && c.hasCondition && c.conditionContext.isAlwaysTrue() || c.hasReturnStatement || ( c.hasCondition && c.conditionContext.isAlwaysTrue() || !c.hasCondition ) )	
			sa.stmtForwardUnreachable = true;
			*/
	}
	
	public void visit( IfStart i )
	{
		top().ifDepth++;
	}
	
	public void visit(MatchedIfElse mie)
	{
		top().ifDepth--;
	}
	
	public void visit(UnmatchedIf u)
	{
		top().ifDepth--;
	}
	
	public void visit(UnmatchedIfElse f)
	{
		top().ifDepth--;
	}
	
	public void visit( ForeachStatement fs )
	{
		ForeachLoopChecker flc = new ForeachLoopChecker(sa);
		fs.traverseBottomUp(flc);
	}
}
