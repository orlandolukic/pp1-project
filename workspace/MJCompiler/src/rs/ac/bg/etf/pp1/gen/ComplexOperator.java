package rs.ac.bg.etf.pp1.gen;

import java.util.LinkedList;

import rs.ac.bg.etf.pp1.Functions;
import rs.ac.bg.etf.pp1.ast.*;
import rs.ac.bg.etf.pp1.des.DesignatorFinder;
import rs.ac.bg.etf.pp1.util.CGExprContext;
import rs.ac.bg.etf.pp1.util.ComplexOperatorContext;
import rs.ac.bg.etf.pp1.util.GetStartNode;
import rs.ac.bg.etf.pp1.util.HasComplexOperators;
import rs.ac.bg.etf.pp1.util.Stack;
import rs.etf.pp1.mj.runtime.Code;
import rs.etf.pp1.symboltable.Tab;
import rs.etf.pp1.symboltable.concepts.*;

public class ComplexOperator {

	private TD_Expr exprBasicGenerator;
	private TD_ComplexExpr exprComplexGenerator;
	
	private HasComplexOperators hco;
	private SyntaxNode startValueNode;
	private SyntaxNode[] toProcess;
	private ComplexOperatorContext[] toProcessOperations;
	private SyntaxNode expression;
	private boolean error;
	private boolean pop;
	private boolean isReturning;
	
	public ComplexOperator()
	{
		hco = new HasComplexOperators();
		exprBasicGenerator = new TD_Expr();
		exprComplexGenerator = new TD_ComplexExpr();
		this.pop = false;
		this.isReturning = false;
	}
	
	public void toPop(boolean toPop)
	{
		this.pop = toPop;
	}
	
	public boolean errorOccured()
	{
		return error;
	}
	
	public void generate(SyntaxNode expression)
	{
		this.expression = expression;
		this.startValueNode = null;
		this.error = false;
		GetStartNode gsn = new GetStartNode();
		
		try {
			// Finds start node from which evaluation begins.
			expression.traverseTopDown(gsn);
			startValueNode = gsn.getFactor();
			if ( startValueNode == null )
				throw new Exception("Start node could not be found");
			
			//System.out.println(startValueNode);
			
			// Find all expressions linked with complex operator.
			T_ComplexOperator t = new T_ComplexOperator(startValueNode);
			expression.traverseBottomUp(t);
			this.toProcess = t.getAllNodes();
			this.toProcessOperations = t.getAllOperationsOnLeftSide();
			
			// Generate code.
			generateCode();		
			
		} catch( Exception e ) {
			this.error = true;
			 e.printStackTrace();
		}
	}
	
	/**
	 * Method used to generate code for the complex expression.
	 */
	private void generateCode()
	{	
		// Generate factors.
		generateFactor(0);
	}
	
	private void generateFactor( int i )
	{
		if ( i == toProcess.length )
		{
			generateExpressionOnRightSide();
			isReturning = true;
			return;
		};
		
		DesignatorFinder finder = new DesignatorFinder(null);
		finder.setCheck(false);
		finder.traverse( (( FactorDesignator ) toProcess[i]).getDesignator() );
		
		// Generate code for each factor.
		generateCodeForFactor( i, toProcess[i] );
		
		// Generate next factor.
		generateFactor(i+1);
		
		// Add operation.
		ComplexOperatorContext c = toProcessOperations[i];
		if ( c.isAddop )
			Functions.generateAddopOperator(c.addop);
		else
			Functions.generateMulopOperator(c.mulop);
		
		// Check if variable is not primitive.
		DesignatorFinder df = new DesignatorFinder(null);
		df.setCheck(false);
		df.traverse( toProcess[i] );		
		
		// Duplicate stack's top element.		
		if ( !df.isArrayAccess() && !df.isPrimitiveVariable() )
		{	
			Code.put( Code.dup );
			Code.put( Code.dup_x2 );
			Code.put(Code.pop);			
		} else if ( !df.isArrayAccess() && df.isPrimitiveVariable() )
		{
			Code.put( Code.dup );
		} else
			Code.put( Code.dup_x2 );
		
		// Store variable.
		store( (FactorDesignator) toProcess[i] );	
		
		if ( i==0 && pop )
			Code.put( Code.pop );
	}
	
	private void store( FactorDesignator node )
	{
		Designator d = node.getDesignator();
		DesignatorFinder finder = new DesignatorFinder(null);
		finder.setCheck(false);
		finder.traverse(d);
		
		if ( finder.isPrimitiveVariable() || isReturning )
			finder.storeVariable();
		else
		{
			finder.generateThroughTheList(false);
			Code.put( Code.dup_x1 );
			Code.put( Code.pop );
			finder.storeVariable();
		}
	}
	
	/**
	 * Generates expression on the right side.
	 */
	private void generateExpressionOnRightSide()
	{
		CGHelper helper = new CGHelper();
		helper.setStartNode(startValueNode);		
		expression.traverseBottomUp(helper);		
	}
	
	private boolean isArrayAccess( SyntaxNode node )
	{
		if ( node instanceof FactorDesignator )
		{
			FactorDesignator fd = (FactorDesignator) node;
			Designator d = fd.getDesignator();
			DesignatorFinder finder = new DesignatorFinder(null);
			d.traverseBottomUp(finder);
			
			return finder.isArrayAccess();
		};
		
		return false;
	}
	
	private void generateCodeForFactor( int i, SyntaxNode node )
	{		
		
		if ( node instanceof FactorDesignator )
		{
			FactorDesignator fd = (FactorDesignator) node;
			boolean isArrayAccess = isArrayAccess(node);
			
			
			hco.performCheck(node);			
			exprBasicGenerator.createNewContext(CGExprContext.SIDE_EXPR);
			exprBasicGenerator.context.generateArrayDuplication = true;
			exprBasicGenerator.visitFactor(fd);			
		}; 
	}
	
	
	/**
	 * Helper class for code generation on the right side.
	 * 
	 * @author P
	 *
	 */
	private class CGHelper extends VisitorAdaptor
	{
		private class CGHelperContext
		{
			private boolean negative;
		};
		
		private int nestedFuncCall;
		private LinkedList<SyntaxNode> visitedNodes;
		private boolean accept;
		private SyntaxNode start;
		private Stack<CGHelperContext> stack;
		
		public void setStartNode(SyntaxNode node)
		{
			accept = false;
			start = node;
			this.stack = new Stack<CGHelperContext>();
			this.stack.push(new CGHelperContext());
			this.visitedNodes = new LinkedList<SyntaxNode>();
			this.nestedFuncCall = 0;
		}
		
		private boolean isNegative()
		{
			return stack.top().negative;
		}
		
		private boolean into(SyntaxNode n)
		{
			if ( n != start && !accept )
				accept = false;
			else
				accept = true;
			
			return accept;
		}

		
		public void visit(NegativeExprMinus m)
		{
			stack.top().negative = true;
		}
		
		public void visit(FactorWithExprStart s)
		{
			stack.push(new CGHelperContext());
		}
		
		public void visit(Designator d)
		{
			if ( !into( d.getParent() ) )
				return;
		}
		
		public void visit(FactorConstDeclType f)
		{
			if ( !into(f) || nestedFuncCall > 0 )
				return;
			
			exprBasicGenerator.createNewContext(CGExprContext.SIDE_EXPR);
			exprBasicGenerator.context.hasSign = isNegative();
			exprBasicGenerator.visitFactor(f);			
		}
		
		public void visit(FactorWithExpr f)
		{
			if ( !into(f) || nestedFuncCall > 0 )
				return;
			
			ExprNoError e = (ExprNoError) f.getExpr();
			hco.performCheck(e);
			if ( !hco.cond() )
			{
				exprBasicGenerator.createNewContext(CGExprContext.SIDE_EXPR);
				exprBasicGenerator.visit(e);
			} else
			{
				exprComplexGenerator.createNewContext(CGExprContext.SIDE_EXPR);
				exprComplexGenerator.visit(e);
			};
			
			stack.pop();
		}
		
		public void visit(FactorDesignator fd)
		{
			if ( !into(fd) || fd.getFactorParens() instanceof FactorParensPresent || nestedFuncCall > 0 )
				return;
			
			hco.performCheck(fd);
			if ( !hco.cond() )
			{
				exprBasicGenerator.createNewContext(CGExprContext.SIDE_EXPR);
				exprBasicGenerator.visitFactor(fd);
			} else
			{
				exprComplexGenerator.createNewContext(CGExprContext.SIDE_EXPR);
				exprComplexGenerator.visitFactor(fd);
			};
		}
		
		public void visit(AddopTermAll at)
		{
			if ( !into(at) || nestedFuncCall > 0 )
				return;
			
			if ( !Functions.isComplexAddopOperator( at.getAddop() ) )
				Functions.generateAddopOperator( at.getAddop() );
		}
		
		public void visit(MulopFactorAll m)
		{
			if ( !into(m) || nestedFuncCall > 0 )
				return;
			
			if ( !Functions.isComplexMulopOperator( m.getMulop() ) )
				Functions.generateMulopOperator( m.getMulop() );
		}
		
		public void visit(ActParam ap)
		{
			/*
			if ( visitedNodes.contains(ap) )
				return;
			
			ExprNoError e = (ExprNoError) ap.getExpr();
			hco.performCheck(e);
			if ( !hco.cond() )
			{
				exprBasicGenerator.createNewContext(CGExprContext.SIDE_EXPR);
				exprBasicGenerator.visit(e);
			} else
			{
				exprComplexGenerator.createNewContext(CGExprContext.SIDE_EXPR);
				exprComplexGenerator.visit(e);
			};
			visitedNodes.add(ap);
			*/
		}
		
		public void visit(FactorParensStart f)
		{
			if ( !into(f) )
				return;
			
			nestedFuncCall++;
		}
		
		public void visit(FactorParensEnd f)
		{
			if ( !into(f) )
				return;
			
			nestedFuncCall--;
			
			if ( nestedFuncCall == 0 )
			{		
				// Generate code.
				MethodCodeGeneration mcg = new MethodCodeGeneration();
				f.getParent().getParent().traverseBottomUp(mcg);
			};
		}
	}
}
