package rs.ac.bg.etf.pp1;

import java_cup.runtime.Symbol;

%%

%{

	// Informacija o poziciji tokena
	private Symbol new_symbol(int type)
	{
		return new Symbol(type, yyline+1, yycolumn);
	}
	
	private Symbol new_symbol(int type, Object value)
	{
		return new Symbol(type, yyline+1, yycolumn, value);
	}

%}

%cup
%line
%column
%xstate COMMENT
%xstate COMMENT_ALL

%eofval{
	return new_symbol(sym.EOF);
%eofval}

%%

" "  		{}
"\t" 		{}
"\b" 		{}
"\r\n" 		{}
"\f"		{}

"program" 	{ return new_symbol(sym.PROGRAM, yytext());  }
"read"		{ return new_symbol(sym.READ, yytext());  }
"print"		{ return new_symbol(sym.PRINT, yytext());  }
"void"		{ return new_symbol(sym.VOID, yytext());  }
"return"	{ return new_symbol(sym.RETURN, yytext()); }
"break"		{ return new_symbol(sym.BREAK, yytext()); }
"continue"	{ return new_symbol(sym.CONTINUE, yytext()); }
"for"		{ return new_symbol(sym.FOR, yytext()); }
"foreach"	{ return new_symbol(sym.FOREACH, yytext()); }
"if"		{ return new_symbol(sym.IF, yytext()); }
"else"		{ return new_symbol(sym.ELSE, yytext()); }
"class"		{ return new_symbol(sym.CLASS, yytext()); }
"extends"	{ return new_symbol(sym.EXTENDS, yytext()); }
"abstract"	{ return new_symbol(sym.ABSTRACT, yytext()); }
"private"	{ return new_symbol(sym.PRIVATE, yytext()); }
"protected"	{ return new_symbol(sym.PROTECTED, yytext()); }
"public"	{ return new_symbol(sym.PUBLIC, yytext()); }
"."			{ return new_symbol(sym.DOT, yytext()); }
":"			{ return new_symbol(sym.COLON, yytext()); }
"!"			{ return new_symbol(sym.INVERTOR, yytext()); }
"&&"		{ return new_symbol(sym.AND, yytext()); }
"||"		{ return new_symbol(sym.OR, yytext()); }
"=="		{ return new_symbol(sym.EQ, yytext()); }
"!="		{ return new_symbol(sym.NEQ, yytext()); }
">"			{ return new_symbol(sym.GT, yytext()); }
">="		{ return new_symbol(sym.GTE, yytext()); }
"<"			{ return new_symbol(sym.LT, yytext()); }
"<="		{ return new_symbol(sym.LTE, yytext()); }
"+="		{ return new_symbol(sym.PLUSEQUAL, yytext()); }
"-="		{ return new_symbol(sym.MINUSEQUAL, yytext()); }
"*="		{ return new_symbol(sym.TIMESEQUAL, yytext()); }
"/="		{ return new_symbol(sym.DIVEQUAL, yytext()); }
"%="		{ return new_symbol(sym.MODEQUAL, yytext()); }
"+"			{ return new_symbol(sym.PLUS, yytext());  }
"-"			{ return new_symbol(sym.MINUS, yytext());  }
"*"			{ return new_symbol(sym.TIMES, yytext());  }
"/"			{ return new_symbol(sym.DIV, yytext());  }
"%"			{ return new_symbol(sym.MOD, yytext());  }
"="			{ return new_symbol(sym.EQUAL, yytext());  }
";"			{ return new_symbol(sym.SEMI, yytext());  }
","			{ return new_symbol(sym.COMMA, yytext());  }
"("			{ return new_symbol(sym.LPAREN, yytext());  }
")"			{ return new_symbol(sym.RPAREN, yytext());  }
"{"			{ return new_symbol(sym.LBRACE, yytext());  }
"}"			{ return new_symbol(sym.RBRACE, yytext());  }
"["			{ return new_symbol(sym.LBRACKET, yytext());  }
"]"			{ return new_symbol(sym.RBRACKET, yytext());  }
"++"		{ return new_symbol(sym.PLUSPLUS, yytext());  }
"--"		{ return new_symbol(sym.MINUSMINUS, yytext());  }
"int"		{ return new_symbol(sym.INT_TYPE, yytext());  }
"bool"		{ return new_symbol(sym.BOOLEAN_TYPE, yytext());  }
"char"		{ return new_symbol(sym.CHAR_TYPE, yytext());  }
"const"		{ return new_symbol(sym.CONST, yytext());  }
"new"		{ return new_symbol(sym.NEW, yytext());  }
"true"|"false"	{ return new_symbol(sym.BOOLEAN, yytext());  }


"/*" "*"?							{ yybegin(COMMENT_ALL); }
<COMMENT_ALL>	"*"? "*/"			{ yybegin(YYINITIAL); }
<COMMENT_ALL> 	[^]   				{ yybegin(COMMENT_ALL); } 

"//"				{ yybegin(COMMENT); }
<COMMENT> . 		{ yybegin(COMMENT); }
<COMMENT> "\r\n"	{ yybegin(YYINITIAL); }

(0|[1-9][0-9]*)					{ return new_symbol(sym.NUMBER, new Integer(yytext())); }
([a-z]|[A-Z])[a-z|A-Z|0-9|_]*	{ return new_symbol(sym.IDENT, yytext()); }
"'"."'"							{ return new_symbol(sym.CHAR, yytext()); }

. { System.err.println("Lexical error (" + yytext() + ") on line " + (yyline+1) + ", column " + (yycolumn+1) + "."); }







